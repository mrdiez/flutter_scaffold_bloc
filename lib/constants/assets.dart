abstract class Asset {
  static const String VerticalLogo = 'assets/logos/logo-vertical.png';
  static const String HorizontalLogo = 'assets/logos/logo-horizontal.png';
  static const String VerticalDarkLogo = 'assets/logos/logo-vertical-dark.png';
  static const String HorizontalDarkLogo = 'assets/logos/logo-horizontal-dark.png';
  static const String EasterEggLogo = 'assets/logos/easter-egg.png';
}
