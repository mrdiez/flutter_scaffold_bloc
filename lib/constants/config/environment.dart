abstract class Environment {
  // static const bool mock = true;
  // true when flutter run or build commands are launched with "--dart-define=TESTING=true" argument
  static const bool Testing = bool.fromEnvironment('TESTING');
  // true when flutter run or build commands are launched with "--dart-define=MOCK=true" argument
  static const bool Mock = bool.fromEnvironment('MOCK') || Testing; // || true;
  // true when "flutter run" or "build" commands are launched with "--dart-define=PROD=true" argument or "--release"
  static const bool Prod = bool.fromEnvironment('PROD') || bool.fromEnvironment('dart.vm.product');

  static const String HttpEndpoint = Prod ? _HttpEndpointProd : _HttpEndpointDev;

  static const _HttpEndpointProd = 'https://prod.endpoint.com';
  static const _HttpEndpointDev = 'https://dev.endpoint.com';

  static const ApiKey = 'KEY';
}
