enum StorageKey {
  // ThemeService
  ThemeMode,
  SelectedTheme,

  // Login
  RememberMe,
  Login,
  Password,
}
