import 'package:collection/collection.dart';
import 'package:flutter_scaffold_bloc/constants/mocks/data/users.dart';
import 'package:flutter_scaffold_bloc/modules/auth/data/auth_provider.dart';
import 'package:flutter_scaffold_bloc/modules/auth/data/auth_repository.dart';

class MockedAuthProvider extends AuthProvider {
  const MockedAuthProvider();

  @override
  Future<Map<String, dynamic>?> authenticate(String login, String password) async {
    await Future.delayed(Duration(seconds: 2));
    return MockedUsers.firstWhereOrNull(
        (data) => (data['username'] == login || data['email'] == login) && data['password'] == password);
  }

  @override
  Future<Map<String, List<dynamic>>?> resetPasswordMethods(String login) async {
    var result;
    await Future.delayed(Duration(seconds: 2));
    var user = MockedUsers.firstWhereOrNull((data) => data['username'] == login || data['email'] == login);
    if (user != null) {
      result = {'methods': []};
      AuthRepository.MethodsByParams.keys.forEach((method) {
        if (user[method] != null && (user[method] as String).isNotEmpty) {
          result['methods'].add(method);
        }
      });
    }
    return result;
  }

  @override
  Future<bool?> postResetPasswordMethod(String login, String method) async {
    await Future.delayed(Duration(seconds: 2));
    var user = MockedUsers.firstWhereOrNull((data) => data['username'] == login || data['email'] == login);
    return user != null;
  }

  @override
  Future<String?> resetPassword(String login, String code) async {
    await Future.delayed(Duration(seconds: 2));
    var user = MockedUsers.firstWhereOrNull((data) => data['username'] == login || data['email'] == login);
    if (user != null && code == '1234') return '1234';
  }

  @override
  Future<String?> signUp(String username, String email, String password) async {
    await Future.delayed(Duration(seconds: 2));
    var user = MockedUsers.firstWhereOrNull((data) => data['username'] == username);
    if (user != null) return username;
    user = MockedUsers.firstWhereOrNull((data) => data['email'] == email);
    if (user != null) return email;
  }
}
