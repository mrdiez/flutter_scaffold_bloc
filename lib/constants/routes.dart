import 'package:flutter/material.dart';
import 'package:flutter_scaffold_bloc/pages/home.dart';
import 'package:flutter_scaffold_bloc/pages/login.dart';

const String LoginRoute = '/';
const String HomeRoute = '/home';

const Map<String, Widget> MyRoutes = {
  LoginRoute: LoginPage(),
  HomeRoute: HomePage(),
};
