// ignore_for_file: non_constant_identifier_names
import 'package:flutter/material.dart';

abstract class ThemeColor {
  // This constructor prevents instantiation and extension.
  // ignore: unused_element
  ThemeColor._();

  // MAIN COLORS

  static Color Primary = _1;
  static Color PrimaryVariant = _1Dark;

  static Color Secondary = _2;
  static Color SecondaryVariant = _2Dark;

  static const Color _1 = Color.fromRGBO(85, 199, 172, 1);
  static const Color _1Dark = Color.fromRGBO(55, 149, 122, 1);

  static const Color _2 = Color.fromRGBO(238, 103, 90, 1);
  static const Color _2Dark = Color.fromRGBO(188, 53, 40, 1);

  static const Color _3 = Color.fromRGBO(53, 167, 255, 1);
  static const Color _3Dark = Color.fromRGBO(13, 127, 215, 1);

  static const Color _4 = Color.fromRGBO(255, 231, 76, 1);
  static const Color _4Dark = Color.fromRGBO(230, 216, 41, 1);

  // CUSTOM THEMES COLORS

  static const String DefaultTheme = 'Green/Orange';

  static Map<String, Function> Switcher = {
    DefaultTheme: () => _switcher(_1, _1Dark, _2, _2Dark),
    'Orange/Green': () => _switcher(_2, _2Dark, _1, _1Dark),
    'Green/Blue': () => _switcher(_1, _1Dark, _3, _3Dark),
    'Blue/Green': () => _switcher(_3, _3Dark, _1, _1Dark),
    'Green/Yellow': () => _switcher(_1, _1Dark, _4, _4Dark),
    'Yellow/Green': () => _switcher(_4, _4Dark, _1, _1Dark),
    'Orange/Blue': () => _switcher(_2, _2Dark, _3, _3Dark),
    'Blue/Orange': () => _switcher(_3, _3Dark, _2, _2Dark),
    'Orange/Yellow': () => _switcher(_2, _2Dark, _4, _4Dark),
    'Yellow/Orange': () => _switcher(_4, _4Dark, _2, _2Dark),
    'Blue/Yellow': () => _switcher(_3, _3Dark, _4, _4Dark),
    'Yellow/Blue': () => _switcher(_4, _4Dark, _3, _3Dark),
  };

  static void _switcher(Color primary, Color primaryVariant, Color secondary, Color secondaryVariant) {
    Primary = primary;
    PrimaryVariant = primaryVariant;
    Secondary = secondary;
    SecondaryVariant = secondaryVariant;
  }

  // COMMON COLORS

  // Actions
  static const Color Positive = _1;
  static const Color Negative = _2Dark;

  // 50 shades of grey
  static const Color White = Color.fromRGBO(255, 255, 255, 1);
  static const Color Lightest = Color.fromRGBO(230, 243, 255, 1);
  static const Color Lighter = Color.fromRGBO(217, 230, 247, 1);
  static const Color Light = Color.fromRGBO(187, 200, 217, 1);
  static const Color LightGrey = Color.fromRGBO(157, 170, 187, 1);
  static const Color Grey = Color.fromRGBO(127, 140, 157, 1);
  static const Color DarkGrey = Color.fromRGBO(97, 110, 127, 1);
  static const Color Dark = Color.fromRGBO(67, 80, 97, 1);
  static const Color Darker = Color.fromRGBO(37, 50, 67, 1);
  static const Color Darkest = Color.fromRGBO(7, 20, 37, 1);
  static const Color Black = Color.fromRGBO(0, 0, 0, 1);

  // Disabled
  static Color DisabledColor = Light.withOpacity(0.95);
  static Color DisabledFontColor = DarkGrey.withOpacity(0.5);

  // Misc
  static Color TooltipColor = DarkGrey.withOpacity(0.75);
  static const Color SideMenuColor = Light;
  static const Color SideMenuColorDark = Darker;
  static const Color ShadowColor = Light;
  static const Color AvatarColor = Light;
  static const Color AvatarColorDark = Darker;
  static const Color RefreshIconColor = Lightest;
  static Color get Divider => Primary;
  static Color get TitleUnderline => Primary;
  static Color get Toast => Primary;
}
