// ignore_for_file: non_constant_identifier_names
import 'package:flutter/material.dart';

import 'colors.dart';
import 'sizes.dart';

ThemeData CommonTheme = ThemeData(
  fontFamily: 'Cera Pro',
  disabledColor: ThemeColor.DisabledColor,
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ButtonStyle(
      backgroundColor: MaterialStateProperty.resolveWith<Color?>((states) {
        if (states.contains(MaterialState.disabled)) return ThemeColor.DisabledColor;
        return null; // Use the default value.
      }),
      padding: MaterialStateProperty.all(ThemeSize.paddingSymmetric(M, S)),
      minimumSize: MaterialStateProperty.all(Size(ThemeSize.buttonWidth(M), ThemeSize.buttonHeight(M))),
    ),
  ),
  inputDecorationTheme: InputDecorationTheme(
      enabledBorder: const UnderlineInputBorder(
        borderSide: BorderSide(color: ThemeColor.Grey, width: 1),
      ),
      errorBorder: const UnderlineInputBorder(
        borderSide: BorderSide(color: ThemeColor.Negative, width: 1),
      ),
      errorStyle: const TextStyle(
        color: ThemeColor.Negative,
      )),
  textTheme: TextTheme(
    overline: TextStyle(
      fontSize: ThemeSize.text(S),
      fontWeight: FontWeight.w500,
    ),
    caption: TextStyle(fontSize: ThemeSize.text(M), fontWeight: FontWeight.w500),
    button: TextStyle(fontSize: ThemeSize.text(M)),
    bodyText1: TextStyle(
      fontSize: ThemeSize.text(M),
    ),
    bodyText2: TextStyle(
      fontSize: ThemeSize.text(S),
    ),
    subtitle1: TextStyle(fontSize: ThemeSize.text(M), fontWeight: FontWeight.bold),
    subtitle2: TextStyle(fontSize: ThemeSize.text(M), fontWeight: FontWeight.normal),
    headline1: TextStyle(
      fontSize: ThemeSize.text(XXXL),
      fontWeight: FontWeight.bold,
    ),
    headline2: TextStyle(
      fontSize: ThemeSize.text(XXL),
      fontWeight: FontWeight.bold,
    ),
    headline3: TextStyle(
      fontSize: ThemeSize.text(XL),
      fontWeight: FontWeight.bold,
    ),
    headline4: TextStyle(
      fontSize: ThemeSize.text(L),
      fontWeight: FontWeight.bold,
    ),
    headline5: TextStyle(
      fontSize: ThemeSize.text(M),
      fontWeight: FontWeight.bold,
    ),
    headline6: TextStyle(
      fontSize: ThemeSize.text(S),
      fontWeight: FontWeight.bold,
    ),
  ),
);
