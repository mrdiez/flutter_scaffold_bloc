// ignore_for_file: non_constant_identifier_names
import 'package:flutter/material.dart';
import 'package:flutter_scaffold_bloc/constants/theme/colors.dart';

import 'common.dart';

ThemeData get DarkTheme => CommonTheme.copyWith(
      accentColor: ThemeColor.Primary,
      brightness: Brightness.dark,
      colorScheme: CommonTheme.colorScheme.copyWith(
        brightness: Brightness.dark,
        primary: ThemeColor.SecondaryVariant,
        primaryVariant: ThemeColor.Secondary,
        secondary: ThemeColor.Primary,
        secondaryVariant: ThemeColor.PrimaryVariant,
        background: ThemeColor.Darkest,
        error: ThemeColor.Negative,
      ),
      inputDecorationTheme: CommonTheme.inputDecorationTheme.copyWith(
          fillColor: ThemeColor.Darkest,
          labelStyle: (CommonTheme.inputDecorationTheme.labelStyle ?? TextStyle()).copyWith(
            color: ThemeColor.Light,
          ),
          focusedBorder: (CommonTheme.inputDecorationTheme.focusedBorder ?? UnderlineInputBorder()).copyWith(
            borderSide: BorderSide(color: ThemeColor.SecondaryVariant, width: 1),
          )),
      primaryColor: ThemeColor.PrimaryVariant,
      primaryColorLight: ThemeColor.PrimaryVariant,
      primaryColorDark: ThemeColor.Primary,
      scaffoldBackgroundColor: ThemeColor.Darkest,
      textSelectionTheme: CommonTheme.textSelectionTheme.copyWith(cursorColor: ThemeColor.Secondary),
      textTheme: CommonTheme.textTheme.copyWith(
        bodyText1: (CommonTheme.textTheme.bodyText1 ?? TextStyle()).copyWith(
          color: ThemeColor.LightGrey,
        ),
        bodyText2: (CommonTheme.textTheme.bodyText2 ?? TextStyle()).copyWith(
          color: ThemeColor.LightGrey,
        ),
        subtitle1: CommonTheme.textTheme.subtitle1?.copyWith(
          color: ThemeColor.Light,
        ),
        subtitle2: CommonTheme.textTheme.subtitle2?.copyWith(
          color: ThemeColor.Light,
        ),
        headline1: (CommonTheme.textTheme.headline1 ?? TextStyle()).copyWith(
          color: ThemeColor.PrimaryVariant,
        ),
        headline2: (CommonTheme.textTheme.headline2 ?? TextStyle()).copyWith(
          color: ThemeColor.PrimaryVariant,
        ),
        headline3: (CommonTheme.textTheme.headline3 ?? TextStyle()).copyWith(
          color: ThemeColor.PrimaryVariant,
        ),
        headline4: (CommonTheme.textTheme.headline4 ?? TextStyle()).copyWith(
          color: ThemeColor.PrimaryVariant,
        ),
        headline5: (CommonTheme.textTheme.headline5 ?? TextStyle()).copyWith(
          color: ThemeColor.PrimaryVariant,
        ),
        headline6: (CommonTheme.textTheme.headline6 ?? TextStyle()).copyWith(
          color: ThemeColor.PrimaryVariant,
        ),
      ),
      toggleableActiveColor: ThemeColor.PrimaryVariant,
      unselectedWidgetColor: ThemeColor.Light,
    );
