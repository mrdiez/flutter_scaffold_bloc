// ignore_for_file: non_constant_identifier_names
import 'package:flutter/material.dart';

import 'colors.dart';
import 'common.dart';

ThemeData get LightTheme => CommonTheme.copyWith(
      accentColor: ThemeColor.Primary,
      brightness: Brightness.light,
      colorScheme: CommonTheme.colorScheme.copyWith(
        brightness: Brightness.light,
        primary: ThemeColor.Primary,
        primaryVariant: ThemeColor.PrimaryVariant,
        secondary: ThemeColor.Secondary,
        secondaryVariant: ThemeColor.SecondaryVariant,
        background: ThemeColor.Lightest,
        error: ThemeColor.Negative,
      ),
      inputDecorationTheme: CommonTheme.inputDecorationTheme.copyWith(
          fillColor: ThemeColor.Lightest,
          labelStyle: (CommonTheme.inputDecorationTheme.labelStyle ?? TextStyle()).copyWith(
            color: ThemeColor.Dark,
          ),
          focusedBorder: (CommonTheme.inputDecorationTheme.focusedBorder ?? UnderlineInputBorder()).copyWith(
            borderSide: BorderSide(color: ThemeColor.Primary, width: 1),
          )),
      primaryColor: ThemeColor.Secondary,
      primaryColorLight: ThemeColor.Secondary,
      primaryColorDark: ThemeColor.SecondaryVariant,
      scaffoldBackgroundColor: ThemeColor.Lightest,
      textSelectionTheme: CommonTheme.textSelectionTheme.copyWith(cursorColor: ThemeColor.PrimaryVariant),
      textTheme: CommonTheme.textTheme.copyWith(
        bodyText1: (CommonTheme.textTheme.bodyText1 ?? TextStyle()).copyWith(
          color: ThemeColor.DarkGrey,
        ),
        bodyText2: (CommonTheme.textTheme.bodyText2 ?? TextStyle()).copyWith(
          color: ThemeColor.DarkGrey,
        ),
        subtitle1: CommonTheme.textTheme.subtitle1?.copyWith(
          color: ThemeColor.Dark,
        ),
        subtitle2: CommonTheme.textTheme.subtitle2?.copyWith(
          color: ThemeColor.Dark,
        ),
        headline1: (CommonTheme.textTheme.headline1 ?? TextStyle()).copyWith(
          color: ThemeColor.Secondary,
        ),
        headline2: (CommonTheme.textTheme.headline2 ?? TextStyle()).copyWith(
          color: ThemeColor.Secondary,
        ),
        headline3: (CommonTheme.textTheme.headline3 ?? TextStyle()).copyWith(
          color: ThemeColor.Secondary,
        ),
        headline4: (CommonTheme.textTheme.headline4 ?? TextStyle()).copyWith(
          color: ThemeColor.Secondary,
        ),
        headline5: (CommonTheme.textTheme.headline5 ?? TextStyle()).copyWith(
          color: ThemeColor.Secondary,
        ),
        headline6: (CommonTheme.textTheme.headline6 ?? TextStyle()).copyWith(
          color: ThemeColor.Secondary,
        ),
      ),
      toggleableActiveColor: ThemeColor.Secondary,
      unselectedWidgetColor: ThemeColor.Dark,
    );
