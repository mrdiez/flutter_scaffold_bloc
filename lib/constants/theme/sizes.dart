import 'dart:math';

import 'package:flutter/material.dart';

abstract class ThemeSize {
  // This constructor prevents instantiation and extension.
  // ignore: unused_element
  ThemeSize._();

  // PADDING

  static const double _PaddingDefaultValue = 20;
  static double paddingValue(Sizing size) => _PaddingDefaultValue * GoldenRatio[size]!;
  static EdgeInsets padding(Sizing size) => EdgeInsets.all(paddingValue(size));
  static EdgeInsets paddingSymmetric(Sizing sizeX, Sizing sizeY) =>
      EdgeInsets.symmetric(horizontal: paddingValue(sizeX), vertical: paddingValue(sizeY));
  static EdgeInsets paddingOnly({Sizing top = Zero, Sizing bottom = Zero, Sizing left = Zero, Sizing right = Zero}) =>
      EdgeInsets.only(
          top: paddingValue(top), bottom: paddingValue(bottom), left: paddingValue(left), right: paddingValue(right));

  // BORDER-RADIUS

  static const double _BorderRadiusDefaultValue = 10;
  static double borderRadiusValue(Sizing? size) => _BorderRadiusDefaultValue * GoldenRatio[size]!;
  static BorderRadius borderRadius(Sizing size) => BorderRadius.all(Radius.circular(borderRadiusValue(size)));
  static BorderRadius borderHorizontal(Sizing left, [Sizing? right]) => BorderRadius.horizontal(
      left: Radius.circular(borderRadiusValue(left)), right: Radius.circular(borderRadiusValue(right ?? left)));
  static BorderRadius borderVertical(Sizing top, [Sizing? bottom]) => BorderRadius.vertical(
      top: Radius.circular(borderRadiusValue(top)), bottom: Radius.circular(borderRadiusValue(bottom ?? top)));
  static BorderRadius borderOnly({Sizing? topLeft, Sizing? topRight, Sizing? bottomLeft, Sizing? bottomRight}) =>
      BorderRadius.only(
          topLeft: Radius.circular(borderRadiusValue(topLeft)),
          topRight: Radius.circular(borderRadiusValue(topRight)),
          bottomLeft: Radius.circular(borderRadiusValue(bottomLeft)),
          bottomRight: Radius.circular(borderRadiusValue(bottomRight)));

  // // TEXT

  static const double _TextDefaultSize = 20;
  static double text(Sizing size) => _TextDefaultSize * GoldenRatio[size]!;

  // BUTTON

  static const double _ButtonDefaultHeight = 50;
  static double buttonHeight(Sizing size) => _ButtonDefaultHeight * GoldenRatio[size]!;

  static const double _ButtonDefaultWidth = 100;
  static double buttonWidth(Sizing size) => _ButtonDefaultWidth * GoldenRatio[size]!;

  // ICON

  static const double _IconDefaultSize = 30;
  static double icon(Sizing size) => _IconDefaultSize * GoldenRatio[size]!;

  // IMAGE

  static const double _ImageDefaultSize = 250;
  static double image(Sizing size) => _ImageDefaultSize * GoldenRatio[size]!;

  // PROGRESS BAR

  static const double _ProgressBarHeight = 5;
  static double progressBarHeight(Sizing size) => _ProgressBarHeight * GoldenRatio[size]!;

  // SCREEN

  static const double _ScreenDefaultWidth = 846;
  static double screenWidth(Sizing size) => _ScreenDefaultWidth * GoldenRatio[size]!;

  /// GOLDEN RATIO
  /// Check this to understand what is golden ratio and why we'll use it:
  /// https://www.invisionapp.com/inside-design/golden-ratio-designers/
  static Map<Sizing, double> GoldenRatio = {
    Sizing.Zero: 0,
    Sizing.XXS: 1 / _Phi(4),
    Sizing.XS: 1 / _Phi(3),
    Sizing.S: 1 / _Phi(2),
    Sizing.M: 1,
    Sizing.L: Phi,
    Sizing.XL: _Phi(2),
    Sizing.XXL: _Phi(3),
    Sizing.XXXL: _Phi(4),
  };

  /// Phi is also known as the golden number
  static double Phi = (1 + sqrt(5)) / 2;

  /// A method that return [Phi] to the power of the given exponent
  static double _Phi(int exponent) => exponent > 1 ? Phi : pow(Phi, exponent).toDouble();
}

enum Sizing { Zero, XXS, XS, S, M, L, XL, XXL, XXXL }

const Zero = Sizing.Zero;
const XXS = Sizing.XXS;
const XS = Sizing.XS;
const S = Sizing.S;
const M = Sizing.M;
const L = Sizing.L;
const XL = Sizing.XL;
const XXL = Sizing.XXL;
const XXXL = Sizing.XXXL;
