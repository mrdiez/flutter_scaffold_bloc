// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static String m0(method) =>
      "Confirm your identity with the code you received by ${method}";

  static String m1(login) => "${login} already exists";

  static String m2(login) => "${login} doesn\'t exist";

  static String m3(nb) => "${nb} characters minimum";

  static String m4(name) => "Welcome ${name}";

  static String m5(password) => "Your new password is ${password}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "_____AUTH_ERRORS_____________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____AUTH_LOGIN______________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____AUTH_RESET_PASSWORD_____________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____AUTH_SIGN_UP____________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____AUTH____________________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____COMMON_ERRORS___________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____COMMON__________________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____HOME____________________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_locale": MessageLookupByLibrary.simpleMessage("en"),
        "cancel": MessageLookupByLibrary.simpleMessage("Cancel"),
        "chooseAMethod": MessageLookupByLibrary.simpleMessage(
            "Choose a method to receive the password reset code"),
        "code": MessageLookupByLibrary.simpleMessage("Code"),
        "confirmPassword":
            MessageLookupByLibrary.simpleMessage("Confirm password"),
        "confirmYourIdentity": m0,
        "darkTheme": MessageLookupByLibrary.simpleMessage("Dark Theme"),
        "email": MessageLookupByLibrary.simpleMessage("Email"),
        "forWhichAccount": MessageLookupByLibrary.simpleMessage(
            "For which account do you want to reset the password?"),
        "forgotPassword":
            MessageLookupByLibrary.simpleMessage("Forgot password?"),
        "incorrectCredentials":
            MessageLookupByLibrary.simpleMessage("Incorrect credentials"),
        "invalidEmail": MessageLookupByLibrary.simpleMessage("Invalid Email"),
        "lightTheme": MessageLookupByLibrary.simpleMessage("Light Theme"),
        "logIn": MessageLookupByLibrary.simpleMessage("Log in"),
        "logOut": MessageLookupByLibrary.simpleMessage("Log out"),
        "loginAlreadyExists": m1,
        "loginDoesNotExist": m2,
        "nbCharactersMinimum": m3,
        "ok": MessageLookupByLibrary.simpleMessage("OK"),
        "password": MessageLookupByLibrary.simpleMessage("Password"),
        "passwordsMustBeIdentical":
            MessageLookupByLibrary.simpleMessage("Passwords must be identical"),
        "pleaseConfirmYourRegistration": MessageLookupByLibrary.simpleMessage(
            "Please confirm your registration by clicking on the link you received by Email"),
        "rememberMe": MessageLookupByLibrary.simpleMessage("Remember me"),
        "resetCodeCantBeSent":
            MessageLookupByLibrary.simpleMessage("Reset code can\'t be sent"),
        "resetCodeInvalid":
            MessageLookupByLibrary.simpleMessage("Reset code is not valid"),
        "signUp": MessageLookupByLibrary.simpleMessage("Sign up"),
        "sms": MessageLookupByLibrary.simpleMessage("SMS"),
        "submit": MessageLookupByLibrary.simpleMessage("Submit"),
        "thisFieldIsRequired":
            MessageLookupByLibrary.simpleMessage("This field is required"),
        "username": MessageLookupByLibrary.simpleMessage("Username"),
        "usernameOrEmail":
            MessageLookupByLibrary.simpleMessage("Username or Email"),
        "welcome": m4,
        "yourNewPassword": m5
      };
}
