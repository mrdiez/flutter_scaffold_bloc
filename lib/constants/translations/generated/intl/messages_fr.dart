// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a fr locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'fr';

  static String m0(method) =>
      "Confirmez votre identité avec le code que vous avez reçu par ${method}";

  static String m1(login) => "${login} existe déjà";

  static String m2(login) => "${login} n\'existe pas";

  static String m3(nb) => "${nb} caractères minimum";

  static String m4(name) => "Bienvenue ${name}";

  static String m5(password) => "Votre nouveau mot de passe est ${password}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "_____AUTH_ERRORS_____________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____AUTH_LOGIN______________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____AUTH_RESET_PASSWORD_____________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____AUTH_SIGN_UP____________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____AUTH____________________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____COMMON_ERRORS___________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____COMMON__________________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____HOME____________________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_locale": MessageLookupByLibrary.simpleMessage("fr"),
        "cancel": MessageLookupByLibrary.simpleMessage("Annuler"),
        "chooseAMethod": MessageLookupByLibrary.simpleMessage(
            "Choisissez une méthode pour recevoir le code de réinitialisation"),
        "code": MessageLookupByLibrary.simpleMessage("Code"),
        "confirmPassword":
            MessageLookupByLibrary.simpleMessage("Confirmez le mot de passe"),
        "confirmYourIdentity": m0,
        "darkTheme": MessageLookupByLibrary.simpleMessage("Thème sombre"),
        "email": MessageLookupByLibrary.simpleMessage("E-mail"),
        "forWhichAccount": MessageLookupByLibrary.simpleMessage(
            "Pour quel compte voulez-vous réinitialiser le mot de passe ?"),
        "forgotPassword":
            MessageLookupByLibrary.simpleMessage("Mot de passe oublié ?"),
        "incorrectCredentials":
            MessageLookupByLibrary.simpleMessage("Identifiants incorrects"),
        "invalidEmail": MessageLookupByLibrary.simpleMessage("E-mail invalide"),
        "lightTheme": MessageLookupByLibrary.simpleMessage("Thème clair"),
        "logIn": MessageLookupByLibrary.simpleMessage("Connexion"),
        "logOut": MessageLookupByLibrary.simpleMessage("Déconnexion"),
        "loginAlreadyExists": m1,
        "loginDoesNotExist": m2,
        "nbCharactersMinimum": m3,
        "ok": MessageLookupByLibrary.simpleMessage("OK"),
        "password": MessageLookupByLibrary.simpleMessage("Mot de passe"),
        "passwordsMustBeIdentical": MessageLookupByLibrary.simpleMessage(
            "Les mots de passe doivent être identiques"),
        "pleaseConfirmYourRegistration": MessageLookupByLibrary.simpleMessage(
            "Merci de confirmer votre inscription en cliquant sur le lien qui vous a été envoyé par e-mail"),
        "rememberMe":
            MessageLookupByLibrary.simpleMessage("Se souvenir de moi"),
        "resetCodeCantBeSent": MessageLookupByLibrary.simpleMessage(
            "Le code de réinitialisation n\'a pas pu être envoyé"),
        "resetCodeInvalid": MessageLookupByLibrary.simpleMessage(
            "Le code de réinitialisation est invalide"),
        "signUp": MessageLookupByLibrary.simpleMessage("Inscription"),
        "sms": MessageLookupByLibrary.simpleMessage("SMS"),
        "submit": MessageLookupByLibrary.simpleMessage("Envoyer"),
        "thisFieldIsRequired":
            MessageLookupByLibrary.simpleMessage("Ce champs est obligatoire"),
        "username": MessageLookupByLibrary.simpleMessage("Nom d\'utilisateur"),
        "usernameOrEmail": MessageLookupByLibrary.simpleMessage(
            "Nom d\'utilisateur ou e-mail"),
        "welcome": m4,
        "yourNewPassword": m5
      };
}
