// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class Translation {
  Translation();

  static Translation? _current;

  static Translation get current {
    assert(_current != null,
        'No instance of Translation was loaded. Try to initialize the Translation delegate before accessing Translation.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<Translation> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = Translation();
      Translation._current = instance;

      return instance;
    });
  }

  static Translation of(BuildContext context) {
    final instance = Translation.maybeOf(context);
    assert(instance != null,
        'No instance of Translation present in the widget tree. Did you add Translation.delegate in localizationsDelegates?');
    return instance!;
  }

  static Translation? maybeOf(BuildContext context) {
    return Localizations.of<Translation>(context, Translation);
  }

  /// `en`
  String get _locale {
    return Intl.message(
      'en',
      name: '_locale',
      desc: '',
      args: [],
    );
  }

  /// ``
  String get _____COMMON__________________________________ {
    return Intl.message(
      '',
      name: '_____COMMON__________________________________',
      desc: '',
      args: [],
    );
  }

  /// `Cancel`
  String get cancel {
    return Intl.message(
      'Cancel',
      name: 'cancel',
      desc: '',
      args: [],
    );
  }

  /// `Submit`
  String get submit {
    return Intl.message(
      'Submit',
      name: 'submit',
      desc: '',
      args: [],
    );
  }

  /// `OK`
  String get ok {
    return Intl.message(
      'OK',
      name: 'ok',
      desc: '',
      args: [],
    );
  }

  /// ``
  String get _____COMMON_ERRORS___________________________ {
    return Intl.message(
      '',
      name: '_____COMMON_ERRORS___________________________',
      desc: '',
      args: [],
    );
  }

  /// `{nb} characters minimum`
  String nbCharactersMinimum(Object nb) {
    return Intl.message(
      '$nb characters minimum',
      name: 'nbCharactersMinimum',
      desc: '',
      args: [nb],
    );
  }

  /// `This field is required`
  String get thisFieldIsRequired {
    return Intl.message(
      'This field is required',
      name: 'thisFieldIsRequired',
      desc: '',
      args: [],
    );
  }

  /// ``
  String get _____AUTH____________________________________ {
    return Intl.message(
      '',
      name: '_____AUTH____________________________________',
      desc: '',
      args: [],
    );
  }

  /// `Username`
  String get username {
    return Intl.message(
      'Username',
      name: 'username',
      desc: '',
      args: [],
    );
  }

  /// `Email`
  String get email {
    return Intl.message(
      'Email',
      name: 'email',
      desc: '',
      args: [],
    );
  }

  /// `Username or Email`
  String get usernameOrEmail {
    return Intl.message(
      'Username or Email',
      name: 'usernameOrEmail',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get password {
    return Intl.message(
      'Password',
      name: 'password',
      desc: '',
      args: [],
    );
  }

  /// `Forgot password?`
  String get forgotPassword {
    return Intl.message(
      'Forgot password?',
      name: 'forgotPassword',
      desc: '',
      args: [],
    );
  }

  /// `Sign up`
  String get signUp {
    return Intl.message(
      'Sign up',
      name: 'signUp',
      desc: '',
      args: [],
    );
  }

  /// ``
  String get _____AUTH_ERRORS_____________________________ {
    return Intl.message(
      '',
      name: '_____AUTH_ERRORS_____________________________',
      desc: '',
      args: [],
    );
  }

  /// `Invalid Email`
  String get invalidEmail {
    return Intl.message(
      'Invalid Email',
      name: 'invalidEmail',
      desc: '',
      args: [],
    );
  }

  /// `Passwords must be identical`
  String get passwordsMustBeIdentical {
    return Intl.message(
      'Passwords must be identical',
      name: 'passwordsMustBeIdentical',
      desc: '',
      args: [],
    );
  }

  /// `Incorrect credentials`
  String get incorrectCredentials {
    return Intl.message(
      'Incorrect credentials',
      name: 'incorrectCredentials',
      desc: '',
      args: [],
    );
  }

  /// `{login} doesn't exist`
  String loginDoesNotExist(Object login) {
    return Intl.message(
      '$login doesn\'t exist',
      name: 'loginDoesNotExist',
      desc: '',
      args: [login],
    );
  }

  /// `Reset code can't be sent`
  String get resetCodeCantBeSent {
    return Intl.message(
      'Reset code can\'t be sent',
      name: 'resetCodeCantBeSent',
      desc: '',
      args: [],
    );
  }

  /// `Reset code is not valid`
  String get resetCodeInvalid {
    return Intl.message(
      'Reset code is not valid',
      name: 'resetCodeInvalid',
      desc: '',
      args: [],
    );
  }

  /// `{login} already exists`
  String loginAlreadyExists(Object login) {
    return Intl.message(
      '$login already exists',
      name: 'loginAlreadyExists',
      desc: '',
      args: [login],
    );
  }

  /// ``
  String get _____AUTH_LOGIN______________________________ {
    return Intl.message(
      '',
      name: '_____AUTH_LOGIN______________________________',
      desc: '',
      args: [],
    );
  }

  /// `Log in`
  String get logIn {
    return Intl.message(
      'Log in',
      name: 'logIn',
      desc: '',
      args: [],
    );
  }

  /// `Remember me`
  String get rememberMe {
    return Intl.message(
      'Remember me',
      name: 'rememberMe',
      desc: '',
      args: [],
    );
  }

  /// ``
  String get _____AUTH_RESET_PASSWORD_____________________ {
    return Intl.message(
      '',
      name: '_____AUTH_RESET_PASSWORD_____________________',
      desc: '',
      args: [],
    );
  }

  /// `Code`
  String get code {
    return Intl.message(
      'Code',
      name: 'code',
      desc: '',
      args: [],
    );
  }

  /// `SMS`
  String get sms {
    return Intl.message(
      'SMS',
      name: 'sms',
      desc: '',
      args: [],
    );
  }

  /// `For which account do you want to reset the password?`
  String get forWhichAccount {
    return Intl.message(
      'For which account do you want to reset the password?',
      name: 'forWhichAccount',
      desc: '',
      args: [],
    );
  }

  /// `Choose a method to receive the password reset code`
  String get chooseAMethod {
    return Intl.message(
      'Choose a method to receive the password reset code',
      name: 'chooseAMethod',
      desc: '',
      args: [],
    );
  }

  /// `Confirm your identity with the code you received by {method}`
  String confirmYourIdentity(Object method) {
    return Intl.message(
      'Confirm your identity with the code you received by $method',
      name: 'confirmYourIdentity',
      desc: '',
      args: [method],
    );
  }

  /// `Your new password is {password}`
  String yourNewPassword(Object password) {
    return Intl.message(
      'Your new password is $password',
      name: 'yourNewPassword',
      desc: '',
      args: [password],
    );
  }

  /// ``
  String get _____AUTH_SIGN_UP____________________________ {
    return Intl.message(
      '',
      name: '_____AUTH_SIGN_UP____________________________',
      desc: '',
      args: [],
    );
  }

  /// `Confirm password`
  String get confirmPassword {
    return Intl.message(
      'Confirm password',
      name: 'confirmPassword',
      desc: '',
      args: [],
    );
  }

  /// `Please confirm your registration by clicking on the link you received by Email`
  String get pleaseConfirmYourRegistration {
    return Intl.message(
      'Please confirm your registration by clicking on the link you received by Email',
      name: 'pleaseConfirmYourRegistration',
      desc: '',
      args: [],
    );
  }

  /// ``
  String get _____HOME____________________________________ {
    return Intl.message(
      '',
      name: '_____HOME____________________________________',
      desc: '',
      args: [],
    );
  }

  /// `Welcome {name}`
  String welcome(Object name) {
    return Intl.message(
      'Welcome $name',
      name: 'welcome',
      desc: '',
      args: [name],
    );
  }

  /// `Light Theme`
  String get lightTheme {
    return Intl.message(
      'Light Theme',
      name: 'lightTheme',
      desc: '',
      args: [],
    );
  }

  /// `Dark Theme`
  String get darkTheme {
    return Intl.message(
      'Dark Theme',
      name: 'darkTheme',
      desc: '',
      args: [],
    );
  }

  /// `Log out`
  String get logOut {
    return Intl.message(
      'Log out',
      name: 'logOut',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<Translation> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'fr'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<Translation> load(Locale locale) => Translation.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
