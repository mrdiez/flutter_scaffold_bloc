import 'package:freezed_annotation/freezed_annotation.dart';

part 'user.freezed.dart';
part 'user.g.dart';

/// An immutable generated class model
/// To generate code execute: flutter pub run build_runner build
/// For more info check the README # How to generate code (with freezed)
@freezed
class User with _$User {
  const factory User(
      {required int id,
      required String username,
      String? firstname,
      String? lastname,
      required String email,
      String? phone}) = UserInit;
  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
}
