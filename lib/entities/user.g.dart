// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$UserInit _$_$UserInitFromJson(Map<String, dynamic> json) {
  return _$UserInit(
    id: json['id'] as int,
    username: json['username'] as String,
    firstname: json['firstname'] as String?,
    lastname: json['lastname'] as String?,
    email: json['email'] as String,
    phone: json['phone'] as String?,
  );
}

Map<String, dynamic> _$_$UserInitToJson(_$UserInit instance) =>
    <String, dynamic>{
      'id': instance.id,
      'username': instance.username,
      'firstname': instance.firstname,
      'lastname': instance.lastname,
      'email': instance.email,
      'phone': instance.phone,
    };
