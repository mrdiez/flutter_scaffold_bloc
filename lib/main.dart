import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_scaffold_bloc/constants/routes.dart';
import 'package:flutter_scaffold_bloc/constants/theme/dark.dart';
import 'package:flutter_scaffold_bloc/constants/theme/light.dart';
import 'package:flutter_scaffold_bloc/constants/translations/generated/l10n.dart';
import 'package:flutter_scaffold_bloc/services/authenticator.dart';
import 'package:flutter_scaffold_bloc/services/bloc_observer.dart';
import 'package:flutter_scaffold_bloc/services/task_manager.dart';
import 'package:flutter_scaffold_bloc/services/theme_manager.dart';
import 'package:flutter_scaffold_bloc/widgets/layout/root_container.dart';

void main() {
  Bloc.observer = CustomBlocObserver();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp();
  static final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider(create: (_) => ThemeManager()),
          BlocProvider(create: (_) => Authenticator()),
          BlocProvider(create: (_) => TaskManager()),
        ],
        child: BlocBuilder<ThemeManager, ThemeManagerModel>(
            buildWhen: (previous, next) => previous != next,
            builder: (_, themeModel) => BlocListener<Authenticator, AuthenticatorState>(
                listener: (_, state) {
                  state.maybeMap(
                      authenticated: (_) => navigatorKey.currentState?.pushNamedAndRemoveUntil(HomeRoute, (_) => false),
                      unauthenticated: (_) =>
                          navigatorKey.currentState?.pushNamedAndRemoveUntil(LoginRoute, (_) => false),
                      orElse: () {});
                },
                child: MaterialApp(
                  title: "Mr#'s scaffold",
                  theme: LightTheme,
                  darkTheme: DarkTheme,
                  themeMode: themeModel.mode,
                  localizationsDelegates: [
                    Translation.delegate,
                    GlobalMaterialLocalizations.delegate,
                    GlobalWidgetsLocalizations.delegate,
                    GlobalCupertinoLocalizations.delegate,
                  ],
                  supportedLocales: Translation.delegate.supportedLocales,
                  initialRoute: MyRoutes.keys.first,
                  navigatorKey: navigatorKey,
                  routes: MyRoutes.map((name, widget) => MapEntry(name, (_) => widget)),
                  builder: (_, page) => RootContainer(child: page!),
                ))));
  }
}
