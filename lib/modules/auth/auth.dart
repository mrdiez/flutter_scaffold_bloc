import 'package:flutter/material.dart';
import 'package:flutter_scaffold_bloc/constants/theme/sizes.dart';
import 'package:flutter_scaffold_bloc/modules/auth/sign_up/ui/sign_up.dart';
import 'package:flutter_scaffold_bloc/widgets/layout/sliver.view.dart';
import 'package:flutter_scaffold_bloc/widgets/misc/custom_navigator.dart';

import 'login/ui/login.dart';
import 'reset_password/ui/reset_password.dart';

class AuthModule extends StatelessWidget {
  const AuthModule();

  static GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  static const String LoginRoute = '/';
  static const String ResetPasswordRoute = '/reset_password';
  static const String SignUpRoute = '/sign_route';

  @override
  Widget build(BuildContext context) => SliverView(
        physics: NeverScrollableScrollPhysics(),
        fillRemaining: true,
        child: Padding(
            padding: ThemeSize.paddingOnly(left: M, top: M, right: M),
            child: CustomNavigator(navigatorKey: navigatorKey, routes: {
              LoginRoute: (_) => LoginView(),
              ResetPasswordRoute: (_) => ResetPasswordView(),
              SignUpRoute: (_) => SignUpView(),
            })),
      );
}
