import 'package:dio/dio.dart';
import 'package:flutter_scaffold_bloc/constants/config/environment.dart';

class AuthProvider {
  const AuthProvider();

  Future<Map<String, dynamic>?> authenticate(String login, String password) async {
    var response =
        await Dio().get('${Environment.HttpEndpoint}/user', queryParameters: {'login': login, 'password': password});
    return response.statusCode == 200 ? response.data : null;
  }

  Future<Map<String, List<dynamic>>?> resetPasswordMethods(String login) async {
    var response =
        await Dio().get('${Environment.HttpEndpoint}/reset_password_methods', queryParameters: {'login': login});
    return response.statusCode == 200 ? response.data : null;
  }

  Future<bool?> postResetPasswordMethod(String login, String method) async {
    var response = await Dio().get('${Environment.HttpEndpoint}/post_reset_password_method',
        queryParameters: {'login': login, 'method': method});
    return response.statusCode == 200 ? response.data : null;
  }

  Future<String?> resetPassword(String login, String code) async {
    var response =
        await Dio().get('${Environment.HttpEndpoint}/reset_password', queryParameters: {'login': login, 'code': code});
    return response.statusCode == 200 ? response.data : null;
  }

  Future<String?> signUp(String username, String email, String password) async {
    var response = await Dio().get('${Environment.HttpEndpoint}/sign_up',
        queryParameters: {'username': username, 'email': email, 'password': password});
    return response.statusCode == 200 ? response.data : null;
  }
}
