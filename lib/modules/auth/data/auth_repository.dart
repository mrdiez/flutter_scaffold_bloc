import 'package:flutter_scaffold_bloc/constants/config/environment.dart';
import 'package:flutter_scaffold_bloc/constants/mocks/providers/auth.dart';
import 'package:flutter_scaffold_bloc/constants/translations/generated/l10n.dart';
import 'package:flutter_scaffold_bloc/entities/user.dart';
import 'package:flutter_scaffold_bloc/modules/auth/login/bloc/login_model.dart';
import 'package:flutter_scaffold_bloc/modules/auth/reset_password/bloc/reset_password_bloc.dart';
import 'package:flutter_scaffold_bloc/modules/auth/reset_password/bloc/reset_password_model.dart';
import 'package:flutter_scaffold_bloc/modules/auth/sign_up/bloc/sign_up_model.dart';

import 'auth_provider.dart';

class AuthRepository {
  const AuthRepository();
  final AuthProvider _mock = const MockedAuthProvider();
  final AuthProvider _provider = const AuthProvider();
  AuthProvider get provider => Environment.Mock ? _mock : _provider;

  static const Map<String, ResetPasswordMethod> MethodsByParams = {
    'email': ResetPasswordMethod.Email,
    'phone': ResetPasswordMethod.SMS
  };
  static Map<ResetPasswordMethod, String> get ParamsByMethods =>
      MethodsByParams.map((method, param) => MapEntry(param, method));

  Future<User> getUser(LoginModel model) async {
    var data = await provider.authenticate(model.login!, model.password!);
    if (data == null) throw Exception(Translation.current.incorrectCredentials);
    return User.fromJson(data);
  }

  Future<List<ResetPasswordMethod>> resetPasswordMethods(ResetPasswordModel model) async {
    var data = await provider.resetPasswordMethods(model.login!);
    if (data == null || data['methods'] == null || (data['methods'] as List).isEmpty) {
      throw Exception(Translation.current.loginDoesNotExist(model.login!));
    }
    return (data['methods'] as List).map((method) => MethodsByParams[method]!).toList();
  }

  Future<void> postResetPasswordMethod(ResetPasswordModel model) async {
    var data = await provider.postResetPasswordMethod(model.login!, ParamsByMethods[model.method]!);
    if (data == null || !data) throw Exception(Translation.current.resetCodeCantBeSent);
  }

  Future<String> resetPassword(ResetPasswordModel model) async {
    var data = await provider.resetPassword(model.login!, model.code!);
    if (data == null || data.isEmpty) throw Exception(Translation.current.resetCodeInvalid);
    return data;
  }

  Future<void> signUp(SignUpModel model) async {
    var data = await provider.signUp(model.username!, model.email!, model.password!);
    if (data != null && data.isNotEmpty) throw Exception(Translation.current.loginAlreadyExists(data));
  }
}
