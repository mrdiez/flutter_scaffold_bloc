import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_scaffold_bloc/constants/config/logger.dart';
import 'package:flutter_scaffold_bloc/constants/keys.dart';
import 'package:flutter_scaffold_bloc/constants/translations/generated/l10n.dart';
import 'package:flutter_scaffold_bloc/modules/auth/data/auth_repository.dart';
import 'package:flutter_scaffold_bloc/modules/auth/login/bloc/login_event.dart';
import 'package:flutter_scaffold_bloc/modules/auth/login/bloc/login_model.dart';
import 'package:flutter_scaffold_bloc/modules/auth/login/bloc/login_state.dart';
import 'package:flutter_scaffold_bloc/services/authenticator.dart';
import 'package:flutter_scaffold_bloc/services/local_storage.dart';
import 'package:flutter_scaffold_bloc/services/task_manager.dart';
import 'package:flutter_scaffold_bloc/utils/string.dart';
import 'package:flutter_scaffold_bloc/widgets/misc/custom_bloc.dart';

/// A [Bloc] class that emit states when receiving events
/// Check this to understand what is a [Bloc] and why we'll use it:
/// https://bloclibrary.dev/#/coreconcepts?id=bloc
class LoginBloc extends CustomBloc<LoginEvent, LoginState> {
  LoginBloc()
      : assert(LogByUsername || LogByEmail),
        super(LoginState.init()) {
    _retrieveCredentials();
  }

  static const bool LogByUsername = true;
  static const bool LogByEmail = true;
  final AuthRepository _repository = const AuthRepository();

  static String get loginLabel {
    if (LogByUsername && !LogByEmail) return Translation.current.username;
    if (!LogByUsername && LogByEmail) return Translation.current.email;
    return Translation.current.usernameOrEmail;
  }

  static String? loginValidator(String? login, [bool isSubmit = false]) =>
      (!LogByUsername && LogByEmail ? emailValidator : usernameValidator)(login, isSubmit);

  static String? usernameValidator(String? username, [bool isSubmit = false]) {
    if (!StringTool.notNull(username) && isSubmit) return Translation.current.thisFieldIsRequired;
    if (StringTool.notNull(username) && username!.length < 4) {
      return Translation.current.nbCharactersMinimum(4);
    }
  }

  static String? emailValidator(String? email, [bool isSubmit = false]) {
    if (!StringTool.notNull(email) && isSubmit) return Translation.current.thisFieldIsRequired;
    if (StringTool.notNull(email) && !email!.isEmail) {
      return Translation.current.invalidEmail;
    }
  }

  static String? passwordValidator(String? password, [bool isSubmit = false]) {
    if (!StringTool.notNull(password) && isSubmit) return Translation.current.thisFieldIsRequired;
    if (StringTool.notNull(password) && password!.length < 4) {
      return Translation.current.nbCharactersMinimum(4);
    }
  }

  LoginModel _validate(LoginModel model, [bool isSubmit = false]) => model.copyWith(
        loginError: loginValidator(model.login, isSubmit),
        passwordError: passwordValidator(model.password, isSubmit),
      );

  void _retrieveCredentials() {
    LocalStorage.get(StorageKey.RememberMe).then((checked) async {
      if (checked != null && checked) {
        var model = LoginModel(
          rememberMe: checked,
          login: await LocalStorage.get(StorageKey.Login),
          password: await LocalStorage.get(StorageKey.Password),
        );
        add(LoginEvent.getStoredCredentials(model));
      }
    });
  }

  Future<void> _storeCredentials(LoginModel model) async {
    if (model.rememberMe) {
      await LocalStorage.storeAll(
          {StorageKey.RememberMe: true, StorageKey.Login: model.login, StorageKey.Password: model.password});
    } else {
      await LocalStorage.store(StorageKey.RememberMe, false);
    }
  }

  Stream<LoginState> _onSubmit(LoginModel model) async* {
    var result = _validate(model, true);
    try {
      if (result.isError) {
        yield LoginState.submitError(result);
      } else {
        yield LoginState.loading(result);
        taskManager.add(TaskManagerEvent.startLoading(LoginSubmit));
        var user = await _repository.getUser(model);
        await _storeCredentials(result);
        yield LoginState.success();
        taskManager.add(TaskManagerEvent.stopLoading(LoginSubmit));
        authenticator.add(AuthenticatorEvent.loggedIn(user));
      }
    } catch (error) {
      yield LoginState.submitError(result.copyWith(passwordError: error.toString().removeException));
      taskManager.add(TaskManagerEvent.stopLoading(LoginSubmit));
    }
  }

  Stream<LoginState> _onTyping(LoginModel model) async* {
    var result = _validate(model.copyWith(login: model.login, password: model.password));
    yield result.isError ? LoginState.typingError(result) : LoginState.init(result);
  }

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) => event.maybeWhen(
        getStoredCredentials: (model) async* {
          yield LoginState.ready(model);
        },
        submit: _onSubmit,
        typing: _onTyping,
        orElse: () async* {
          yield LoginState.init();
        },
      );

  @override
  void onError(Object error, StackTrace stackTrace) {
    Log.e('LoginBloc onError', error, stackTrace);
    super.onError(error, stackTrace);
  }
}
