import 'package:flutter_scaffold_bloc/modules/auth/login/bloc/login_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'login_event.freezed.dart';

/// An immutable generated class that define BLOC events
/// To generate code execute: flutter pub run build_runner build
/// For more info check the README # How to generate code (with freezed)
@freezed
class LoginEvent with _$LoginEvent {
  const factory LoginEvent.submit(LoginModel model) = LoginSubmit;
  const factory LoginEvent.typing(LoginModel model) = LoginTyping;
  const factory LoginEvent.getStoredCredentials(LoginModel model) = LoginGetStoredCredentials;
  const factory LoginEvent.forgotPasswordClick() = LoginForgotPassword;
  const factory LoginEvent.signUpClick() = LoginSignUp;
}
