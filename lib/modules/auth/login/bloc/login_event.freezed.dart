// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'login_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$LoginEventTearOff {
  const _$LoginEventTearOff();

  LoginSubmit submit(LoginModel model) {
    return LoginSubmit(
      model,
    );
  }

  LoginTyping typing(LoginModel model) {
    return LoginTyping(
      model,
    );
  }

  LoginGetStoredCredentials getStoredCredentials(LoginModel model) {
    return LoginGetStoredCredentials(
      model,
    );
  }

  LoginForgotPassword forgotPasswordClick() {
    return const LoginForgotPassword();
  }

  LoginSignUp signUpClick() {
    return const LoginSignUp();
  }
}

/// @nodoc
const $LoginEvent = _$LoginEventTearOff();

/// @nodoc
mixin _$LoginEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(LoginModel model) submit,
    required TResult Function(LoginModel model) typing,
    required TResult Function(LoginModel model) getStoredCredentials,
    required TResult Function() forgotPasswordClick,
    required TResult Function() signUpClick,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(LoginModel model)? submit,
    TResult Function(LoginModel model)? typing,
    TResult Function(LoginModel model)? getStoredCredentials,
    TResult Function()? forgotPasswordClick,
    TResult Function()? signUpClick,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginSubmit value) submit,
    required TResult Function(LoginTyping value) typing,
    required TResult Function(LoginGetStoredCredentials value)
        getStoredCredentials,
    required TResult Function(LoginForgotPassword value) forgotPasswordClick,
    required TResult Function(LoginSignUp value) signUpClick,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginSubmit value)? submit,
    TResult Function(LoginTyping value)? typing,
    TResult Function(LoginGetStoredCredentials value)? getStoredCredentials,
    TResult Function(LoginForgotPassword value)? forgotPasswordClick,
    TResult Function(LoginSignUp value)? signUpClick,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LoginEventCopyWith<$Res> {
  factory $LoginEventCopyWith(
          LoginEvent value, $Res Function(LoginEvent) then) =
      _$LoginEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$LoginEventCopyWithImpl<$Res> implements $LoginEventCopyWith<$Res> {
  _$LoginEventCopyWithImpl(this._value, this._then);

  final LoginEvent _value;
  // ignore: unused_field
  final $Res Function(LoginEvent) _then;
}

/// @nodoc
abstract class $LoginSubmitCopyWith<$Res> {
  factory $LoginSubmitCopyWith(
          LoginSubmit value, $Res Function(LoginSubmit) then) =
      _$LoginSubmitCopyWithImpl<$Res>;
  $Res call({LoginModel model});

  $LoginModelCopyWith<$Res> get model;
}

/// @nodoc
class _$LoginSubmitCopyWithImpl<$Res> extends _$LoginEventCopyWithImpl<$Res>
    implements $LoginSubmitCopyWith<$Res> {
  _$LoginSubmitCopyWithImpl(
      LoginSubmit _value, $Res Function(LoginSubmit) _then)
      : super(_value, (v) => _then(v as LoginSubmit));

  @override
  LoginSubmit get _value => super._value as LoginSubmit;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(LoginSubmit(
      model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as LoginModel,
    ));
  }

  @override
  $LoginModelCopyWith<$Res> get model {
    return $LoginModelCopyWith<$Res>(_value.model, (value) {
      return _then(_value.copyWith(model: value));
    });
  }
}

/// @nodoc

class _$LoginSubmit implements LoginSubmit {
  const _$LoginSubmit(this.model);

  @override
  final LoginModel model;

  @override
  String toString() {
    return 'LoginEvent.submit(model: $model)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is LoginSubmit &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(model);

  @JsonKey(ignore: true)
  @override
  $LoginSubmitCopyWith<LoginSubmit> get copyWith =>
      _$LoginSubmitCopyWithImpl<LoginSubmit>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(LoginModel model) submit,
    required TResult Function(LoginModel model) typing,
    required TResult Function(LoginModel model) getStoredCredentials,
    required TResult Function() forgotPasswordClick,
    required TResult Function() signUpClick,
  }) {
    return submit(model);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(LoginModel model)? submit,
    TResult Function(LoginModel model)? typing,
    TResult Function(LoginModel model)? getStoredCredentials,
    TResult Function()? forgotPasswordClick,
    TResult Function()? signUpClick,
    required TResult orElse(),
  }) {
    if (submit != null) {
      return submit(model);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginSubmit value) submit,
    required TResult Function(LoginTyping value) typing,
    required TResult Function(LoginGetStoredCredentials value)
        getStoredCredentials,
    required TResult Function(LoginForgotPassword value) forgotPasswordClick,
    required TResult Function(LoginSignUp value) signUpClick,
  }) {
    return submit(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginSubmit value)? submit,
    TResult Function(LoginTyping value)? typing,
    TResult Function(LoginGetStoredCredentials value)? getStoredCredentials,
    TResult Function(LoginForgotPassword value)? forgotPasswordClick,
    TResult Function(LoginSignUp value)? signUpClick,
    required TResult orElse(),
  }) {
    if (submit != null) {
      return submit(this);
    }
    return orElse();
  }
}

abstract class LoginSubmit implements LoginEvent {
  const factory LoginSubmit(LoginModel model) = _$LoginSubmit;

  LoginModel get model => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $LoginSubmitCopyWith<LoginSubmit> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LoginTypingCopyWith<$Res> {
  factory $LoginTypingCopyWith(
          LoginTyping value, $Res Function(LoginTyping) then) =
      _$LoginTypingCopyWithImpl<$Res>;
  $Res call({LoginModel model});

  $LoginModelCopyWith<$Res> get model;
}

/// @nodoc
class _$LoginTypingCopyWithImpl<$Res> extends _$LoginEventCopyWithImpl<$Res>
    implements $LoginTypingCopyWith<$Res> {
  _$LoginTypingCopyWithImpl(
      LoginTyping _value, $Res Function(LoginTyping) _then)
      : super(_value, (v) => _then(v as LoginTyping));

  @override
  LoginTyping get _value => super._value as LoginTyping;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(LoginTyping(
      model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as LoginModel,
    ));
  }

  @override
  $LoginModelCopyWith<$Res> get model {
    return $LoginModelCopyWith<$Res>(_value.model, (value) {
      return _then(_value.copyWith(model: value));
    });
  }
}

/// @nodoc

class _$LoginTyping implements LoginTyping {
  const _$LoginTyping(this.model);

  @override
  final LoginModel model;

  @override
  String toString() {
    return 'LoginEvent.typing(model: $model)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is LoginTyping &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(model);

  @JsonKey(ignore: true)
  @override
  $LoginTypingCopyWith<LoginTyping> get copyWith =>
      _$LoginTypingCopyWithImpl<LoginTyping>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(LoginModel model) submit,
    required TResult Function(LoginModel model) typing,
    required TResult Function(LoginModel model) getStoredCredentials,
    required TResult Function() forgotPasswordClick,
    required TResult Function() signUpClick,
  }) {
    return typing(model);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(LoginModel model)? submit,
    TResult Function(LoginModel model)? typing,
    TResult Function(LoginModel model)? getStoredCredentials,
    TResult Function()? forgotPasswordClick,
    TResult Function()? signUpClick,
    required TResult orElse(),
  }) {
    if (typing != null) {
      return typing(model);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginSubmit value) submit,
    required TResult Function(LoginTyping value) typing,
    required TResult Function(LoginGetStoredCredentials value)
        getStoredCredentials,
    required TResult Function(LoginForgotPassword value) forgotPasswordClick,
    required TResult Function(LoginSignUp value) signUpClick,
  }) {
    return typing(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginSubmit value)? submit,
    TResult Function(LoginTyping value)? typing,
    TResult Function(LoginGetStoredCredentials value)? getStoredCredentials,
    TResult Function(LoginForgotPassword value)? forgotPasswordClick,
    TResult Function(LoginSignUp value)? signUpClick,
    required TResult orElse(),
  }) {
    if (typing != null) {
      return typing(this);
    }
    return orElse();
  }
}

abstract class LoginTyping implements LoginEvent {
  const factory LoginTyping(LoginModel model) = _$LoginTyping;

  LoginModel get model => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $LoginTypingCopyWith<LoginTyping> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LoginGetStoredCredentialsCopyWith<$Res> {
  factory $LoginGetStoredCredentialsCopyWith(LoginGetStoredCredentials value,
          $Res Function(LoginGetStoredCredentials) then) =
      _$LoginGetStoredCredentialsCopyWithImpl<$Res>;
  $Res call({LoginModel model});

  $LoginModelCopyWith<$Res> get model;
}

/// @nodoc
class _$LoginGetStoredCredentialsCopyWithImpl<$Res>
    extends _$LoginEventCopyWithImpl<$Res>
    implements $LoginGetStoredCredentialsCopyWith<$Res> {
  _$LoginGetStoredCredentialsCopyWithImpl(LoginGetStoredCredentials _value,
      $Res Function(LoginGetStoredCredentials) _then)
      : super(_value, (v) => _then(v as LoginGetStoredCredentials));

  @override
  LoginGetStoredCredentials get _value =>
      super._value as LoginGetStoredCredentials;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(LoginGetStoredCredentials(
      model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as LoginModel,
    ));
  }

  @override
  $LoginModelCopyWith<$Res> get model {
    return $LoginModelCopyWith<$Res>(_value.model, (value) {
      return _then(_value.copyWith(model: value));
    });
  }
}

/// @nodoc

class _$LoginGetStoredCredentials implements LoginGetStoredCredentials {
  const _$LoginGetStoredCredentials(this.model);

  @override
  final LoginModel model;

  @override
  String toString() {
    return 'LoginEvent.getStoredCredentials(model: $model)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is LoginGetStoredCredentials &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(model);

  @JsonKey(ignore: true)
  @override
  $LoginGetStoredCredentialsCopyWith<LoginGetStoredCredentials> get copyWith =>
      _$LoginGetStoredCredentialsCopyWithImpl<LoginGetStoredCredentials>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(LoginModel model) submit,
    required TResult Function(LoginModel model) typing,
    required TResult Function(LoginModel model) getStoredCredentials,
    required TResult Function() forgotPasswordClick,
    required TResult Function() signUpClick,
  }) {
    return getStoredCredentials(model);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(LoginModel model)? submit,
    TResult Function(LoginModel model)? typing,
    TResult Function(LoginModel model)? getStoredCredentials,
    TResult Function()? forgotPasswordClick,
    TResult Function()? signUpClick,
    required TResult orElse(),
  }) {
    if (getStoredCredentials != null) {
      return getStoredCredentials(model);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginSubmit value) submit,
    required TResult Function(LoginTyping value) typing,
    required TResult Function(LoginGetStoredCredentials value)
        getStoredCredentials,
    required TResult Function(LoginForgotPassword value) forgotPasswordClick,
    required TResult Function(LoginSignUp value) signUpClick,
  }) {
    return getStoredCredentials(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginSubmit value)? submit,
    TResult Function(LoginTyping value)? typing,
    TResult Function(LoginGetStoredCredentials value)? getStoredCredentials,
    TResult Function(LoginForgotPassword value)? forgotPasswordClick,
    TResult Function(LoginSignUp value)? signUpClick,
    required TResult orElse(),
  }) {
    if (getStoredCredentials != null) {
      return getStoredCredentials(this);
    }
    return orElse();
  }
}

abstract class LoginGetStoredCredentials implements LoginEvent {
  const factory LoginGetStoredCredentials(LoginModel model) =
      _$LoginGetStoredCredentials;

  LoginModel get model => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $LoginGetStoredCredentialsCopyWith<LoginGetStoredCredentials> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LoginForgotPasswordCopyWith<$Res> {
  factory $LoginForgotPasswordCopyWith(
          LoginForgotPassword value, $Res Function(LoginForgotPassword) then) =
      _$LoginForgotPasswordCopyWithImpl<$Res>;
}

/// @nodoc
class _$LoginForgotPasswordCopyWithImpl<$Res>
    extends _$LoginEventCopyWithImpl<$Res>
    implements $LoginForgotPasswordCopyWith<$Res> {
  _$LoginForgotPasswordCopyWithImpl(
      LoginForgotPassword _value, $Res Function(LoginForgotPassword) _then)
      : super(_value, (v) => _then(v as LoginForgotPassword));

  @override
  LoginForgotPassword get _value => super._value as LoginForgotPassword;
}

/// @nodoc

class _$LoginForgotPassword implements LoginForgotPassword {
  const _$LoginForgotPassword();

  @override
  String toString() {
    return 'LoginEvent.forgotPasswordClick()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is LoginForgotPassword);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(LoginModel model) submit,
    required TResult Function(LoginModel model) typing,
    required TResult Function(LoginModel model) getStoredCredentials,
    required TResult Function() forgotPasswordClick,
    required TResult Function() signUpClick,
  }) {
    return forgotPasswordClick();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(LoginModel model)? submit,
    TResult Function(LoginModel model)? typing,
    TResult Function(LoginModel model)? getStoredCredentials,
    TResult Function()? forgotPasswordClick,
    TResult Function()? signUpClick,
    required TResult orElse(),
  }) {
    if (forgotPasswordClick != null) {
      return forgotPasswordClick();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginSubmit value) submit,
    required TResult Function(LoginTyping value) typing,
    required TResult Function(LoginGetStoredCredentials value)
        getStoredCredentials,
    required TResult Function(LoginForgotPassword value) forgotPasswordClick,
    required TResult Function(LoginSignUp value) signUpClick,
  }) {
    return forgotPasswordClick(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginSubmit value)? submit,
    TResult Function(LoginTyping value)? typing,
    TResult Function(LoginGetStoredCredentials value)? getStoredCredentials,
    TResult Function(LoginForgotPassword value)? forgotPasswordClick,
    TResult Function(LoginSignUp value)? signUpClick,
    required TResult orElse(),
  }) {
    if (forgotPasswordClick != null) {
      return forgotPasswordClick(this);
    }
    return orElse();
  }
}

abstract class LoginForgotPassword implements LoginEvent {
  const factory LoginForgotPassword() = _$LoginForgotPassword;
}

/// @nodoc
abstract class $LoginSignUpCopyWith<$Res> {
  factory $LoginSignUpCopyWith(
          LoginSignUp value, $Res Function(LoginSignUp) then) =
      _$LoginSignUpCopyWithImpl<$Res>;
}

/// @nodoc
class _$LoginSignUpCopyWithImpl<$Res> extends _$LoginEventCopyWithImpl<$Res>
    implements $LoginSignUpCopyWith<$Res> {
  _$LoginSignUpCopyWithImpl(
      LoginSignUp _value, $Res Function(LoginSignUp) _then)
      : super(_value, (v) => _then(v as LoginSignUp));

  @override
  LoginSignUp get _value => super._value as LoginSignUp;
}

/// @nodoc

class _$LoginSignUp implements LoginSignUp {
  const _$LoginSignUp();

  @override
  String toString() {
    return 'LoginEvent.signUpClick()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is LoginSignUp);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(LoginModel model) submit,
    required TResult Function(LoginModel model) typing,
    required TResult Function(LoginModel model) getStoredCredentials,
    required TResult Function() forgotPasswordClick,
    required TResult Function() signUpClick,
  }) {
    return signUpClick();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(LoginModel model)? submit,
    TResult Function(LoginModel model)? typing,
    TResult Function(LoginModel model)? getStoredCredentials,
    TResult Function()? forgotPasswordClick,
    TResult Function()? signUpClick,
    required TResult orElse(),
  }) {
    if (signUpClick != null) {
      return signUpClick();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginSubmit value) submit,
    required TResult Function(LoginTyping value) typing,
    required TResult Function(LoginGetStoredCredentials value)
        getStoredCredentials,
    required TResult Function(LoginForgotPassword value) forgotPasswordClick,
    required TResult Function(LoginSignUp value) signUpClick,
  }) {
    return signUpClick(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginSubmit value)? submit,
    TResult Function(LoginTyping value)? typing,
    TResult Function(LoginGetStoredCredentials value)? getStoredCredentials,
    TResult Function(LoginForgotPassword value)? forgotPasswordClick,
    TResult Function(LoginSignUp value)? signUpClick,
    required TResult orElse(),
  }) {
    if (signUpClick != null) {
      return signUpClick(this);
    }
    return orElse();
  }
}

abstract class LoginSignUp implements LoginEvent {
  const factory LoginSignUp() = _$LoginSignUp;
}
