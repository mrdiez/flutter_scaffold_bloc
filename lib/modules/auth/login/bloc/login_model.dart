import 'package:flutter_scaffold_bloc/utils/string.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'login_model.freezed.dart';

/// An immutable generated class model
/// To generate code execute: flutter pub run build_runner build
/// For more info check the README # How to generate code (with freezed)
@freezed
class LoginModel with _$LoginModel {
  const LoginModel._();
  const factory LoginModel(
      {String? login,
      String? password,
      String? loginError,
      String? passwordError,
      @Default(false) bool rememberMe}) = _LoginModel;

  bool get isLoginError => StringTool.notNull(loginError);
  bool get isPasswordError => StringTool.notNull(passwordError);
  bool get isError => isLoginError || isPasswordError;
}
