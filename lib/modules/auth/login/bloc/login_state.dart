import 'package:flutter_scaffold_bloc/entities/user.dart';
import 'package:flutter_scaffold_bloc/modules/auth/login/bloc/login_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'login_state.freezed.dart';

/// An immutable class that define UI states
/// To generate code execute: flutter pub run build_runner build
/// For more info check the README # How to generate code (with freezed)
@freezed
class LoginState with _$LoginState {
  const LoginState._();
  const factory LoginState.init([@Default(LoginModel()) LoginModel model]) = LoginInit;
  const factory LoginState.ready(LoginModel model) = LoginReady;
  const factory LoginState.loading(LoginModel model) = LoginLoading;
  const factory LoginState.typingError(LoginModel model) = LoginTypingError;
  const factory LoginState.submitError(LoginModel model) = LoginSubmitError;
  const factory LoginState.success() = LoginSuccess;

  LoginModel get model => maybeMap(
      ready: (state) => state.model,
      loading: (state) => state.model,
      typingError: (state) => state.model,
      submitError: (state) => state.model,
      orElse: () => LoginModel());
}
