// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'login_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$LoginStateTearOff {
  const _$LoginStateTearOff();

  LoginInit init([LoginModel model = const LoginModel()]) {
    return LoginInit(
      model,
    );
  }

  LoginReady ready(LoginModel model) {
    return LoginReady(
      model,
    );
  }

  LoginLoading loading(LoginModel model) {
    return LoginLoading(
      model,
    );
  }

  LoginTypingError typingError(LoginModel model) {
    return LoginTypingError(
      model,
    );
  }

  LoginSubmitError submitError(LoginModel model) {
    return LoginSubmitError(
      model,
    );
  }

  LoginSuccess success() {
    return const LoginSuccess();
  }
}

/// @nodoc
const $LoginState = _$LoginStateTearOff();

/// @nodoc
mixin _$LoginState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(LoginModel model) init,
    required TResult Function(LoginModel model) ready,
    required TResult Function(LoginModel model) loading,
    required TResult Function(LoginModel model) typingError,
    required TResult Function(LoginModel model) submitError,
    required TResult Function() success,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(LoginModel model)? init,
    TResult Function(LoginModel model)? ready,
    TResult Function(LoginModel model)? loading,
    TResult Function(LoginModel model)? typingError,
    TResult Function(LoginModel model)? submitError,
    TResult Function()? success,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginInit value) init,
    required TResult Function(LoginReady value) ready,
    required TResult Function(LoginLoading value) loading,
    required TResult Function(LoginTypingError value) typingError,
    required TResult Function(LoginSubmitError value) submitError,
    required TResult Function(LoginSuccess value) success,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginInit value)? init,
    TResult Function(LoginReady value)? ready,
    TResult Function(LoginLoading value)? loading,
    TResult Function(LoginTypingError value)? typingError,
    TResult Function(LoginSubmitError value)? submitError,
    TResult Function(LoginSuccess value)? success,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LoginStateCopyWith<$Res> {
  factory $LoginStateCopyWith(
          LoginState value, $Res Function(LoginState) then) =
      _$LoginStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$LoginStateCopyWithImpl<$Res> implements $LoginStateCopyWith<$Res> {
  _$LoginStateCopyWithImpl(this._value, this._then);

  final LoginState _value;
  // ignore: unused_field
  final $Res Function(LoginState) _then;
}

/// @nodoc
abstract class $LoginInitCopyWith<$Res> {
  factory $LoginInitCopyWith(LoginInit value, $Res Function(LoginInit) then) =
      _$LoginInitCopyWithImpl<$Res>;
  $Res call({LoginModel model});

  $LoginModelCopyWith<$Res> get model;
}

/// @nodoc
class _$LoginInitCopyWithImpl<$Res> extends _$LoginStateCopyWithImpl<$Res>
    implements $LoginInitCopyWith<$Res> {
  _$LoginInitCopyWithImpl(LoginInit _value, $Res Function(LoginInit) _then)
      : super(_value, (v) => _then(v as LoginInit));

  @override
  LoginInit get _value => super._value as LoginInit;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(LoginInit(
      model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as LoginModel,
    ));
  }

  @override
  $LoginModelCopyWith<$Res> get model {
    return $LoginModelCopyWith<$Res>(_value.model, (value) {
      return _then(_value.copyWith(model: value));
    });
  }
}

/// @nodoc

class _$LoginInit extends LoginInit {
  const _$LoginInit([this.model = const LoginModel()]) : super._();

  @JsonKey(defaultValue: const LoginModel())
  @override
  final LoginModel model;

  @override
  String toString() {
    return 'LoginState.init(model: $model)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is LoginInit &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(model);

  @JsonKey(ignore: true)
  @override
  $LoginInitCopyWith<LoginInit> get copyWith =>
      _$LoginInitCopyWithImpl<LoginInit>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(LoginModel model) init,
    required TResult Function(LoginModel model) ready,
    required TResult Function(LoginModel model) loading,
    required TResult Function(LoginModel model) typingError,
    required TResult Function(LoginModel model) submitError,
    required TResult Function() success,
  }) {
    return init(model);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(LoginModel model)? init,
    TResult Function(LoginModel model)? ready,
    TResult Function(LoginModel model)? loading,
    TResult Function(LoginModel model)? typingError,
    TResult Function(LoginModel model)? submitError,
    TResult Function()? success,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(model);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginInit value) init,
    required TResult Function(LoginReady value) ready,
    required TResult Function(LoginLoading value) loading,
    required TResult Function(LoginTypingError value) typingError,
    required TResult Function(LoginSubmitError value) submitError,
    required TResult Function(LoginSuccess value) success,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginInit value)? init,
    TResult Function(LoginReady value)? ready,
    TResult Function(LoginLoading value)? loading,
    TResult Function(LoginTypingError value)? typingError,
    TResult Function(LoginSubmitError value)? submitError,
    TResult Function(LoginSuccess value)? success,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class LoginInit extends LoginState {
  const factory LoginInit([LoginModel model]) = _$LoginInit;
  const LoginInit._() : super._();

  LoginModel get model => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $LoginInitCopyWith<LoginInit> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LoginReadyCopyWith<$Res> {
  factory $LoginReadyCopyWith(
          LoginReady value, $Res Function(LoginReady) then) =
      _$LoginReadyCopyWithImpl<$Res>;
  $Res call({LoginModel model});

  $LoginModelCopyWith<$Res> get model;
}

/// @nodoc
class _$LoginReadyCopyWithImpl<$Res> extends _$LoginStateCopyWithImpl<$Res>
    implements $LoginReadyCopyWith<$Res> {
  _$LoginReadyCopyWithImpl(LoginReady _value, $Res Function(LoginReady) _then)
      : super(_value, (v) => _then(v as LoginReady));

  @override
  LoginReady get _value => super._value as LoginReady;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(LoginReady(
      model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as LoginModel,
    ));
  }

  @override
  $LoginModelCopyWith<$Res> get model {
    return $LoginModelCopyWith<$Res>(_value.model, (value) {
      return _then(_value.copyWith(model: value));
    });
  }
}

/// @nodoc

class _$LoginReady extends LoginReady {
  const _$LoginReady(this.model) : super._();

  @override
  final LoginModel model;

  @override
  String toString() {
    return 'LoginState.ready(model: $model)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is LoginReady &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(model);

  @JsonKey(ignore: true)
  @override
  $LoginReadyCopyWith<LoginReady> get copyWith =>
      _$LoginReadyCopyWithImpl<LoginReady>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(LoginModel model) init,
    required TResult Function(LoginModel model) ready,
    required TResult Function(LoginModel model) loading,
    required TResult Function(LoginModel model) typingError,
    required TResult Function(LoginModel model) submitError,
    required TResult Function() success,
  }) {
    return ready(model);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(LoginModel model)? init,
    TResult Function(LoginModel model)? ready,
    TResult Function(LoginModel model)? loading,
    TResult Function(LoginModel model)? typingError,
    TResult Function(LoginModel model)? submitError,
    TResult Function()? success,
    required TResult orElse(),
  }) {
    if (ready != null) {
      return ready(model);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginInit value) init,
    required TResult Function(LoginReady value) ready,
    required TResult Function(LoginLoading value) loading,
    required TResult Function(LoginTypingError value) typingError,
    required TResult Function(LoginSubmitError value) submitError,
    required TResult Function(LoginSuccess value) success,
  }) {
    return ready(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginInit value)? init,
    TResult Function(LoginReady value)? ready,
    TResult Function(LoginLoading value)? loading,
    TResult Function(LoginTypingError value)? typingError,
    TResult Function(LoginSubmitError value)? submitError,
    TResult Function(LoginSuccess value)? success,
    required TResult orElse(),
  }) {
    if (ready != null) {
      return ready(this);
    }
    return orElse();
  }
}

abstract class LoginReady extends LoginState {
  const factory LoginReady(LoginModel model) = _$LoginReady;
  const LoginReady._() : super._();

  LoginModel get model => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $LoginReadyCopyWith<LoginReady> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LoginLoadingCopyWith<$Res> {
  factory $LoginLoadingCopyWith(
          LoginLoading value, $Res Function(LoginLoading) then) =
      _$LoginLoadingCopyWithImpl<$Res>;
  $Res call({LoginModel model});

  $LoginModelCopyWith<$Res> get model;
}

/// @nodoc
class _$LoginLoadingCopyWithImpl<$Res> extends _$LoginStateCopyWithImpl<$Res>
    implements $LoginLoadingCopyWith<$Res> {
  _$LoginLoadingCopyWithImpl(
      LoginLoading _value, $Res Function(LoginLoading) _then)
      : super(_value, (v) => _then(v as LoginLoading));

  @override
  LoginLoading get _value => super._value as LoginLoading;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(LoginLoading(
      model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as LoginModel,
    ));
  }

  @override
  $LoginModelCopyWith<$Res> get model {
    return $LoginModelCopyWith<$Res>(_value.model, (value) {
      return _then(_value.copyWith(model: value));
    });
  }
}

/// @nodoc

class _$LoginLoading extends LoginLoading {
  const _$LoginLoading(this.model) : super._();

  @override
  final LoginModel model;

  @override
  String toString() {
    return 'LoginState.loading(model: $model)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is LoginLoading &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(model);

  @JsonKey(ignore: true)
  @override
  $LoginLoadingCopyWith<LoginLoading> get copyWith =>
      _$LoginLoadingCopyWithImpl<LoginLoading>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(LoginModel model) init,
    required TResult Function(LoginModel model) ready,
    required TResult Function(LoginModel model) loading,
    required TResult Function(LoginModel model) typingError,
    required TResult Function(LoginModel model) submitError,
    required TResult Function() success,
  }) {
    return loading(model);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(LoginModel model)? init,
    TResult Function(LoginModel model)? ready,
    TResult Function(LoginModel model)? loading,
    TResult Function(LoginModel model)? typingError,
    TResult Function(LoginModel model)? submitError,
    TResult Function()? success,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(model);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginInit value) init,
    required TResult Function(LoginReady value) ready,
    required TResult Function(LoginLoading value) loading,
    required TResult Function(LoginTypingError value) typingError,
    required TResult Function(LoginSubmitError value) submitError,
    required TResult Function(LoginSuccess value) success,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginInit value)? init,
    TResult Function(LoginReady value)? ready,
    TResult Function(LoginLoading value)? loading,
    TResult Function(LoginTypingError value)? typingError,
    TResult Function(LoginSubmitError value)? submitError,
    TResult Function(LoginSuccess value)? success,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class LoginLoading extends LoginState {
  const factory LoginLoading(LoginModel model) = _$LoginLoading;
  const LoginLoading._() : super._();

  LoginModel get model => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $LoginLoadingCopyWith<LoginLoading> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LoginTypingErrorCopyWith<$Res> {
  factory $LoginTypingErrorCopyWith(
          LoginTypingError value, $Res Function(LoginTypingError) then) =
      _$LoginTypingErrorCopyWithImpl<$Res>;
  $Res call({LoginModel model});

  $LoginModelCopyWith<$Res> get model;
}

/// @nodoc
class _$LoginTypingErrorCopyWithImpl<$Res>
    extends _$LoginStateCopyWithImpl<$Res>
    implements $LoginTypingErrorCopyWith<$Res> {
  _$LoginTypingErrorCopyWithImpl(
      LoginTypingError _value, $Res Function(LoginTypingError) _then)
      : super(_value, (v) => _then(v as LoginTypingError));

  @override
  LoginTypingError get _value => super._value as LoginTypingError;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(LoginTypingError(
      model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as LoginModel,
    ));
  }

  @override
  $LoginModelCopyWith<$Res> get model {
    return $LoginModelCopyWith<$Res>(_value.model, (value) {
      return _then(_value.copyWith(model: value));
    });
  }
}

/// @nodoc

class _$LoginTypingError extends LoginTypingError {
  const _$LoginTypingError(this.model) : super._();

  @override
  final LoginModel model;

  @override
  String toString() {
    return 'LoginState.typingError(model: $model)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is LoginTypingError &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(model);

  @JsonKey(ignore: true)
  @override
  $LoginTypingErrorCopyWith<LoginTypingError> get copyWith =>
      _$LoginTypingErrorCopyWithImpl<LoginTypingError>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(LoginModel model) init,
    required TResult Function(LoginModel model) ready,
    required TResult Function(LoginModel model) loading,
    required TResult Function(LoginModel model) typingError,
    required TResult Function(LoginModel model) submitError,
    required TResult Function() success,
  }) {
    return typingError(model);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(LoginModel model)? init,
    TResult Function(LoginModel model)? ready,
    TResult Function(LoginModel model)? loading,
    TResult Function(LoginModel model)? typingError,
    TResult Function(LoginModel model)? submitError,
    TResult Function()? success,
    required TResult orElse(),
  }) {
    if (typingError != null) {
      return typingError(model);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginInit value) init,
    required TResult Function(LoginReady value) ready,
    required TResult Function(LoginLoading value) loading,
    required TResult Function(LoginTypingError value) typingError,
    required TResult Function(LoginSubmitError value) submitError,
    required TResult Function(LoginSuccess value) success,
  }) {
    return typingError(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginInit value)? init,
    TResult Function(LoginReady value)? ready,
    TResult Function(LoginLoading value)? loading,
    TResult Function(LoginTypingError value)? typingError,
    TResult Function(LoginSubmitError value)? submitError,
    TResult Function(LoginSuccess value)? success,
    required TResult orElse(),
  }) {
    if (typingError != null) {
      return typingError(this);
    }
    return orElse();
  }
}

abstract class LoginTypingError extends LoginState {
  const factory LoginTypingError(LoginModel model) = _$LoginTypingError;
  const LoginTypingError._() : super._();

  LoginModel get model => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $LoginTypingErrorCopyWith<LoginTypingError> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LoginSubmitErrorCopyWith<$Res> {
  factory $LoginSubmitErrorCopyWith(
          LoginSubmitError value, $Res Function(LoginSubmitError) then) =
      _$LoginSubmitErrorCopyWithImpl<$Res>;
  $Res call({LoginModel model});

  $LoginModelCopyWith<$Res> get model;
}

/// @nodoc
class _$LoginSubmitErrorCopyWithImpl<$Res>
    extends _$LoginStateCopyWithImpl<$Res>
    implements $LoginSubmitErrorCopyWith<$Res> {
  _$LoginSubmitErrorCopyWithImpl(
      LoginSubmitError _value, $Res Function(LoginSubmitError) _then)
      : super(_value, (v) => _then(v as LoginSubmitError));

  @override
  LoginSubmitError get _value => super._value as LoginSubmitError;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(LoginSubmitError(
      model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as LoginModel,
    ));
  }

  @override
  $LoginModelCopyWith<$Res> get model {
    return $LoginModelCopyWith<$Res>(_value.model, (value) {
      return _then(_value.copyWith(model: value));
    });
  }
}

/// @nodoc

class _$LoginSubmitError extends LoginSubmitError {
  const _$LoginSubmitError(this.model) : super._();

  @override
  final LoginModel model;

  @override
  String toString() {
    return 'LoginState.submitError(model: $model)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is LoginSubmitError &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(model);

  @JsonKey(ignore: true)
  @override
  $LoginSubmitErrorCopyWith<LoginSubmitError> get copyWith =>
      _$LoginSubmitErrorCopyWithImpl<LoginSubmitError>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(LoginModel model) init,
    required TResult Function(LoginModel model) ready,
    required TResult Function(LoginModel model) loading,
    required TResult Function(LoginModel model) typingError,
    required TResult Function(LoginModel model) submitError,
    required TResult Function() success,
  }) {
    return submitError(model);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(LoginModel model)? init,
    TResult Function(LoginModel model)? ready,
    TResult Function(LoginModel model)? loading,
    TResult Function(LoginModel model)? typingError,
    TResult Function(LoginModel model)? submitError,
    TResult Function()? success,
    required TResult orElse(),
  }) {
    if (submitError != null) {
      return submitError(model);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginInit value) init,
    required TResult Function(LoginReady value) ready,
    required TResult Function(LoginLoading value) loading,
    required TResult Function(LoginTypingError value) typingError,
    required TResult Function(LoginSubmitError value) submitError,
    required TResult Function(LoginSuccess value) success,
  }) {
    return submitError(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginInit value)? init,
    TResult Function(LoginReady value)? ready,
    TResult Function(LoginLoading value)? loading,
    TResult Function(LoginTypingError value)? typingError,
    TResult Function(LoginSubmitError value)? submitError,
    TResult Function(LoginSuccess value)? success,
    required TResult orElse(),
  }) {
    if (submitError != null) {
      return submitError(this);
    }
    return orElse();
  }
}

abstract class LoginSubmitError extends LoginState {
  const factory LoginSubmitError(LoginModel model) = _$LoginSubmitError;
  const LoginSubmitError._() : super._();

  LoginModel get model => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $LoginSubmitErrorCopyWith<LoginSubmitError> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LoginSuccessCopyWith<$Res> {
  factory $LoginSuccessCopyWith(
          LoginSuccess value, $Res Function(LoginSuccess) then) =
      _$LoginSuccessCopyWithImpl<$Res>;
}

/// @nodoc
class _$LoginSuccessCopyWithImpl<$Res> extends _$LoginStateCopyWithImpl<$Res>
    implements $LoginSuccessCopyWith<$Res> {
  _$LoginSuccessCopyWithImpl(
      LoginSuccess _value, $Res Function(LoginSuccess) _then)
      : super(_value, (v) => _then(v as LoginSuccess));

  @override
  LoginSuccess get _value => super._value as LoginSuccess;
}

/// @nodoc

class _$LoginSuccess extends LoginSuccess {
  const _$LoginSuccess() : super._();

  @override
  String toString() {
    return 'LoginState.success()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is LoginSuccess);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(LoginModel model) init,
    required TResult Function(LoginModel model) ready,
    required TResult Function(LoginModel model) loading,
    required TResult Function(LoginModel model) typingError,
    required TResult Function(LoginModel model) submitError,
    required TResult Function() success,
  }) {
    return success();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(LoginModel model)? init,
    TResult Function(LoginModel model)? ready,
    TResult Function(LoginModel model)? loading,
    TResult Function(LoginModel model)? typingError,
    TResult Function(LoginModel model)? submitError,
    TResult Function()? success,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoginInit value) init,
    required TResult Function(LoginReady value) ready,
    required TResult Function(LoginLoading value) loading,
    required TResult Function(LoginTypingError value) typingError,
    required TResult Function(LoginSubmitError value) submitError,
    required TResult Function(LoginSuccess value) success,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoginInit value)? init,
    TResult Function(LoginReady value)? ready,
    TResult Function(LoginLoading value)? loading,
    TResult Function(LoginTypingError value)? typingError,
    TResult Function(LoginSubmitError value)? submitError,
    TResult Function(LoginSuccess value)? success,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class LoginSuccess extends LoginState {
  const factory LoginSuccess() = _$LoginSuccess;
  const LoginSuccess._() : super._();
}
