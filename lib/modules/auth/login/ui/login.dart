import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_scaffold_bloc/modules/auth/auth.dart';
import 'package:flutter_scaffold_bloc/modules/auth/login/bloc/login_bloc.dart';
import 'package:flutter_scaffold_bloc/modules/auth/login/bloc/login_event.dart';
import 'package:flutter_scaffold_bloc/modules/auth/login/bloc/login_state.dart';
import 'package:flutter_scaffold_bloc/widgets/buttons/secondary.dart';
import 'package:flutter_scaffold_bloc/widgets/buttons/submit.dart';
import 'package:flutter_scaffold_bloc/widgets/inputs/custom_text.dart';
import 'package:flutter_scaffold_bloc/widgets/misc/custom_bloc.dart';

class LoginView extends CustomBlocWidget<LoginBloc, LoginEvent, LoginState> {
  final _formKey = GlobalKey<FormBuilderState>();
  final _loginKey = GlobalKey<CustomTextInputState>();
  final _passwordKey = GlobalKey<CustomTextInputState>();
  final _rememberMeKey = GlobalKey<FormFieldState>();

  @override
  final provider = (_) => LoginBloc();

  @override
  void onReady() {
    if (!(_loginKey.currentState?.focusNode.hasFocus ?? true)) _loginKey.currentState!.focusNode.requestFocus();
  }

  @override
  void onBuild() {
    if (state is LoginSubmitError) {
      if (state.model.isLoginError) _loginKey.currentState?.shake();
      if (state.model.isPasswordError) _passwordKey.currentState?.shake();
    }
  }

  bool get isLoading => state is LoginLoading;
  void Function()? get _onSubmit => !isLoading ? () => bloc.add(LoginEvent.submit(state.model)) : null;

  @override
  Widget builder(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CustomTextInput(
                  label: LoginBloc.loginLabel,
                  key: _loginKey,
                  keyboard: TextInputType.emailAddress,
                  value: state.model.login,
                  error: state.model.loginError,
                  onChanged: (value) => bloc.add(LoginEvent.typing(state.model.copyWith(login: value))),
                  onSubmitted: (_) => _passwordKey.currentState!.focusNode.requestFocus(),
                ),
                CustomTextInput(
                  label: translation.password,
                  key: _passwordKey,
                  secret: true,
                  value: state.model.password,
                  error: state.model.passwordError,
                  onChanged: (value) => bloc.add(LoginEvent.typing(state.model.copyWith(password: value))),
                  onSubmitted: (_) => (_onSubmit ?? () {})(),
                ),
                Row(
                  children: [
                    Expanded(
                      child: FormBuilderCheckbox(
                        key: _rememberMeKey,
                        name: translation.rememberMe,
                        title: AutoSizeText(translation.rememberMe, maxLines: 1),
                        initialValue: state.model.rememberMe,
                        onChanged: (value) =>
                            bloc.add(LoginEvent.typing(state.model.copyWith(rememberMe: value ?? false))),
                      ),
                    ),
                    Expanded(
                        child: SecondaryButton(
                      text: translation.forgotPassword,
                      hero: true,
                      action: !isLoading ? () => Navigator.of(context).pushNamed(AuthModule.ResetPasswordRoute) : null,
                    )),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    SubmitButton(
                      action: _onSubmit,
                      hero: true,
                    ),
                    SecondaryButton(
                      text: translation.signUp,
                      hero: true,
                      action: !isLoading ? () => Navigator.of(context).pushNamed(AuthModule.SignUpRoute) : null,
                    ),
                  ],
                )
              ],
            )),
        if (isLoading) CircularProgressIndicator()
      ],
    );
  }
}
