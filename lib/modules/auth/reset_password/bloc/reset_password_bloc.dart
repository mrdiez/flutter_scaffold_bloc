import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_scaffold_bloc/constants/translations/generated/l10n.dart';
import 'package:flutter_scaffold_bloc/modules/auth/data/auth_repository.dart';
import 'package:flutter_scaffold_bloc/modules/auth/login/bloc/login_bloc.dart';
import 'package:flutter_scaffold_bloc/modules/auth/reset_password/bloc/reset_password_event.dart';
import 'package:flutter_scaffold_bloc/modules/auth/reset_password/bloc/reset_password_model.dart';
import 'package:flutter_scaffold_bloc/modules/auth/reset_password/bloc/reset_password_state.dart';
import 'package:flutter_scaffold_bloc/services/task_manager.dart';
import 'package:flutter_scaffold_bloc/utils/string.dart';
import 'package:flutter_scaffold_bloc/widgets/misc/custom_bloc.dart';

enum ResetPasswordMethod { Email, SMS }

/// A [Bloc] class that emit states when receiving events
/// Check this to understand what is a [Bloc] and why we'll use it:
/// https://bloclibrary.dev/#/coreconcepts?id=bloc
class ResetPasswordBloc extends CustomBloc<ResetPasswordEvent, ResetPasswordState> {
  ResetPasswordBloc() : super(ResetPasswordState.init());
  final AuthRepository _repository = const AuthRepository();

  static Map<ResetPasswordMethod, String> MethodLabels = {
    ResetPasswordMethod.Email: Translation.current.email,
    ResetPasswordMethod.SMS: Translation.current.sms,
  };

  static String? resetMethodValidator(ResetPasswordMethod? method, [bool isSubmit = false]) {
    if (method == null) return Translation.current.thisFieldIsRequired;
  }

  static String? resetCodeValidator(String? code, [bool isSubmit = false]) {
    if (!StringTool.notNull(code) && isSubmit) return Translation.current.thisFieldIsRequired;
    if (StringTool.notNull(code) && code!.length < 4) {
      return Translation.current.nbCharactersMinimum(4);
    }
  }

  ResetPasswordModel _validateLogin(ResetPasswordModel model, [bool isSubmit = false]) =>
      model.copyWith(loginError: LoginBloc.loginValidator(model.login, isSubmit));

  ResetPasswordModel _validateMethod(ResetPasswordModel model, [bool isSubmit = false]) =>
      model.copyWith(methodError: resetMethodValidator(model.method, isSubmit));

  ResetPasswordModel _validateCode(ResetPasswordModel model, [bool isSubmit = false]) =>
      model.copyWith(codeError: resetCodeValidator(model.code, isSubmit));

  Stream<ResetPasswordState> _onSubmitEmail(ResetPasswordModel model) async* {
    var result = _validateLogin(model, true);
    try {
      if (result.isError) {
        yield ResetPasswordState.submitError(result);
      } else {
        yield ResetPasswordState.loading(result);
        taskManager.add(TaskManagerEvent.startLoading(ResetPasswordSubmitEmail));
        result = result.copyWith(methods: await _repository.resetPasswordMethods(model));
        if (result.methods!.length > 1) {
          yield ResetPasswordState.chooseMethod(result);
        } else if (result.methods!.length == 1) {
          yield ResetPasswordState.confirmReset(result.copyWith(method: result.methods!.first));
        }
        taskManager.add(TaskManagerEvent.stopLoading(ResetPasswordSubmitEmail));
      }
    } catch (error) {
      yield ResetPasswordState.submitError(result.copyWith(loginError: error.toString().removeException));
      taskManager.add(TaskManagerEvent.stopLoading(ResetPasswordSubmitEmail));
    }
  }

  Stream<ResetPasswordState> _onSubmitMethod(ResetPasswordModel model) async* {
    var result = _validateMethod(model, true);
    try {
      if (result.isError) {
        yield ResetPasswordState.submitError(result);
      } else {
        yield ResetPasswordState.loading(result);
        taskManager.add(TaskManagerEvent.startLoading(ResetPasswordSubmitMethod));
        await _repository.postResetPasswordMethod(model);
        yield ResetPasswordState.confirmReset(result);
        taskManager.add(TaskManagerEvent.stopLoading(ResetPasswordSubmitMethod));
      }
    } catch (error) {
      yield ResetPasswordState.submitError(model.copyWith(methodError: error.toString().removeException));
      taskManager.add(TaskManagerEvent.stopLoading(ResetPasswordSubmitMethod));
    }
  }

  Stream<ResetPasswordState> _onSubmitCode(ResetPasswordModel model) async* {
    var result = _validateCode(model, true);
    try {
      if (result.isError) {
        yield ResetPasswordState.submitError(result);
      } else {
        yield ResetPasswordState.loading(result);
        taskManager.add(TaskManagerEvent.startLoading(ResetPasswordSubmitCode));
        result = result.copyWith(newPassword: await _repository.resetPassword(model));
        yield ResetPasswordState.success(result);
        taskManager.add(TaskManagerEvent.stopLoading(ResetPasswordSubmitCode));
      }
    } catch (error) {
      yield ResetPasswordState.submitError(model.copyWith(codeError: error.toString().removeException));
      taskManager.add(TaskManagerEvent.stopLoading(ResetPasswordSubmitCode));
    }
  }

  Stream<ResetPasswordState> _onTypingEmail(ResetPasswordModel model) async* {
    var result = _validateLogin(model);
    yield result.isError ? ResetPasswordState.typingError(result) : ResetPasswordState.init(result);
  }

  Stream<ResetPasswordState> _onRadioSelect(ResetPasswordModel model) async* {
    var result = _validateMethod(model);
    yield result.isError ? ResetPasswordState.typingError(result) : ResetPasswordState.chooseMethod(result);
  }

  Stream<ResetPasswordState> _onTypingCode(ResetPasswordModel model) async* {
    var result = _validateCode(model);
    yield result.isError ? ResetPasswordState.typingError(result) : ResetPasswordState.confirmReset(result);
  }

  @override
  Stream<ResetPasswordState> mapEventToState(ResetPasswordEvent event) => event.maybeWhen(
        typingEmail: _onTypingEmail,
        submitEmail: _onSubmitEmail,
        radioSelect: _onRadioSelect,
        submitMethod: _onSubmitMethod,
        typingCode: _onTypingCode,
        submitCode: _onSubmitCode,
        orElse: () async* {
          yield ResetPasswordState.init();
        },
      );
}
