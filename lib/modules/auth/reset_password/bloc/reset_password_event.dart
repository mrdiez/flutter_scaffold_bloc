import 'package:flutter_scaffold_bloc/modules/auth/reset_password/bloc/reset_password_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'reset_password_event.freezed.dart';

/// An immutable generated class that define BLOC events
/// To generate code execute: flutter pub run build_runner build
/// For more info check the README # How to generate code (with freezed)
@freezed
class ResetPasswordEvent with _$ResetPasswordEvent {
  const factory ResetPasswordEvent.submitEmail(ResetPasswordModel model) = ResetPasswordSubmitEmail;
  const factory ResetPasswordEvent.submitMethod(ResetPasswordModel model) = ResetPasswordSubmitMethod;
  const factory ResetPasswordEvent.submitCode(ResetPasswordModel model) = ResetPasswordSubmitCode;
  const factory ResetPasswordEvent.radioSelect(ResetPasswordModel model) = ResetPasswordRadioSelect;
  const factory ResetPasswordEvent.typingEmail(ResetPasswordModel model) = ResetPasswordTypingEmail;
  const factory ResetPasswordEvent.typingCode(ResetPasswordModel model) = ResetPasswordTypingCode;
  const factory ResetPasswordEvent.cancelClick() = ResetPasswordCancel;
  const factory ResetPasswordEvent.end() = ResetPasswordEnd;
}
