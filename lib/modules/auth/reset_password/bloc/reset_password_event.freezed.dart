// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'reset_password_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$ResetPasswordEventTearOff {
  const _$ResetPasswordEventTearOff();

  ResetPasswordSubmitEmail submitEmail(ResetPasswordModel model) {
    return ResetPasswordSubmitEmail(
      model,
    );
  }

  ResetPasswordSubmitMethod submitMethod(ResetPasswordModel model) {
    return ResetPasswordSubmitMethod(
      model,
    );
  }

  ResetPasswordSubmitCode submitCode(ResetPasswordModel model) {
    return ResetPasswordSubmitCode(
      model,
    );
  }

  ResetPasswordRadioSelect radioSelect(ResetPasswordModel model) {
    return ResetPasswordRadioSelect(
      model,
    );
  }

  ResetPasswordTypingEmail typingEmail(ResetPasswordModel model) {
    return ResetPasswordTypingEmail(
      model,
    );
  }

  ResetPasswordTypingCode typingCode(ResetPasswordModel model) {
    return ResetPasswordTypingCode(
      model,
    );
  }

  ResetPasswordCancel cancelClick() {
    return const ResetPasswordCancel();
  }

  ResetPasswordEnd end() {
    return const ResetPasswordEnd();
  }
}

/// @nodoc
const $ResetPasswordEvent = _$ResetPasswordEventTearOff();

/// @nodoc
mixin _$ResetPasswordEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ResetPasswordModel model) submitEmail,
    required TResult Function(ResetPasswordModel model) submitMethod,
    required TResult Function(ResetPasswordModel model) submitCode,
    required TResult Function(ResetPasswordModel model) radioSelect,
    required TResult Function(ResetPasswordModel model) typingEmail,
    required TResult Function(ResetPasswordModel model) typingCode,
    required TResult Function() cancelClick,
    required TResult Function() end,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ResetPasswordModel model)? submitEmail,
    TResult Function(ResetPasswordModel model)? submitMethod,
    TResult Function(ResetPasswordModel model)? submitCode,
    TResult Function(ResetPasswordModel model)? radioSelect,
    TResult Function(ResetPasswordModel model)? typingEmail,
    TResult Function(ResetPasswordModel model)? typingCode,
    TResult Function()? cancelClick,
    TResult Function()? end,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ResetPasswordSubmitEmail value) submitEmail,
    required TResult Function(ResetPasswordSubmitMethod value) submitMethod,
    required TResult Function(ResetPasswordSubmitCode value) submitCode,
    required TResult Function(ResetPasswordRadioSelect value) radioSelect,
    required TResult Function(ResetPasswordTypingEmail value) typingEmail,
    required TResult Function(ResetPasswordTypingCode value) typingCode,
    required TResult Function(ResetPasswordCancel value) cancelClick,
    required TResult Function(ResetPasswordEnd value) end,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ResetPasswordSubmitEmail value)? submitEmail,
    TResult Function(ResetPasswordSubmitMethod value)? submitMethod,
    TResult Function(ResetPasswordSubmitCode value)? submitCode,
    TResult Function(ResetPasswordRadioSelect value)? radioSelect,
    TResult Function(ResetPasswordTypingEmail value)? typingEmail,
    TResult Function(ResetPasswordTypingCode value)? typingCode,
    TResult Function(ResetPasswordCancel value)? cancelClick,
    TResult Function(ResetPasswordEnd value)? end,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ResetPasswordEventCopyWith<$Res> {
  factory $ResetPasswordEventCopyWith(
          ResetPasswordEvent value, $Res Function(ResetPasswordEvent) then) =
      _$ResetPasswordEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$ResetPasswordEventCopyWithImpl<$Res>
    implements $ResetPasswordEventCopyWith<$Res> {
  _$ResetPasswordEventCopyWithImpl(this._value, this._then);

  final ResetPasswordEvent _value;
  // ignore: unused_field
  final $Res Function(ResetPasswordEvent) _then;
}

/// @nodoc
abstract class $ResetPasswordSubmitEmailCopyWith<$Res> {
  factory $ResetPasswordSubmitEmailCopyWith(ResetPasswordSubmitEmail value,
          $Res Function(ResetPasswordSubmitEmail) then) =
      _$ResetPasswordSubmitEmailCopyWithImpl<$Res>;
  $Res call({ResetPasswordModel model});

  $ResetPasswordModelCopyWith<$Res> get model;
}

/// @nodoc
class _$ResetPasswordSubmitEmailCopyWithImpl<$Res>
    extends _$ResetPasswordEventCopyWithImpl<$Res>
    implements $ResetPasswordSubmitEmailCopyWith<$Res> {
  _$ResetPasswordSubmitEmailCopyWithImpl(ResetPasswordSubmitEmail _value,
      $Res Function(ResetPasswordSubmitEmail) _then)
      : super(_value, (v) => _then(v as ResetPasswordSubmitEmail));

  @override
  ResetPasswordSubmitEmail get _value =>
      super._value as ResetPasswordSubmitEmail;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(ResetPasswordSubmitEmail(
      model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as ResetPasswordModel,
    ));
  }

  @override
  $ResetPasswordModelCopyWith<$Res> get model {
    return $ResetPasswordModelCopyWith<$Res>(_value.model, (value) {
      return _then(_value.copyWith(model: value));
    });
  }
}

/// @nodoc

class _$ResetPasswordSubmitEmail implements ResetPasswordSubmitEmail {
  const _$ResetPasswordSubmitEmail(this.model);

  @override
  final ResetPasswordModel model;

  @override
  String toString() {
    return 'ResetPasswordEvent.submitEmail(model: $model)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ResetPasswordSubmitEmail &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(model);

  @JsonKey(ignore: true)
  @override
  $ResetPasswordSubmitEmailCopyWith<ResetPasswordSubmitEmail> get copyWith =>
      _$ResetPasswordSubmitEmailCopyWithImpl<ResetPasswordSubmitEmail>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ResetPasswordModel model) submitEmail,
    required TResult Function(ResetPasswordModel model) submitMethod,
    required TResult Function(ResetPasswordModel model) submitCode,
    required TResult Function(ResetPasswordModel model) radioSelect,
    required TResult Function(ResetPasswordModel model) typingEmail,
    required TResult Function(ResetPasswordModel model) typingCode,
    required TResult Function() cancelClick,
    required TResult Function() end,
  }) {
    return submitEmail(model);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ResetPasswordModel model)? submitEmail,
    TResult Function(ResetPasswordModel model)? submitMethod,
    TResult Function(ResetPasswordModel model)? submitCode,
    TResult Function(ResetPasswordModel model)? radioSelect,
    TResult Function(ResetPasswordModel model)? typingEmail,
    TResult Function(ResetPasswordModel model)? typingCode,
    TResult Function()? cancelClick,
    TResult Function()? end,
    required TResult orElse(),
  }) {
    if (submitEmail != null) {
      return submitEmail(model);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ResetPasswordSubmitEmail value) submitEmail,
    required TResult Function(ResetPasswordSubmitMethod value) submitMethod,
    required TResult Function(ResetPasswordSubmitCode value) submitCode,
    required TResult Function(ResetPasswordRadioSelect value) radioSelect,
    required TResult Function(ResetPasswordTypingEmail value) typingEmail,
    required TResult Function(ResetPasswordTypingCode value) typingCode,
    required TResult Function(ResetPasswordCancel value) cancelClick,
    required TResult Function(ResetPasswordEnd value) end,
  }) {
    return submitEmail(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ResetPasswordSubmitEmail value)? submitEmail,
    TResult Function(ResetPasswordSubmitMethod value)? submitMethod,
    TResult Function(ResetPasswordSubmitCode value)? submitCode,
    TResult Function(ResetPasswordRadioSelect value)? radioSelect,
    TResult Function(ResetPasswordTypingEmail value)? typingEmail,
    TResult Function(ResetPasswordTypingCode value)? typingCode,
    TResult Function(ResetPasswordCancel value)? cancelClick,
    TResult Function(ResetPasswordEnd value)? end,
    required TResult orElse(),
  }) {
    if (submitEmail != null) {
      return submitEmail(this);
    }
    return orElse();
  }
}

abstract class ResetPasswordSubmitEmail implements ResetPasswordEvent {
  const factory ResetPasswordSubmitEmail(ResetPasswordModel model) =
      _$ResetPasswordSubmitEmail;

  ResetPasswordModel get model => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ResetPasswordSubmitEmailCopyWith<ResetPasswordSubmitEmail> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ResetPasswordSubmitMethodCopyWith<$Res> {
  factory $ResetPasswordSubmitMethodCopyWith(ResetPasswordSubmitMethod value,
          $Res Function(ResetPasswordSubmitMethod) then) =
      _$ResetPasswordSubmitMethodCopyWithImpl<$Res>;
  $Res call({ResetPasswordModel model});

  $ResetPasswordModelCopyWith<$Res> get model;
}

/// @nodoc
class _$ResetPasswordSubmitMethodCopyWithImpl<$Res>
    extends _$ResetPasswordEventCopyWithImpl<$Res>
    implements $ResetPasswordSubmitMethodCopyWith<$Res> {
  _$ResetPasswordSubmitMethodCopyWithImpl(ResetPasswordSubmitMethod _value,
      $Res Function(ResetPasswordSubmitMethod) _then)
      : super(_value, (v) => _then(v as ResetPasswordSubmitMethod));

  @override
  ResetPasswordSubmitMethod get _value =>
      super._value as ResetPasswordSubmitMethod;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(ResetPasswordSubmitMethod(
      model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as ResetPasswordModel,
    ));
  }

  @override
  $ResetPasswordModelCopyWith<$Res> get model {
    return $ResetPasswordModelCopyWith<$Res>(_value.model, (value) {
      return _then(_value.copyWith(model: value));
    });
  }
}

/// @nodoc

class _$ResetPasswordSubmitMethod implements ResetPasswordSubmitMethod {
  const _$ResetPasswordSubmitMethod(this.model);

  @override
  final ResetPasswordModel model;

  @override
  String toString() {
    return 'ResetPasswordEvent.submitMethod(model: $model)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ResetPasswordSubmitMethod &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(model);

  @JsonKey(ignore: true)
  @override
  $ResetPasswordSubmitMethodCopyWith<ResetPasswordSubmitMethod> get copyWith =>
      _$ResetPasswordSubmitMethodCopyWithImpl<ResetPasswordSubmitMethod>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ResetPasswordModel model) submitEmail,
    required TResult Function(ResetPasswordModel model) submitMethod,
    required TResult Function(ResetPasswordModel model) submitCode,
    required TResult Function(ResetPasswordModel model) radioSelect,
    required TResult Function(ResetPasswordModel model) typingEmail,
    required TResult Function(ResetPasswordModel model) typingCode,
    required TResult Function() cancelClick,
    required TResult Function() end,
  }) {
    return submitMethod(model);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ResetPasswordModel model)? submitEmail,
    TResult Function(ResetPasswordModel model)? submitMethod,
    TResult Function(ResetPasswordModel model)? submitCode,
    TResult Function(ResetPasswordModel model)? radioSelect,
    TResult Function(ResetPasswordModel model)? typingEmail,
    TResult Function(ResetPasswordModel model)? typingCode,
    TResult Function()? cancelClick,
    TResult Function()? end,
    required TResult orElse(),
  }) {
    if (submitMethod != null) {
      return submitMethod(model);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ResetPasswordSubmitEmail value) submitEmail,
    required TResult Function(ResetPasswordSubmitMethod value) submitMethod,
    required TResult Function(ResetPasswordSubmitCode value) submitCode,
    required TResult Function(ResetPasswordRadioSelect value) radioSelect,
    required TResult Function(ResetPasswordTypingEmail value) typingEmail,
    required TResult Function(ResetPasswordTypingCode value) typingCode,
    required TResult Function(ResetPasswordCancel value) cancelClick,
    required TResult Function(ResetPasswordEnd value) end,
  }) {
    return submitMethod(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ResetPasswordSubmitEmail value)? submitEmail,
    TResult Function(ResetPasswordSubmitMethod value)? submitMethod,
    TResult Function(ResetPasswordSubmitCode value)? submitCode,
    TResult Function(ResetPasswordRadioSelect value)? radioSelect,
    TResult Function(ResetPasswordTypingEmail value)? typingEmail,
    TResult Function(ResetPasswordTypingCode value)? typingCode,
    TResult Function(ResetPasswordCancel value)? cancelClick,
    TResult Function(ResetPasswordEnd value)? end,
    required TResult orElse(),
  }) {
    if (submitMethod != null) {
      return submitMethod(this);
    }
    return orElse();
  }
}

abstract class ResetPasswordSubmitMethod implements ResetPasswordEvent {
  const factory ResetPasswordSubmitMethod(ResetPasswordModel model) =
      _$ResetPasswordSubmitMethod;

  ResetPasswordModel get model => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ResetPasswordSubmitMethodCopyWith<ResetPasswordSubmitMethod> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ResetPasswordSubmitCodeCopyWith<$Res> {
  factory $ResetPasswordSubmitCodeCopyWith(ResetPasswordSubmitCode value,
          $Res Function(ResetPasswordSubmitCode) then) =
      _$ResetPasswordSubmitCodeCopyWithImpl<$Res>;
  $Res call({ResetPasswordModel model});

  $ResetPasswordModelCopyWith<$Res> get model;
}

/// @nodoc
class _$ResetPasswordSubmitCodeCopyWithImpl<$Res>
    extends _$ResetPasswordEventCopyWithImpl<$Res>
    implements $ResetPasswordSubmitCodeCopyWith<$Res> {
  _$ResetPasswordSubmitCodeCopyWithImpl(ResetPasswordSubmitCode _value,
      $Res Function(ResetPasswordSubmitCode) _then)
      : super(_value, (v) => _then(v as ResetPasswordSubmitCode));

  @override
  ResetPasswordSubmitCode get _value => super._value as ResetPasswordSubmitCode;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(ResetPasswordSubmitCode(
      model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as ResetPasswordModel,
    ));
  }

  @override
  $ResetPasswordModelCopyWith<$Res> get model {
    return $ResetPasswordModelCopyWith<$Res>(_value.model, (value) {
      return _then(_value.copyWith(model: value));
    });
  }
}

/// @nodoc

class _$ResetPasswordSubmitCode implements ResetPasswordSubmitCode {
  const _$ResetPasswordSubmitCode(this.model);

  @override
  final ResetPasswordModel model;

  @override
  String toString() {
    return 'ResetPasswordEvent.submitCode(model: $model)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ResetPasswordSubmitCode &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(model);

  @JsonKey(ignore: true)
  @override
  $ResetPasswordSubmitCodeCopyWith<ResetPasswordSubmitCode> get copyWith =>
      _$ResetPasswordSubmitCodeCopyWithImpl<ResetPasswordSubmitCode>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ResetPasswordModel model) submitEmail,
    required TResult Function(ResetPasswordModel model) submitMethod,
    required TResult Function(ResetPasswordModel model) submitCode,
    required TResult Function(ResetPasswordModel model) radioSelect,
    required TResult Function(ResetPasswordModel model) typingEmail,
    required TResult Function(ResetPasswordModel model) typingCode,
    required TResult Function() cancelClick,
    required TResult Function() end,
  }) {
    return submitCode(model);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ResetPasswordModel model)? submitEmail,
    TResult Function(ResetPasswordModel model)? submitMethod,
    TResult Function(ResetPasswordModel model)? submitCode,
    TResult Function(ResetPasswordModel model)? radioSelect,
    TResult Function(ResetPasswordModel model)? typingEmail,
    TResult Function(ResetPasswordModel model)? typingCode,
    TResult Function()? cancelClick,
    TResult Function()? end,
    required TResult orElse(),
  }) {
    if (submitCode != null) {
      return submitCode(model);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ResetPasswordSubmitEmail value) submitEmail,
    required TResult Function(ResetPasswordSubmitMethod value) submitMethod,
    required TResult Function(ResetPasswordSubmitCode value) submitCode,
    required TResult Function(ResetPasswordRadioSelect value) radioSelect,
    required TResult Function(ResetPasswordTypingEmail value) typingEmail,
    required TResult Function(ResetPasswordTypingCode value) typingCode,
    required TResult Function(ResetPasswordCancel value) cancelClick,
    required TResult Function(ResetPasswordEnd value) end,
  }) {
    return submitCode(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ResetPasswordSubmitEmail value)? submitEmail,
    TResult Function(ResetPasswordSubmitMethod value)? submitMethod,
    TResult Function(ResetPasswordSubmitCode value)? submitCode,
    TResult Function(ResetPasswordRadioSelect value)? radioSelect,
    TResult Function(ResetPasswordTypingEmail value)? typingEmail,
    TResult Function(ResetPasswordTypingCode value)? typingCode,
    TResult Function(ResetPasswordCancel value)? cancelClick,
    TResult Function(ResetPasswordEnd value)? end,
    required TResult orElse(),
  }) {
    if (submitCode != null) {
      return submitCode(this);
    }
    return orElse();
  }
}

abstract class ResetPasswordSubmitCode implements ResetPasswordEvent {
  const factory ResetPasswordSubmitCode(ResetPasswordModel model) =
      _$ResetPasswordSubmitCode;

  ResetPasswordModel get model => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ResetPasswordSubmitCodeCopyWith<ResetPasswordSubmitCode> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ResetPasswordRadioSelectCopyWith<$Res> {
  factory $ResetPasswordRadioSelectCopyWith(ResetPasswordRadioSelect value,
          $Res Function(ResetPasswordRadioSelect) then) =
      _$ResetPasswordRadioSelectCopyWithImpl<$Res>;
  $Res call({ResetPasswordModel model});

  $ResetPasswordModelCopyWith<$Res> get model;
}

/// @nodoc
class _$ResetPasswordRadioSelectCopyWithImpl<$Res>
    extends _$ResetPasswordEventCopyWithImpl<$Res>
    implements $ResetPasswordRadioSelectCopyWith<$Res> {
  _$ResetPasswordRadioSelectCopyWithImpl(ResetPasswordRadioSelect _value,
      $Res Function(ResetPasswordRadioSelect) _then)
      : super(_value, (v) => _then(v as ResetPasswordRadioSelect));

  @override
  ResetPasswordRadioSelect get _value =>
      super._value as ResetPasswordRadioSelect;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(ResetPasswordRadioSelect(
      model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as ResetPasswordModel,
    ));
  }

  @override
  $ResetPasswordModelCopyWith<$Res> get model {
    return $ResetPasswordModelCopyWith<$Res>(_value.model, (value) {
      return _then(_value.copyWith(model: value));
    });
  }
}

/// @nodoc

class _$ResetPasswordRadioSelect implements ResetPasswordRadioSelect {
  const _$ResetPasswordRadioSelect(this.model);

  @override
  final ResetPasswordModel model;

  @override
  String toString() {
    return 'ResetPasswordEvent.radioSelect(model: $model)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ResetPasswordRadioSelect &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(model);

  @JsonKey(ignore: true)
  @override
  $ResetPasswordRadioSelectCopyWith<ResetPasswordRadioSelect> get copyWith =>
      _$ResetPasswordRadioSelectCopyWithImpl<ResetPasswordRadioSelect>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ResetPasswordModel model) submitEmail,
    required TResult Function(ResetPasswordModel model) submitMethod,
    required TResult Function(ResetPasswordModel model) submitCode,
    required TResult Function(ResetPasswordModel model) radioSelect,
    required TResult Function(ResetPasswordModel model) typingEmail,
    required TResult Function(ResetPasswordModel model) typingCode,
    required TResult Function() cancelClick,
    required TResult Function() end,
  }) {
    return radioSelect(model);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ResetPasswordModel model)? submitEmail,
    TResult Function(ResetPasswordModel model)? submitMethod,
    TResult Function(ResetPasswordModel model)? submitCode,
    TResult Function(ResetPasswordModel model)? radioSelect,
    TResult Function(ResetPasswordModel model)? typingEmail,
    TResult Function(ResetPasswordModel model)? typingCode,
    TResult Function()? cancelClick,
    TResult Function()? end,
    required TResult orElse(),
  }) {
    if (radioSelect != null) {
      return radioSelect(model);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ResetPasswordSubmitEmail value) submitEmail,
    required TResult Function(ResetPasswordSubmitMethod value) submitMethod,
    required TResult Function(ResetPasswordSubmitCode value) submitCode,
    required TResult Function(ResetPasswordRadioSelect value) radioSelect,
    required TResult Function(ResetPasswordTypingEmail value) typingEmail,
    required TResult Function(ResetPasswordTypingCode value) typingCode,
    required TResult Function(ResetPasswordCancel value) cancelClick,
    required TResult Function(ResetPasswordEnd value) end,
  }) {
    return radioSelect(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ResetPasswordSubmitEmail value)? submitEmail,
    TResult Function(ResetPasswordSubmitMethod value)? submitMethod,
    TResult Function(ResetPasswordSubmitCode value)? submitCode,
    TResult Function(ResetPasswordRadioSelect value)? radioSelect,
    TResult Function(ResetPasswordTypingEmail value)? typingEmail,
    TResult Function(ResetPasswordTypingCode value)? typingCode,
    TResult Function(ResetPasswordCancel value)? cancelClick,
    TResult Function(ResetPasswordEnd value)? end,
    required TResult orElse(),
  }) {
    if (radioSelect != null) {
      return radioSelect(this);
    }
    return orElse();
  }
}

abstract class ResetPasswordRadioSelect implements ResetPasswordEvent {
  const factory ResetPasswordRadioSelect(ResetPasswordModel model) =
      _$ResetPasswordRadioSelect;

  ResetPasswordModel get model => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ResetPasswordRadioSelectCopyWith<ResetPasswordRadioSelect> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ResetPasswordTypingEmailCopyWith<$Res> {
  factory $ResetPasswordTypingEmailCopyWith(ResetPasswordTypingEmail value,
          $Res Function(ResetPasswordTypingEmail) then) =
      _$ResetPasswordTypingEmailCopyWithImpl<$Res>;
  $Res call({ResetPasswordModel model});

  $ResetPasswordModelCopyWith<$Res> get model;
}

/// @nodoc
class _$ResetPasswordTypingEmailCopyWithImpl<$Res>
    extends _$ResetPasswordEventCopyWithImpl<$Res>
    implements $ResetPasswordTypingEmailCopyWith<$Res> {
  _$ResetPasswordTypingEmailCopyWithImpl(ResetPasswordTypingEmail _value,
      $Res Function(ResetPasswordTypingEmail) _then)
      : super(_value, (v) => _then(v as ResetPasswordTypingEmail));

  @override
  ResetPasswordTypingEmail get _value =>
      super._value as ResetPasswordTypingEmail;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(ResetPasswordTypingEmail(
      model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as ResetPasswordModel,
    ));
  }

  @override
  $ResetPasswordModelCopyWith<$Res> get model {
    return $ResetPasswordModelCopyWith<$Res>(_value.model, (value) {
      return _then(_value.copyWith(model: value));
    });
  }
}

/// @nodoc

class _$ResetPasswordTypingEmail implements ResetPasswordTypingEmail {
  const _$ResetPasswordTypingEmail(this.model);

  @override
  final ResetPasswordModel model;

  @override
  String toString() {
    return 'ResetPasswordEvent.typingEmail(model: $model)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ResetPasswordTypingEmail &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(model);

  @JsonKey(ignore: true)
  @override
  $ResetPasswordTypingEmailCopyWith<ResetPasswordTypingEmail> get copyWith =>
      _$ResetPasswordTypingEmailCopyWithImpl<ResetPasswordTypingEmail>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ResetPasswordModel model) submitEmail,
    required TResult Function(ResetPasswordModel model) submitMethod,
    required TResult Function(ResetPasswordModel model) submitCode,
    required TResult Function(ResetPasswordModel model) radioSelect,
    required TResult Function(ResetPasswordModel model) typingEmail,
    required TResult Function(ResetPasswordModel model) typingCode,
    required TResult Function() cancelClick,
    required TResult Function() end,
  }) {
    return typingEmail(model);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ResetPasswordModel model)? submitEmail,
    TResult Function(ResetPasswordModel model)? submitMethod,
    TResult Function(ResetPasswordModel model)? submitCode,
    TResult Function(ResetPasswordModel model)? radioSelect,
    TResult Function(ResetPasswordModel model)? typingEmail,
    TResult Function(ResetPasswordModel model)? typingCode,
    TResult Function()? cancelClick,
    TResult Function()? end,
    required TResult orElse(),
  }) {
    if (typingEmail != null) {
      return typingEmail(model);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ResetPasswordSubmitEmail value) submitEmail,
    required TResult Function(ResetPasswordSubmitMethod value) submitMethod,
    required TResult Function(ResetPasswordSubmitCode value) submitCode,
    required TResult Function(ResetPasswordRadioSelect value) radioSelect,
    required TResult Function(ResetPasswordTypingEmail value) typingEmail,
    required TResult Function(ResetPasswordTypingCode value) typingCode,
    required TResult Function(ResetPasswordCancel value) cancelClick,
    required TResult Function(ResetPasswordEnd value) end,
  }) {
    return typingEmail(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ResetPasswordSubmitEmail value)? submitEmail,
    TResult Function(ResetPasswordSubmitMethod value)? submitMethod,
    TResult Function(ResetPasswordSubmitCode value)? submitCode,
    TResult Function(ResetPasswordRadioSelect value)? radioSelect,
    TResult Function(ResetPasswordTypingEmail value)? typingEmail,
    TResult Function(ResetPasswordTypingCode value)? typingCode,
    TResult Function(ResetPasswordCancel value)? cancelClick,
    TResult Function(ResetPasswordEnd value)? end,
    required TResult orElse(),
  }) {
    if (typingEmail != null) {
      return typingEmail(this);
    }
    return orElse();
  }
}

abstract class ResetPasswordTypingEmail implements ResetPasswordEvent {
  const factory ResetPasswordTypingEmail(ResetPasswordModel model) =
      _$ResetPasswordTypingEmail;

  ResetPasswordModel get model => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ResetPasswordTypingEmailCopyWith<ResetPasswordTypingEmail> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ResetPasswordTypingCodeCopyWith<$Res> {
  factory $ResetPasswordTypingCodeCopyWith(ResetPasswordTypingCode value,
          $Res Function(ResetPasswordTypingCode) then) =
      _$ResetPasswordTypingCodeCopyWithImpl<$Res>;
  $Res call({ResetPasswordModel model});

  $ResetPasswordModelCopyWith<$Res> get model;
}

/// @nodoc
class _$ResetPasswordTypingCodeCopyWithImpl<$Res>
    extends _$ResetPasswordEventCopyWithImpl<$Res>
    implements $ResetPasswordTypingCodeCopyWith<$Res> {
  _$ResetPasswordTypingCodeCopyWithImpl(ResetPasswordTypingCode _value,
      $Res Function(ResetPasswordTypingCode) _then)
      : super(_value, (v) => _then(v as ResetPasswordTypingCode));

  @override
  ResetPasswordTypingCode get _value => super._value as ResetPasswordTypingCode;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(ResetPasswordTypingCode(
      model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as ResetPasswordModel,
    ));
  }

  @override
  $ResetPasswordModelCopyWith<$Res> get model {
    return $ResetPasswordModelCopyWith<$Res>(_value.model, (value) {
      return _then(_value.copyWith(model: value));
    });
  }
}

/// @nodoc

class _$ResetPasswordTypingCode implements ResetPasswordTypingCode {
  const _$ResetPasswordTypingCode(this.model);

  @override
  final ResetPasswordModel model;

  @override
  String toString() {
    return 'ResetPasswordEvent.typingCode(model: $model)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ResetPasswordTypingCode &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(model);

  @JsonKey(ignore: true)
  @override
  $ResetPasswordTypingCodeCopyWith<ResetPasswordTypingCode> get copyWith =>
      _$ResetPasswordTypingCodeCopyWithImpl<ResetPasswordTypingCode>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ResetPasswordModel model) submitEmail,
    required TResult Function(ResetPasswordModel model) submitMethod,
    required TResult Function(ResetPasswordModel model) submitCode,
    required TResult Function(ResetPasswordModel model) radioSelect,
    required TResult Function(ResetPasswordModel model) typingEmail,
    required TResult Function(ResetPasswordModel model) typingCode,
    required TResult Function() cancelClick,
    required TResult Function() end,
  }) {
    return typingCode(model);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ResetPasswordModel model)? submitEmail,
    TResult Function(ResetPasswordModel model)? submitMethod,
    TResult Function(ResetPasswordModel model)? submitCode,
    TResult Function(ResetPasswordModel model)? radioSelect,
    TResult Function(ResetPasswordModel model)? typingEmail,
    TResult Function(ResetPasswordModel model)? typingCode,
    TResult Function()? cancelClick,
    TResult Function()? end,
    required TResult orElse(),
  }) {
    if (typingCode != null) {
      return typingCode(model);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ResetPasswordSubmitEmail value) submitEmail,
    required TResult Function(ResetPasswordSubmitMethod value) submitMethod,
    required TResult Function(ResetPasswordSubmitCode value) submitCode,
    required TResult Function(ResetPasswordRadioSelect value) radioSelect,
    required TResult Function(ResetPasswordTypingEmail value) typingEmail,
    required TResult Function(ResetPasswordTypingCode value) typingCode,
    required TResult Function(ResetPasswordCancel value) cancelClick,
    required TResult Function(ResetPasswordEnd value) end,
  }) {
    return typingCode(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ResetPasswordSubmitEmail value)? submitEmail,
    TResult Function(ResetPasswordSubmitMethod value)? submitMethod,
    TResult Function(ResetPasswordSubmitCode value)? submitCode,
    TResult Function(ResetPasswordRadioSelect value)? radioSelect,
    TResult Function(ResetPasswordTypingEmail value)? typingEmail,
    TResult Function(ResetPasswordTypingCode value)? typingCode,
    TResult Function(ResetPasswordCancel value)? cancelClick,
    TResult Function(ResetPasswordEnd value)? end,
    required TResult orElse(),
  }) {
    if (typingCode != null) {
      return typingCode(this);
    }
    return orElse();
  }
}

abstract class ResetPasswordTypingCode implements ResetPasswordEvent {
  const factory ResetPasswordTypingCode(ResetPasswordModel model) =
      _$ResetPasswordTypingCode;

  ResetPasswordModel get model => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ResetPasswordTypingCodeCopyWith<ResetPasswordTypingCode> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ResetPasswordCancelCopyWith<$Res> {
  factory $ResetPasswordCancelCopyWith(
          ResetPasswordCancel value, $Res Function(ResetPasswordCancel) then) =
      _$ResetPasswordCancelCopyWithImpl<$Res>;
}

/// @nodoc
class _$ResetPasswordCancelCopyWithImpl<$Res>
    extends _$ResetPasswordEventCopyWithImpl<$Res>
    implements $ResetPasswordCancelCopyWith<$Res> {
  _$ResetPasswordCancelCopyWithImpl(
      ResetPasswordCancel _value, $Res Function(ResetPasswordCancel) _then)
      : super(_value, (v) => _then(v as ResetPasswordCancel));

  @override
  ResetPasswordCancel get _value => super._value as ResetPasswordCancel;
}

/// @nodoc

class _$ResetPasswordCancel implements ResetPasswordCancel {
  const _$ResetPasswordCancel();

  @override
  String toString() {
    return 'ResetPasswordEvent.cancelClick()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is ResetPasswordCancel);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ResetPasswordModel model) submitEmail,
    required TResult Function(ResetPasswordModel model) submitMethod,
    required TResult Function(ResetPasswordModel model) submitCode,
    required TResult Function(ResetPasswordModel model) radioSelect,
    required TResult Function(ResetPasswordModel model) typingEmail,
    required TResult Function(ResetPasswordModel model) typingCode,
    required TResult Function() cancelClick,
    required TResult Function() end,
  }) {
    return cancelClick();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ResetPasswordModel model)? submitEmail,
    TResult Function(ResetPasswordModel model)? submitMethod,
    TResult Function(ResetPasswordModel model)? submitCode,
    TResult Function(ResetPasswordModel model)? radioSelect,
    TResult Function(ResetPasswordModel model)? typingEmail,
    TResult Function(ResetPasswordModel model)? typingCode,
    TResult Function()? cancelClick,
    TResult Function()? end,
    required TResult orElse(),
  }) {
    if (cancelClick != null) {
      return cancelClick();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ResetPasswordSubmitEmail value) submitEmail,
    required TResult Function(ResetPasswordSubmitMethod value) submitMethod,
    required TResult Function(ResetPasswordSubmitCode value) submitCode,
    required TResult Function(ResetPasswordRadioSelect value) radioSelect,
    required TResult Function(ResetPasswordTypingEmail value) typingEmail,
    required TResult Function(ResetPasswordTypingCode value) typingCode,
    required TResult Function(ResetPasswordCancel value) cancelClick,
    required TResult Function(ResetPasswordEnd value) end,
  }) {
    return cancelClick(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ResetPasswordSubmitEmail value)? submitEmail,
    TResult Function(ResetPasswordSubmitMethod value)? submitMethod,
    TResult Function(ResetPasswordSubmitCode value)? submitCode,
    TResult Function(ResetPasswordRadioSelect value)? radioSelect,
    TResult Function(ResetPasswordTypingEmail value)? typingEmail,
    TResult Function(ResetPasswordTypingCode value)? typingCode,
    TResult Function(ResetPasswordCancel value)? cancelClick,
    TResult Function(ResetPasswordEnd value)? end,
    required TResult orElse(),
  }) {
    if (cancelClick != null) {
      return cancelClick(this);
    }
    return orElse();
  }
}

abstract class ResetPasswordCancel implements ResetPasswordEvent {
  const factory ResetPasswordCancel() = _$ResetPasswordCancel;
}

/// @nodoc
abstract class $ResetPasswordEndCopyWith<$Res> {
  factory $ResetPasswordEndCopyWith(
          ResetPasswordEnd value, $Res Function(ResetPasswordEnd) then) =
      _$ResetPasswordEndCopyWithImpl<$Res>;
}

/// @nodoc
class _$ResetPasswordEndCopyWithImpl<$Res>
    extends _$ResetPasswordEventCopyWithImpl<$Res>
    implements $ResetPasswordEndCopyWith<$Res> {
  _$ResetPasswordEndCopyWithImpl(
      ResetPasswordEnd _value, $Res Function(ResetPasswordEnd) _then)
      : super(_value, (v) => _then(v as ResetPasswordEnd));

  @override
  ResetPasswordEnd get _value => super._value as ResetPasswordEnd;
}

/// @nodoc

class _$ResetPasswordEnd implements ResetPasswordEnd {
  const _$ResetPasswordEnd();

  @override
  String toString() {
    return 'ResetPasswordEvent.end()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is ResetPasswordEnd);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ResetPasswordModel model) submitEmail,
    required TResult Function(ResetPasswordModel model) submitMethod,
    required TResult Function(ResetPasswordModel model) submitCode,
    required TResult Function(ResetPasswordModel model) radioSelect,
    required TResult Function(ResetPasswordModel model) typingEmail,
    required TResult Function(ResetPasswordModel model) typingCode,
    required TResult Function() cancelClick,
    required TResult Function() end,
  }) {
    return end();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ResetPasswordModel model)? submitEmail,
    TResult Function(ResetPasswordModel model)? submitMethod,
    TResult Function(ResetPasswordModel model)? submitCode,
    TResult Function(ResetPasswordModel model)? radioSelect,
    TResult Function(ResetPasswordModel model)? typingEmail,
    TResult Function(ResetPasswordModel model)? typingCode,
    TResult Function()? cancelClick,
    TResult Function()? end,
    required TResult orElse(),
  }) {
    if (end != null) {
      return end();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ResetPasswordSubmitEmail value) submitEmail,
    required TResult Function(ResetPasswordSubmitMethod value) submitMethod,
    required TResult Function(ResetPasswordSubmitCode value) submitCode,
    required TResult Function(ResetPasswordRadioSelect value) radioSelect,
    required TResult Function(ResetPasswordTypingEmail value) typingEmail,
    required TResult Function(ResetPasswordTypingCode value) typingCode,
    required TResult Function(ResetPasswordCancel value) cancelClick,
    required TResult Function(ResetPasswordEnd value) end,
  }) {
    return end(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ResetPasswordSubmitEmail value)? submitEmail,
    TResult Function(ResetPasswordSubmitMethod value)? submitMethod,
    TResult Function(ResetPasswordSubmitCode value)? submitCode,
    TResult Function(ResetPasswordRadioSelect value)? radioSelect,
    TResult Function(ResetPasswordTypingEmail value)? typingEmail,
    TResult Function(ResetPasswordTypingCode value)? typingCode,
    TResult Function(ResetPasswordCancel value)? cancelClick,
    TResult Function(ResetPasswordEnd value)? end,
    required TResult orElse(),
  }) {
    if (end != null) {
      return end(this);
    }
    return orElse();
  }
}

abstract class ResetPasswordEnd implements ResetPasswordEvent {
  const factory ResetPasswordEnd() = _$ResetPasswordEnd;
}
