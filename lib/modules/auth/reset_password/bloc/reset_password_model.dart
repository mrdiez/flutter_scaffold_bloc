import 'package:flutter_scaffold_bloc/modules/auth/reset_password/bloc/reset_password_bloc.dart';
import 'package:flutter_scaffold_bloc/utils/string.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'reset_password_model.freezed.dart';

/// An immutable generated class model
/// To generate code execute: flutter pub run build_runner build
/// For more info check the README # How to generate code (with freezed)
@freezed
class ResetPasswordModel with _$ResetPasswordModel {
  const ResetPasswordModel._();
  const factory ResetPasswordModel({
    String? login,
    String? loginError,
    List<ResetPasswordMethod>? methods,
    ResetPasswordMethod? method,
    String? methodError,
    String? code,
    String? codeError,
    String? newPassword,
  }) = _ResetPasswordModel;

  bool get isLoginError => StringTool.notNull(loginError);
  bool get isMethodError => StringTool.notNull(methodError);
  bool get isCodeError => StringTool.notNull(codeError);
  bool get isError => isLoginError || isMethodError || isCodeError;

  Map<ResetPasswordMethod, String> get labelsByMethods =>
      Map.fromEntries((methods ?? []).map((method) => MapEntry(method, ResetPasswordBloc.MethodLabels[method]!)));
}
