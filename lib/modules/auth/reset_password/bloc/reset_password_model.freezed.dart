// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'reset_password_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$ResetPasswordModelTearOff {
  const _$ResetPasswordModelTearOff();

  _ResetPasswordModel call(
      {String? login,
      String? loginError,
      List<ResetPasswordMethod>? methods,
      ResetPasswordMethod? method,
      String? methodError,
      String? code,
      String? codeError,
      String? newPassword}) {
    return _ResetPasswordModel(
      login: login,
      loginError: loginError,
      methods: methods,
      method: method,
      methodError: methodError,
      code: code,
      codeError: codeError,
      newPassword: newPassword,
    );
  }
}

/// @nodoc
const $ResetPasswordModel = _$ResetPasswordModelTearOff();

/// @nodoc
mixin _$ResetPasswordModel {
  String? get login => throw _privateConstructorUsedError;
  String? get loginError => throw _privateConstructorUsedError;
  List<ResetPasswordMethod>? get methods => throw _privateConstructorUsedError;
  ResetPasswordMethod? get method => throw _privateConstructorUsedError;
  String? get methodError => throw _privateConstructorUsedError;
  String? get code => throw _privateConstructorUsedError;
  String? get codeError => throw _privateConstructorUsedError;
  String? get newPassword => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ResetPasswordModelCopyWith<ResetPasswordModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ResetPasswordModelCopyWith<$Res> {
  factory $ResetPasswordModelCopyWith(
          ResetPasswordModel value, $Res Function(ResetPasswordModel) then) =
      _$ResetPasswordModelCopyWithImpl<$Res>;
  $Res call(
      {String? login,
      String? loginError,
      List<ResetPasswordMethod>? methods,
      ResetPasswordMethod? method,
      String? methodError,
      String? code,
      String? codeError,
      String? newPassword});
}

/// @nodoc
class _$ResetPasswordModelCopyWithImpl<$Res>
    implements $ResetPasswordModelCopyWith<$Res> {
  _$ResetPasswordModelCopyWithImpl(this._value, this._then);

  final ResetPasswordModel _value;
  // ignore: unused_field
  final $Res Function(ResetPasswordModel) _then;

  @override
  $Res call({
    Object? login = freezed,
    Object? loginError = freezed,
    Object? methods = freezed,
    Object? method = freezed,
    Object? methodError = freezed,
    Object? code = freezed,
    Object? codeError = freezed,
    Object? newPassword = freezed,
  }) {
    return _then(_value.copyWith(
      login: login == freezed
          ? _value.login
          : login // ignore: cast_nullable_to_non_nullable
              as String?,
      loginError: loginError == freezed
          ? _value.loginError
          : loginError // ignore: cast_nullable_to_non_nullable
              as String?,
      methods: methods == freezed
          ? _value.methods
          : methods // ignore: cast_nullable_to_non_nullable
              as List<ResetPasswordMethod>?,
      method: method == freezed
          ? _value.method
          : method // ignore: cast_nullable_to_non_nullable
              as ResetPasswordMethod?,
      methodError: methodError == freezed
          ? _value.methodError
          : methodError // ignore: cast_nullable_to_non_nullable
              as String?,
      code: code == freezed
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String?,
      codeError: codeError == freezed
          ? _value.codeError
          : codeError // ignore: cast_nullable_to_non_nullable
              as String?,
      newPassword: newPassword == freezed
          ? _value.newPassword
          : newPassword // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$ResetPasswordModelCopyWith<$Res>
    implements $ResetPasswordModelCopyWith<$Res> {
  factory _$ResetPasswordModelCopyWith(
          _ResetPasswordModel value, $Res Function(_ResetPasswordModel) then) =
      __$ResetPasswordModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String? login,
      String? loginError,
      List<ResetPasswordMethod>? methods,
      ResetPasswordMethod? method,
      String? methodError,
      String? code,
      String? codeError,
      String? newPassword});
}

/// @nodoc
class __$ResetPasswordModelCopyWithImpl<$Res>
    extends _$ResetPasswordModelCopyWithImpl<$Res>
    implements _$ResetPasswordModelCopyWith<$Res> {
  __$ResetPasswordModelCopyWithImpl(
      _ResetPasswordModel _value, $Res Function(_ResetPasswordModel) _then)
      : super(_value, (v) => _then(v as _ResetPasswordModel));

  @override
  _ResetPasswordModel get _value => super._value as _ResetPasswordModel;

  @override
  $Res call({
    Object? login = freezed,
    Object? loginError = freezed,
    Object? methods = freezed,
    Object? method = freezed,
    Object? methodError = freezed,
    Object? code = freezed,
    Object? codeError = freezed,
    Object? newPassword = freezed,
  }) {
    return _then(_ResetPasswordModel(
      login: login == freezed
          ? _value.login
          : login // ignore: cast_nullable_to_non_nullable
              as String?,
      loginError: loginError == freezed
          ? _value.loginError
          : loginError // ignore: cast_nullable_to_non_nullable
              as String?,
      methods: methods == freezed
          ? _value.methods
          : methods // ignore: cast_nullable_to_non_nullable
              as List<ResetPasswordMethod>?,
      method: method == freezed
          ? _value.method
          : method // ignore: cast_nullable_to_non_nullable
              as ResetPasswordMethod?,
      methodError: methodError == freezed
          ? _value.methodError
          : methodError // ignore: cast_nullable_to_non_nullable
              as String?,
      code: code == freezed
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String?,
      codeError: codeError == freezed
          ? _value.codeError
          : codeError // ignore: cast_nullable_to_non_nullable
              as String?,
      newPassword: newPassword == freezed
          ? _value.newPassword
          : newPassword // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$_ResetPasswordModel extends _ResetPasswordModel {
  const _$_ResetPasswordModel(
      {this.login,
      this.loginError,
      this.methods,
      this.method,
      this.methodError,
      this.code,
      this.codeError,
      this.newPassword})
      : super._();

  @override
  final String? login;
  @override
  final String? loginError;
  @override
  final List<ResetPasswordMethod>? methods;
  @override
  final ResetPasswordMethod? method;
  @override
  final String? methodError;
  @override
  final String? code;
  @override
  final String? codeError;
  @override
  final String? newPassword;

  @override
  String toString() {
    return 'ResetPasswordModel(login: $login, loginError: $loginError, methods: $methods, method: $method, methodError: $methodError, code: $code, codeError: $codeError, newPassword: $newPassword)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ResetPasswordModel &&
            (identical(other.login, login) ||
                const DeepCollectionEquality().equals(other.login, login)) &&
            (identical(other.loginError, loginError) ||
                const DeepCollectionEquality()
                    .equals(other.loginError, loginError)) &&
            (identical(other.methods, methods) ||
                const DeepCollectionEquality()
                    .equals(other.methods, methods)) &&
            (identical(other.method, method) ||
                const DeepCollectionEquality().equals(other.method, method)) &&
            (identical(other.methodError, methodError) ||
                const DeepCollectionEquality()
                    .equals(other.methodError, methodError)) &&
            (identical(other.code, code) ||
                const DeepCollectionEquality().equals(other.code, code)) &&
            (identical(other.codeError, codeError) ||
                const DeepCollectionEquality()
                    .equals(other.codeError, codeError)) &&
            (identical(other.newPassword, newPassword) ||
                const DeepCollectionEquality()
                    .equals(other.newPassword, newPassword)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(login) ^
      const DeepCollectionEquality().hash(loginError) ^
      const DeepCollectionEquality().hash(methods) ^
      const DeepCollectionEquality().hash(method) ^
      const DeepCollectionEquality().hash(methodError) ^
      const DeepCollectionEquality().hash(code) ^
      const DeepCollectionEquality().hash(codeError) ^
      const DeepCollectionEquality().hash(newPassword);

  @JsonKey(ignore: true)
  @override
  _$ResetPasswordModelCopyWith<_ResetPasswordModel> get copyWith =>
      __$ResetPasswordModelCopyWithImpl<_ResetPasswordModel>(this, _$identity);
}

abstract class _ResetPasswordModel extends ResetPasswordModel {
  const factory _ResetPasswordModel(
      {String? login,
      String? loginError,
      List<ResetPasswordMethod>? methods,
      ResetPasswordMethod? method,
      String? methodError,
      String? code,
      String? codeError,
      String? newPassword}) = _$_ResetPasswordModel;
  const _ResetPasswordModel._() : super._();

  @override
  String? get login => throw _privateConstructorUsedError;
  @override
  String? get loginError => throw _privateConstructorUsedError;
  @override
  List<ResetPasswordMethod>? get methods => throw _privateConstructorUsedError;
  @override
  ResetPasswordMethod? get method => throw _privateConstructorUsedError;
  @override
  String? get methodError => throw _privateConstructorUsedError;
  @override
  String? get code => throw _privateConstructorUsedError;
  @override
  String? get codeError => throw _privateConstructorUsedError;
  @override
  String? get newPassword => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$ResetPasswordModelCopyWith<_ResetPasswordModel> get copyWith =>
      throw _privateConstructorUsedError;
}
