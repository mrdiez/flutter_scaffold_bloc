import 'package:flutter_scaffold_bloc/modules/auth/reset_password/bloc/reset_password_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'reset_password_state.freezed.dart';

/// An immutable class that define UI states
/// To generate code execute: flutter pub run build_runner build
/// For more info check the README # How to generate code (with freezed)
@freezed
class ResetPasswordState with _$ResetPasswordState {
  const ResetPasswordState._();
  const factory ResetPasswordState.init([@Default(ResetPasswordModel()) ResetPasswordModel model]) = ResetPasswordInit;
  const factory ResetPasswordState.loading(ResetPasswordModel model) = ResetPasswordLoading;
  const factory ResetPasswordState.typingError(ResetPasswordModel model) = ResetPasswordTypingError;
  const factory ResetPasswordState.submitError(ResetPasswordModel model) = ResetPasswordSubmitError;
  const factory ResetPasswordState.chooseMethod(ResetPasswordModel model) = ResetPasswordChooseMethod;
  const factory ResetPasswordState.confirmReset(ResetPasswordModel model) = ResetPasswordConfirmReset;
  const factory ResetPasswordState.success(ResetPasswordModel model) = ResetPasswordSuccess;
}
