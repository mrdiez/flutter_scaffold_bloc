// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'reset_password_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$ResetPasswordStateTearOff {
  const _$ResetPasswordStateTearOff();

  ResetPasswordInit init(
      [ResetPasswordModel model = const ResetPasswordModel()]) {
    return ResetPasswordInit(
      model,
    );
  }

  ResetPasswordLoading loading(ResetPasswordModel model) {
    return ResetPasswordLoading(
      model,
    );
  }

  ResetPasswordTypingError typingError(ResetPasswordModel model) {
    return ResetPasswordTypingError(
      model,
    );
  }

  ResetPasswordSubmitError submitError(ResetPasswordModel model) {
    return ResetPasswordSubmitError(
      model,
    );
  }

  ResetPasswordChooseMethod chooseMethod(ResetPasswordModel model) {
    return ResetPasswordChooseMethod(
      model,
    );
  }

  ResetPasswordConfirmReset confirmReset(ResetPasswordModel model) {
    return ResetPasswordConfirmReset(
      model,
    );
  }

  ResetPasswordSuccess success(ResetPasswordModel model) {
    return ResetPasswordSuccess(
      model,
    );
  }
}

/// @nodoc
const $ResetPasswordState = _$ResetPasswordStateTearOff();

/// @nodoc
mixin _$ResetPasswordState {
  ResetPasswordModel get model => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ResetPasswordModel model) init,
    required TResult Function(ResetPasswordModel model) loading,
    required TResult Function(ResetPasswordModel model) typingError,
    required TResult Function(ResetPasswordModel model) submitError,
    required TResult Function(ResetPasswordModel model) chooseMethod,
    required TResult Function(ResetPasswordModel model) confirmReset,
    required TResult Function(ResetPasswordModel model) success,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ResetPasswordModel model)? init,
    TResult Function(ResetPasswordModel model)? loading,
    TResult Function(ResetPasswordModel model)? typingError,
    TResult Function(ResetPasswordModel model)? submitError,
    TResult Function(ResetPasswordModel model)? chooseMethod,
    TResult Function(ResetPasswordModel model)? confirmReset,
    TResult Function(ResetPasswordModel model)? success,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ResetPasswordInit value) init,
    required TResult Function(ResetPasswordLoading value) loading,
    required TResult Function(ResetPasswordTypingError value) typingError,
    required TResult Function(ResetPasswordSubmitError value) submitError,
    required TResult Function(ResetPasswordChooseMethod value) chooseMethod,
    required TResult Function(ResetPasswordConfirmReset value) confirmReset,
    required TResult Function(ResetPasswordSuccess value) success,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ResetPasswordInit value)? init,
    TResult Function(ResetPasswordLoading value)? loading,
    TResult Function(ResetPasswordTypingError value)? typingError,
    TResult Function(ResetPasswordSubmitError value)? submitError,
    TResult Function(ResetPasswordChooseMethod value)? chooseMethod,
    TResult Function(ResetPasswordConfirmReset value)? confirmReset,
    TResult Function(ResetPasswordSuccess value)? success,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ResetPasswordStateCopyWith<ResetPasswordState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ResetPasswordStateCopyWith<$Res> {
  factory $ResetPasswordStateCopyWith(
          ResetPasswordState value, $Res Function(ResetPasswordState) then) =
      _$ResetPasswordStateCopyWithImpl<$Res>;
  $Res call({ResetPasswordModel model});

  $ResetPasswordModelCopyWith<$Res> get model;
}

/// @nodoc
class _$ResetPasswordStateCopyWithImpl<$Res>
    implements $ResetPasswordStateCopyWith<$Res> {
  _$ResetPasswordStateCopyWithImpl(this._value, this._then);

  final ResetPasswordState _value;
  // ignore: unused_field
  final $Res Function(ResetPasswordState) _then;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(_value.copyWith(
      model: model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as ResetPasswordModel,
    ));
  }

  @override
  $ResetPasswordModelCopyWith<$Res> get model {
    return $ResetPasswordModelCopyWith<$Res>(_value.model, (value) {
      return _then(_value.copyWith(model: value));
    });
  }
}

/// @nodoc
abstract class $ResetPasswordInitCopyWith<$Res>
    implements $ResetPasswordStateCopyWith<$Res> {
  factory $ResetPasswordInitCopyWith(
          ResetPasswordInit value, $Res Function(ResetPasswordInit) then) =
      _$ResetPasswordInitCopyWithImpl<$Res>;
  @override
  $Res call({ResetPasswordModel model});

  @override
  $ResetPasswordModelCopyWith<$Res> get model;
}

/// @nodoc
class _$ResetPasswordInitCopyWithImpl<$Res>
    extends _$ResetPasswordStateCopyWithImpl<$Res>
    implements $ResetPasswordInitCopyWith<$Res> {
  _$ResetPasswordInitCopyWithImpl(
      ResetPasswordInit _value, $Res Function(ResetPasswordInit) _then)
      : super(_value, (v) => _then(v as ResetPasswordInit));

  @override
  ResetPasswordInit get _value => super._value as ResetPasswordInit;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(ResetPasswordInit(
      model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as ResetPasswordModel,
    ));
  }
}

/// @nodoc

class _$ResetPasswordInit extends ResetPasswordInit {
  const _$ResetPasswordInit([this.model = const ResetPasswordModel()])
      : super._();

  @JsonKey(defaultValue: const ResetPasswordModel())
  @override
  final ResetPasswordModel model;

  @override
  String toString() {
    return 'ResetPasswordState.init(model: $model)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ResetPasswordInit &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(model);

  @JsonKey(ignore: true)
  @override
  $ResetPasswordInitCopyWith<ResetPasswordInit> get copyWith =>
      _$ResetPasswordInitCopyWithImpl<ResetPasswordInit>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ResetPasswordModel model) init,
    required TResult Function(ResetPasswordModel model) loading,
    required TResult Function(ResetPasswordModel model) typingError,
    required TResult Function(ResetPasswordModel model) submitError,
    required TResult Function(ResetPasswordModel model) chooseMethod,
    required TResult Function(ResetPasswordModel model) confirmReset,
    required TResult Function(ResetPasswordModel model) success,
  }) {
    return init(model);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ResetPasswordModel model)? init,
    TResult Function(ResetPasswordModel model)? loading,
    TResult Function(ResetPasswordModel model)? typingError,
    TResult Function(ResetPasswordModel model)? submitError,
    TResult Function(ResetPasswordModel model)? chooseMethod,
    TResult Function(ResetPasswordModel model)? confirmReset,
    TResult Function(ResetPasswordModel model)? success,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(model);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ResetPasswordInit value) init,
    required TResult Function(ResetPasswordLoading value) loading,
    required TResult Function(ResetPasswordTypingError value) typingError,
    required TResult Function(ResetPasswordSubmitError value) submitError,
    required TResult Function(ResetPasswordChooseMethod value) chooseMethod,
    required TResult Function(ResetPasswordConfirmReset value) confirmReset,
    required TResult Function(ResetPasswordSuccess value) success,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ResetPasswordInit value)? init,
    TResult Function(ResetPasswordLoading value)? loading,
    TResult Function(ResetPasswordTypingError value)? typingError,
    TResult Function(ResetPasswordSubmitError value)? submitError,
    TResult Function(ResetPasswordChooseMethod value)? chooseMethod,
    TResult Function(ResetPasswordConfirmReset value)? confirmReset,
    TResult Function(ResetPasswordSuccess value)? success,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class ResetPasswordInit extends ResetPasswordState {
  const factory ResetPasswordInit([ResetPasswordModel model]) =
      _$ResetPasswordInit;
  const ResetPasswordInit._() : super._();

  @override
  ResetPasswordModel get model => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  $ResetPasswordInitCopyWith<ResetPasswordInit> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ResetPasswordLoadingCopyWith<$Res>
    implements $ResetPasswordStateCopyWith<$Res> {
  factory $ResetPasswordLoadingCopyWith(ResetPasswordLoading value,
          $Res Function(ResetPasswordLoading) then) =
      _$ResetPasswordLoadingCopyWithImpl<$Res>;
  @override
  $Res call({ResetPasswordModel model});

  @override
  $ResetPasswordModelCopyWith<$Res> get model;
}

/// @nodoc
class _$ResetPasswordLoadingCopyWithImpl<$Res>
    extends _$ResetPasswordStateCopyWithImpl<$Res>
    implements $ResetPasswordLoadingCopyWith<$Res> {
  _$ResetPasswordLoadingCopyWithImpl(
      ResetPasswordLoading _value, $Res Function(ResetPasswordLoading) _then)
      : super(_value, (v) => _then(v as ResetPasswordLoading));

  @override
  ResetPasswordLoading get _value => super._value as ResetPasswordLoading;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(ResetPasswordLoading(
      model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as ResetPasswordModel,
    ));
  }
}

/// @nodoc

class _$ResetPasswordLoading extends ResetPasswordLoading {
  const _$ResetPasswordLoading(this.model) : super._();

  @override
  final ResetPasswordModel model;

  @override
  String toString() {
    return 'ResetPasswordState.loading(model: $model)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ResetPasswordLoading &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(model);

  @JsonKey(ignore: true)
  @override
  $ResetPasswordLoadingCopyWith<ResetPasswordLoading> get copyWith =>
      _$ResetPasswordLoadingCopyWithImpl<ResetPasswordLoading>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ResetPasswordModel model) init,
    required TResult Function(ResetPasswordModel model) loading,
    required TResult Function(ResetPasswordModel model) typingError,
    required TResult Function(ResetPasswordModel model) submitError,
    required TResult Function(ResetPasswordModel model) chooseMethod,
    required TResult Function(ResetPasswordModel model) confirmReset,
    required TResult Function(ResetPasswordModel model) success,
  }) {
    return loading(model);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ResetPasswordModel model)? init,
    TResult Function(ResetPasswordModel model)? loading,
    TResult Function(ResetPasswordModel model)? typingError,
    TResult Function(ResetPasswordModel model)? submitError,
    TResult Function(ResetPasswordModel model)? chooseMethod,
    TResult Function(ResetPasswordModel model)? confirmReset,
    TResult Function(ResetPasswordModel model)? success,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(model);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ResetPasswordInit value) init,
    required TResult Function(ResetPasswordLoading value) loading,
    required TResult Function(ResetPasswordTypingError value) typingError,
    required TResult Function(ResetPasswordSubmitError value) submitError,
    required TResult Function(ResetPasswordChooseMethod value) chooseMethod,
    required TResult Function(ResetPasswordConfirmReset value) confirmReset,
    required TResult Function(ResetPasswordSuccess value) success,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ResetPasswordInit value)? init,
    TResult Function(ResetPasswordLoading value)? loading,
    TResult Function(ResetPasswordTypingError value)? typingError,
    TResult Function(ResetPasswordSubmitError value)? submitError,
    TResult Function(ResetPasswordChooseMethod value)? chooseMethod,
    TResult Function(ResetPasswordConfirmReset value)? confirmReset,
    TResult Function(ResetPasswordSuccess value)? success,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class ResetPasswordLoading extends ResetPasswordState {
  const factory ResetPasswordLoading(ResetPasswordModel model) =
      _$ResetPasswordLoading;
  const ResetPasswordLoading._() : super._();

  @override
  ResetPasswordModel get model => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  $ResetPasswordLoadingCopyWith<ResetPasswordLoading> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ResetPasswordTypingErrorCopyWith<$Res>
    implements $ResetPasswordStateCopyWith<$Res> {
  factory $ResetPasswordTypingErrorCopyWith(ResetPasswordTypingError value,
          $Res Function(ResetPasswordTypingError) then) =
      _$ResetPasswordTypingErrorCopyWithImpl<$Res>;
  @override
  $Res call({ResetPasswordModel model});

  @override
  $ResetPasswordModelCopyWith<$Res> get model;
}

/// @nodoc
class _$ResetPasswordTypingErrorCopyWithImpl<$Res>
    extends _$ResetPasswordStateCopyWithImpl<$Res>
    implements $ResetPasswordTypingErrorCopyWith<$Res> {
  _$ResetPasswordTypingErrorCopyWithImpl(ResetPasswordTypingError _value,
      $Res Function(ResetPasswordTypingError) _then)
      : super(_value, (v) => _then(v as ResetPasswordTypingError));

  @override
  ResetPasswordTypingError get _value =>
      super._value as ResetPasswordTypingError;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(ResetPasswordTypingError(
      model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as ResetPasswordModel,
    ));
  }
}

/// @nodoc

class _$ResetPasswordTypingError extends ResetPasswordTypingError {
  const _$ResetPasswordTypingError(this.model) : super._();

  @override
  final ResetPasswordModel model;

  @override
  String toString() {
    return 'ResetPasswordState.typingError(model: $model)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ResetPasswordTypingError &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(model);

  @JsonKey(ignore: true)
  @override
  $ResetPasswordTypingErrorCopyWith<ResetPasswordTypingError> get copyWith =>
      _$ResetPasswordTypingErrorCopyWithImpl<ResetPasswordTypingError>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ResetPasswordModel model) init,
    required TResult Function(ResetPasswordModel model) loading,
    required TResult Function(ResetPasswordModel model) typingError,
    required TResult Function(ResetPasswordModel model) submitError,
    required TResult Function(ResetPasswordModel model) chooseMethod,
    required TResult Function(ResetPasswordModel model) confirmReset,
    required TResult Function(ResetPasswordModel model) success,
  }) {
    return typingError(model);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ResetPasswordModel model)? init,
    TResult Function(ResetPasswordModel model)? loading,
    TResult Function(ResetPasswordModel model)? typingError,
    TResult Function(ResetPasswordModel model)? submitError,
    TResult Function(ResetPasswordModel model)? chooseMethod,
    TResult Function(ResetPasswordModel model)? confirmReset,
    TResult Function(ResetPasswordModel model)? success,
    required TResult orElse(),
  }) {
    if (typingError != null) {
      return typingError(model);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ResetPasswordInit value) init,
    required TResult Function(ResetPasswordLoading value) loading,
    required TResult Function(ResetPasswordTypingError value) typingError,
    required TResult Function(ResetPasswordSubmitError value) submitError,
    required TResult Function(ResetPasswordChooseMethod value) chooseMethod,
    required TResult Function(ResetPasswordConfirmReset value) confirmReset,
    required TResult Function(ResetPasswordSuccess value) success,
  }) {
    return typingError(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ResetPasswordInit value)? init,
    TResult Function(ResetPasswordLoading value)? loading,
    TResult Function(ResetPasswordTypingError value)? typingError,
    TResult Function(ResetPasswordSubmitError value)? submitError,
    TResult Function(ResetPasswordChooseMethod value)? chooseMethod,
    TResult Function(ResetPasswordConfirmReset value)? confirmReset,
    TResult Function(ResetPasswordSuccess value)? success,
    required TResult orElse(),
  }) {
    if (typingError != null) {
      return typingError(this);
    }
    return orElse();
  }
}

abstract class ResetPasswordTypingError extends ResetPasswordState {
  const factory ResetPasswordTypingError(ResetPasswordModel model) =
      _$ResetPasswordTypingError;
  const ResetPasswordTypingError._() : super._();

  @override
  ResetPasswordModel get model => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  $ResetPasswordTypingErrorCopyWith<ResetPasswordTypingError> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ResetPasswordSubmitErrorCopyWith<$Res>
    implements $ResetPasswordStateCopyWith<$Res> {
  factory $ResetPasswordSubmitErrorCopyWith(ResetPasswordSubmitError value,
          $Res Function(ResetPasswordSubmitError) then) =
      _$ResetPasswordSubmitErrorCopyWithImpl<$Res>;
  @override
  $Res call({ResetPasswordModel model});

  @override
  $ResetPasswordModelCopyWith<$Res> get model;
}

/// @nodoc
class _$ResetPasswordSubmitErrorCopyWithImpl<$Res>
    extends _$ResetPasswordStateCopyWithImpl<$Res>
    implements $ResetPasswordSubmitErrorCopyWith<$Res> {
  _$ResetPasswordSubmitErrorCopyWithImpl(ResetPasswordSubmitError _value,
      $Res Function(ResetPasswordSubmitError) _then)
      : super(_value, (v) => _then(v as ResetPasswordSubmitError));

  @override
  ResetPasswordSubmitError get _value =>
      super._value as ResetPasswordSubmitError;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(ResetPasswordSubmitError(
      model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as ResetPasswordModel,
    ));
  }
}

/// @nodoc

class _$ResetPasswordSubmitError extends ResetPasswordSubmitError {
  const _$ResetPasswordSubmitError(this.model) : super._();

  @override
  final ResetPasswordModel model;

  @override
  String toString() {
    return 'ResetPasswordState.submitError(model: $model)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ResetPasswordSubmitError &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(model);

  @JsonKey(ignore: true)
  @override
  $ResetPasswordSubmitErrorCopyWith<ResetPasswordSubmitError> get copyWith =>
      _$ResetPasswordSubmitErrorCopyWithImpl<ResetPasswordSubmitError>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ResetPasswordModel model) init,
    required TResult Function(ResetPasswordModel model) loading,
    required TResult Function(ResetPasswordModel model) typingError,
    required TResult Function(ResetPasswordModel model) submitError,
    required TResult Function(ResetPasswordModel model) chooseMethod,
    required TResult Function(ResetPasswordModel model) confirmReset,
    required TResult Function(ResetPasswordModel model) success,
  }) {
    return submitError(model);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ResetPasswordModel model)? init,
    TResult Function(ResetPasswordModel model)? loading,
    TResult Function(ResetPasswordModel model)? typingError,
    TResult Function(ResetPasswordModel model)? submitError,
    TResult Function(ResetPasswordModel model)? chooseMethod,
    TResult Function(ResetPasswordModel model)? confirmReset,
    TResult Function(ResetPasswordModel model)? success,
    required TResult orElse(),
  }) {
    if (submitError != null) {
      return submitError(model);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ResetPasswordInit value) init,
    required TResult Function(ResetPasswordLoading value) loading,
    required TResult Function(ResetPasswordTypingError value) typingError,
    required TResult Function(ResetPasswordSubmitError value) submitError,
    required TResult Function(ResetPasswordChooseMethod value) chooseMethod,
    required TResult Function(ResetPasswordConfirmReset value) confirmReset,
    required TResult Function(ResetPasswordSuccess value) success,
  }) {
    return submitError(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ResetPasswordInit value)? init,
    TResult Function(ResetPasswordLoading value)? loading,
    TResult Function(ResetPasswordTypingError value)? typingError,
    TResult Function(ResetPasswordSubmitError value)? submitError,
    TResult Function(ResetPasswordChooseMethod value)? chooseMethod,
    TResult Function(ResetPasswordConfirmReset value)? confirmReset,
    TResult Function(ResetPasswordSuccess value)? success,
    required TResult orElse(),
  }) {
    if (submitError != null) {
      return submitError(this);
    }
    return orElse();
  }
}

abstract class ResetPasswordSubmitError extends ResetPasswordState {
  const factory ResetPasswordSubmitError(ResetPasswordModel model) =
      _$ResetPasswordSubmitError;
  const ResetPasswordSubmitError._() : super._();

  @override
  ResetPasswordModel get model => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  $ResetPasswordSubmitErrorCopyWith<ResetPasswordSubmitError> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ResetPasswordChooseMethodCopyWith<$Res>
    implements $ResetPasswordStateCopyWith<$Res> {
  factory $ResetPasswordChooseMethodCopyWith(ResetPasswordChooseMethod value,
          $Res Function(ResetPasswordChooseMethod) then) =
      _$ResetPasswordChooseMethodCopyWithImpl<$Res>;
  @override
  $Res call({ResetPasswordModel model});

  @override
  $ResetPasswordModelCopyWith<$Res> get model;
}

/// @nodoc
class _$ResetPasswordChooseMethodCopyWithImpl<$Res>
    extends _$ResetPasswordStateCopyWithImpl<$Res>
    implements $ResetPasswordChooseMethodCopyWith<$Res> {
  _$ResetPasswordChooseMethodCopyWithImpl(ResetPasswordChooseMethod _value,
      $Res Function(ResetPasswordChooseMethod) _then)
      : super(_value, (v) => _then(v as ResetPasswordChooseMethod));

  @override
  ResetPasswordChooseMethod get _value =>
      super._value as ResetPasswordChooseMethod;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(ResetPasswordChooseMethod(
      model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as ResetPasswordModel,
    ));
  }
}

/// @nodoc

class _$ResetPasswordChooseMethod extends ResetPasswordChooseMethod {
  const _$ResetPasswordChooseMethod(this.model) : super._();

  @override
  final ResetPasswordModel model;

  @override
  String toString() {
    return 'ResetPasswordState.chooseMethod(model: $model)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ResetPasswordChooseMethod &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(model);

  @JsonKey(ignore: true)
  @override
  $ResetPasswordChooseMethodCopyWith<ResetPasswordChooseMethod> get copyWith =>
      _$ResetPasswordChooseMethodCopyWithImpl<ResetPasswordChooseMethod>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ResetPasswordModel model) init,
    required TResult Function(ResetPasswordModel model) loading,
    required TResult Function(ResetPasswordModel model) typingError,
    required TResult Function(ResetPasswordModel model) submitError,
    required TResult Function(ResetPasswordModel model) chooseMethod,
    required TResult Function(ResetPasswordModel model) confirmReset,
    required TResult Function(ResetPasswordModel model) success,
  }) {
    return chooseMethod(model);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ResetPasswordModel model)? init,
    TResult Function(ResetPasswordModel model)? loading,
    TResult Function(ResetPasswordModel model)? typingError,
    TResult Function(ResetPasswordModel model)? submitError,
    TResult Function(ResetPasswordModel model)? chooseMethod,
    TResult Function(ResetPasswordModel model)? confirmReset,
    TResult Function(ResetPasswordModel model)? success,
    required TResult orElse(),
  }) {
    if (chooseMethod != null) {
      return chooseMethod(model);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ResetPasswordInit value) init,
    required TResult Function(ResetPasswordLoading value) loading,
    required TResult Function(ResetPasswordTypingError value) typingError,
    required TResult Function(ResetPasswordSubmitError value) submitError,
    required TResult Function(ResetPasswordChooseMethod value) chooseMethod,
    required TResult Function(ResetPasswordConfirmReset value) confirmReset,
    required TResult Function(ResetPasswordSuccess value) success,
  }) {
    return chooseMethod(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ResetPasswordInit value)? init,
    TResult Function(ResetPasswordLoading value)? loading,
    TResult Function(ResetPasswordTypingError value)? typingError,
    TResult Function(ResetPasswordSubmitError value)? submitError,
    TResult Function(ResetPasswordChooseMethod value)? chooseMethod,
    TResult Function(ResetPasswordConfirmReset value)? confirmReset,
    TResult Function(ResetPasswordSuccess value)? success,
    required TResult orElse(),
  }) {
    if (chooseMethod != null) {
      return chooseMethod(this);
    }
    return orElse();
  }
}

abstract class ResetPasswordChooseMethod extends ResetPasswordState {
  const factory ResetPasswordChooseMethod(ResetPasswordModel model) =
      _$ResetPasswordChooseMethod;
  const ResetPasswordChooseMethod._() : super._();

  @override
  ResetPasswordModel get model => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  $ResetPasswordChooseMethodCopyWith<ResetPasswordChooseMethod> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ResetPasswordConfirmResetCopyWith<$Res>
    implements $ResetPasswordStateCopyWith<$Res> {
  factory $ResetPasswordConfirmResetCopyWith(ResetPasswordConfirmReset value,
          $Res Function(ResetPasswordConfirmReset) then) =
      _$ResetPasswordConfirmResetCopyWithImpl<$Res>;
  @override
  $Res call({ResetPasswordModel model});

  @override
  $ResetPasswordModelCopyWith<$Res> get model;
}

/// @nodoc
class _$ResetPasswordConfirmResetCopyWithImpl<$Res>
    extends _$ResetPasswordStateCopyWithImpl<$Res>
    implements $ResetPasswordConfirmResetCopyWith<$Res> {
  _$ResetPasswordConfirmResetCopyWithImpl(ResetPasswordConfirmReset _value,
      $Res Function(ResetPasswordConfirmReset) _then)
      : super(_value, (v) => _then(v as ResetPasswordConfirmReset));

  @override
  ResetPasswordConfirmReset get _value =>
      super._value as ResetPasswordConfirmReset;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(ResetPasswordConfirmReset(
      model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as ResetPasswordModel,
    ));
  }
}

/// @nodoc

class _$ResetPasswordConfirmReset extends ResetPasswordConfirmReset {
  const _$ResetPasswordConfirmReset(this.model) : super._();

  @override
  final ResetPasswordModel model;

  @override
  String toString() {
    return 'ResetPasswordState.confirmReset(model: $model)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ResetPasswordConfirmReset &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(model);

  @JsonKey(ignore: true)
  @override
  $ResetPasswordConfirmResetCopyWith<ResetPasswordConfirmReset> get copyWith =>
      _$ResetPasswordConfirmResetCopyWithImpl<ResetPasswordConfirmReset>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ResetPasswordModel model) init,
    required TResult Function(ResetPasswordModel model) loading,
    required TResult Function(ResetPasswordModel model) typingError,
    required TResult Function(ResetPasswordModel model) submitError,
    required TResult Function(ResetPasswordModel model) chooseMethod,
    required TResult Function(ResetPasswordModel model) confirmReset,
    required TResult Function(ResetPasswordModel model) success,
  }) {
    return confirmReset(model);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ResetPasswordModel model)? init,
    TResult Function(ResetPasswordModel model)? loading,
    TResult Function(ResetPasswordModel model)? typingError,
    TResult Function(ResetPasswordModel model)? submitError,
    TResult Function(ResetPasswordModel model)? chooseMethod,
    TResult Function(ResetPasswordModel model)? confirmReset,
    TResult Function(ResetPasswordModel model)? success,
    required TResult orElse(),
  }) {
    if (confirmReset != null) {
      return confirmReset(model);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ResetPasswordInit value) init,
    required TResult Function(ResetPasswordLoading value) loading,
    required TResult Function(ResetPasswordTypingError value) typingError,
    required TResult Function(ResetPasswordSubmitError value) submitError,
    required TResult Function(ResetPasswordChooseMethod value) chooseMethod,
    required TResult Function(ResetPasswordConfirmReset value) confirmReset,
    required TResult Function(ResetPasswordSuccess value) success,
  }) {
    return confirmReset(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ResetPasswordInit value)? init,
    TResult Function(ResetPasswordLoading value)? loading,
    TResult Function(ResetPasswordTypingError value)? typingError,
    TResult Function(ResetPasswordSubmitError value)? submitError,
    TResult Function(ResetPasswordChooseMethod value)? chooseMethod,
    TResult Function(ResetPasswordConfirmReset value)? confirmReset,
    TResult Function(ResetPasswordSuccess value)? success,
    required TResult orElse(),
  }) {
    if (confirmReset != null) {
      return confirmReset(this);
    }
    return orElse();
  }
}

abstract class ResetPasswordConfirmReset extends ResetPasswordState {
  const factory ResetPasswordConfirmReset(ResetPasswordModel model) =
      _$ResetPasswordConfirmReset;
  const ResetPasswordConfirmReset._() : super._();

  @override
  ResetPasswordModel get model => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  $ResetPasswordConfirmResetCopyWith<ResetPasswordConfirmReset> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ResetPasswordSuccessCopyWith<$Res>
    implements $ResetPasswordStateCopyWith<$Res> {
  factory $ResetPasswordSuccessCopyWith(ResetPasswordSuccess value,
          $Res Function(ResetPasswordSuccess) then) =
      _$ResetPasswordSuccessCopyWithImpl<$Res>;
  @override
  $Res call({ResetPasswordModel model});

  @override
  $ResetPasswordModelCopyWith<$Res> get model;
}

/// @nodoc
class _$ResetPasswordSuccessCopyWithImpl<$Res>
    extends _$ResetPasswordStateCopyWithImpl<$Res>
    implements $ResetPasswordSuccessCopyWith<$Res> {
  _$ResetPasswordSuccessCopyWithImpl(
      ResetPasswordSuccess _value, $Res Function(ResetPasswordSuccess) _then)
      : super(_value, (v) => _then(v as ResetPasswordSuccess));

  @override
  ResetPasswordSuccess get _value => super._value as ResetPasswordSuccess;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(ResetPasswordSuccess(
      model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as ResetPasswordModel,
    ));
  }
}

/// @nodoc

class _$ResetPasswordSuccess extends ResetPasswordSuccess {
  const _$ResetPasswordSuccess(this.model) : super._();

  @override
  final ResetPasswordModel model;

  @override
  String toString() {
    return 'ResetPasswordState.success(model: $model)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ResetPasswordSuccess &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(model);

  @JsonKey(ignore: true)
  @override
  $ResetPasswordSuccessCopyWith<ResetPasswordSuccess> get copyWith =>
      _$ResetPasswordSuccessCopyWithImpl<ResetPasswordSuccess>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ResetPasswordModel model) init,
    required TResult Function(ResetPasswordModel model) loading,
    required TResult Function(ResetPasswordModel model) typingError,
    required TResult Function(ResetPasswordModel model) submitError,
    required TResult Function(ResetPasswordModel model) chooseMethod,
    required TResult Function(ResetPasswordModel model) confirmReset,
    required TResult Function(ResetPasswordModel model) success,
  }) {
    return success(model);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ResetPasswordModel model)? init,
    TResult Function(ResetPasswordModel model)? loading,
    TResult Function(ResetPasswordModel model)? typingError,
    TResult Function(ResetPasswordModel model)? submitError,
    TResult Function(ResetPasswordModel model)? chooseMethod,
    TResult Function(ResetPasswordModel model)? confirmReset,
    TResult Function(ResetPasswordModel model)? success,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(model);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ResetPasswordInit value) init,
    required TResult Function(ResetPasswordLoading value) loading,
    required TResult Function(ResetPasswordTypingError value) typingError,
    required TResult Function(ResetPasswordSubmitError value) submitError,
    required TResult Function(ResetPasswordChooseMethod value) chooseMethod,
    required TResult Function(ResetPasswordConfirmReset value) confirmReset,
    required TResult Function(ResetPasswordSuccess value) success,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ResetPasswordInit value)? init,
    TResult Function(ResetPasswordLoading value)? loading,
    TResult Function(ResetPasswordTypingError value)? typingError,
    TResult Function(ResetPasswordSubmitError value)? submitError,
    TResult Function(ResetPasswordChooseMethod value)? chooseMethod,
    TResult Function(ResetPasswordConfirmReset value)? confirmReset,
    TResult Function(ResetPasswordSuccess value)? success,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class ResetPasswordSuccess extends ResetPasswordState {
  const factory ResetPasswordSuccess(ResetPasswordModel model) =
      _$ResetPasswordSuccess;
  const ResetPasswordSuccess._() : super._();

  @override
  ResetPasswordModel get model => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  $ResetPasswordSuccessCopyWith<ResetPasswordSuccess> get copyWith =>
      throw _privateConstructorUsedError;
}
