import 'package:flutter/material.dart';
import 'package:flutter_scaffold_bloc/modules/auth/reset_password/bloc/reset_password_bloc.dart';
import 'package:flutter_scaffold_bloc/modules/auth/reset_password/bloc/reset_password_event.dart';
import 'package:flutter_scaffold_bloc/modules/auth/reset_password/bloc/reset_password_state.dart';
import 'package:flutter_scaffold_bloc/modules/auth/reset_password/ui/reset_password_step_1.dart';
import 'package:flutter_scaffold_bloc/modules/auth/reset_password/ui/reset_password_step_2.dart';
import 'package:flutter_scaffold_bloc/modules/auth/reset_password/ui/reset_password_step_3.dart';
import 'package:flutter_scaffold_bloc/modules/auth/reset_password/ui/reset_password_step_4.dart';
import 'package:flutter_scaffold_bloc/widgets/misc/bloc_router.dart';
import 'package:flutter_scaffold_bloc/widgets/misc/custom_bloc.dart';

class ResetPasswordView extends CustomBlocWidget<ResetPasswordBloc, ResetPasswordEvent, ResetPasswordState> {
  @override
  final provider = (_) => ResetPasswordBloc();

  @override
  final List<BlocRoute> routes = [
    BlocRoute(
        name: BlocRoute.Initial,
        view: ResetPasswordStep1View(),
        pushOn: [ResetPasswordInit],
        pushMethod: BlocRoutingMethod.PushAndRemoveUntil),
    BlocRoute(
        name: '/step_2',
        view: ResetPasswordStep2View(),
        pushOn: [ResetPasswordChooseMethod],
        pushMethod: BlocRoutingMethod.PushReplacement),
    BlocRoute(
        name: '/step_3',
        view: ResetPasswordStep3View(),
        pushOn: [ResetPasswordConfirmReset],
        pushMethod: BlocRoutingMethod.PushReplacement),
    BlocRoute(
        name: '/step_4',
        view: ResetPasswordStep4View(),
        pushOn: [ResetPasswordSuccess],
        pushMethod: BlocRoutingMethod.PushReplacement),
  ];

  @override
  Widget builder(BuildContext context) => Stack(
        alignment: Alignment.center,
        children: [route.view, if (state is ResetPasswordLoading) CircularProgressIndicator()],
      );
}
