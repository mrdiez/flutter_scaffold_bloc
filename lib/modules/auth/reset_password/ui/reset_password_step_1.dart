import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_scaffold_bloc/constants/theme/sizes.dart';
import 'package:flutter_scaffold_bloc/modules/auth/auth.dart';
import 'package:flutter_scaffold_bloc/modules/auth/login/bloc/login_bloc.dart';
import 'package:flutter_scaffold_bloc/modules/auth/reset_password/bloc/reset_password_bloc.dart';
import 'package:flutter_scaffold_bloc/modules/auth/reset_password/bloc/reset_password_event.dart';
import 'package:flutter_scaffold_bloc/modules/auth/reset_password/bloc/reset_password_state.dart';
import 'package:flutter_scaffold_bloc/widgets/buttons/secondary.dart';
import 'package:flutter_scaffold_bloc/widgets/buttons/submit.dart';
import 'package:flutter_scaffold_bloc/widgets/inputs/custom_text.dart';
import 'package:flutter_scaffold_bloc/widgets/misc/custom_bloc.dart';
import 'package:flutter_scaffold_bloc/widgets/texts/simple.dart';
import 'package:flutter_scaffold_bloc/widgets/texts/title.dart';

class ResetPasswordStep1View extends CustomBlocWidget<ResetPasswordBloc, ResetPasswordEvent, ResetPasswordState> {
  final _formKey = GlobalKey<FormBuilderState>();
  final _loginKey = GlobalKey<CustomTextInputState>();

  @override
  void onReady() {
    if (!(_loginKey.currentState?.focusNode.hasFocus ?? true)) _loginKey.currentState!.focusNode.requestFocus();
  }

  @override
  void onBuild() {
    if (state is ResetPasswordSubmitError && state.model.isLoginError) _loginKey.currentState?.shake();
  }

  void Function()? get _onSubmit =>
      !(state is ResetPasswordLoading) ? () => bloc.add(ResetPasswordEvent.submitEmail(state.model)) : null;

  @override
  Widget builder(BuildContext context) {
    return FormBuilder(
      key: _formKey,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TitleText(translation.forgotPassword, hero: true),
          Padding(
            padding: ThemeSize.paddingOnly(top: S),
            child: SimpleText(translation.forWhichAccount),
          ),
          CustomTextInput(
            label: LoginBloc.loginLabel,
            key: _loginKey,
            keyboard: TextInputType.emailAddress,
            error: state.model.loginError,
            onChanged: (value) => bloc.add(ResetPasswordEvent.typingEmail(state.model.copyWith(login: value))),
            onSubmitted: (_) => (_onSubmit ?? () {})(),
          ),
          Padding(
            padding: ThemeSize.padding(M),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SubmitButton(
                  action: _onSubmit,
                  hero: true,
                ),
                SecondaryButton(
                  text: translation.cancel,
                  hero: true,
                  action: () {
                    AuthModule.navigatorKey.currentState!.pop();
                  },
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
