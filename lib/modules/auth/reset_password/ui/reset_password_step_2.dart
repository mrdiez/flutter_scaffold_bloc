import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_scaffold_bloc/constants/theme/sizes.dart';
import 'package:flutter_scaffold_bloc/modules/auth/auth.dart';
import 'package:flutter_scaffold_bloc/modules/auth/reset_password/bloc/reset_password_bloc.dart';
import 'package:flutter_scaffold_bloc/modules/auth/reset_password/bloc/reset_password_event.dart';
import 'package:flutter_scaffold_bloc/modules/auth/reset_password/bloc/reset_password_state.dart';
import 'package:flutter_scaffold_bloc/widgets/buttons/secondary.dart';
import 'package:flutter_scaffold_bloc/widgets/buttons/submit.dart';
import 'package:flutter_scaffold_bloc/widgets/inputs/custom_radio.dart';
import 'package:flutter_scaffold_bloc/widgets/misc/custom_bloc.dart';
import 'package:flutter_scaffold_bloc/widgets/texts/simple.dart';
import 'package:flutter_scaffold_bloc/widgets/texts/title.dart';

class ResetPasswordStep2View extends CustomBlocWidget<ResetPasswordBloc, ResetPasswordEvent, ResetPasswordState> {
  final _formKey = GlobalKey<FormBuilderState>();
  final GlobalKey<CustomRadioInputState> _radioKey = GlobalKey<CustomRadioInputState>();

  @override
  void onBuild() {
    if (state is ResetPasswordSubmitError && state.model.isMethodError) _radioKey.currentState?.shakeIfInvalid();
  }

  void Function()? get _onSubmit =>
      !(state is ResetPasswordLoading) ? () => bloc.add(ResetPasswordEvent.submitMethod(state.model)) : null;

  @override
  Widget builder(BuildContext context) {
    return FormBuilder(
      key: _formKey,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TitleText(translation.forgotPassword, hero: true),
          Padding(
            padding: ThemeSize.paddingOnly(top: S),
            child: SimpleText(translation.chooseAMethod),
          ),
          CustomRadioInput<ResetPasswordMethod>(
            key: _radioKey,
            value: state.model.method,
            valuesAndLabels: ResetPasswordBloc.MethodLabels,
            error: state.model.methodError,
            onChanged: (_value) => bloc.add(ResetPasswordEvent.radioSelect(state.model.copyWith(method: _value))),
          ),
          Padding(
            padding: ThemeSize.padding(M),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SubmitButton(
                  action: _onSubmit,
                  hero: true,
                ),
                SecondaryButton(
                  text: translation.cancel,
                  hero: true,
                  action: () {
                    AuthModule.navigatorKey.currentState!.pop();
                  },
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
