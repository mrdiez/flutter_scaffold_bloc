import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_scaffold_bloc/constants/theme/sizes.dart';
import 'package:flutter_scaffold_bloc/modules/auth/auth.dart';
import 'package:flutter_scaffold_bloc/modules/auth/reset_password/bloc/reset_password_bloc.dart';
import 'package:flutter_scaffold_bloc/modules/auth/reset_password/bloc/reset_password_event.dart';
import 'package:flutter_scaffold_bloc/modules/auth/reset_password/bloc/reset_password_state.dart';
import 'package:flutter_scaffold_bloc/widgets/buttons/secondary.dart';
import 'package:flutter_scaffold_bloc/widgets/buttons/submit.dart';
import 'package:flutter_scaffold_bloc/widgets/inputs/custom_text.dart';
import 'package:flutter_scaffold_bloc/widgets/misc/custom_bloc.dart';
import 'package:flutter_scaffold_bloc/widgets/texts/simple.dart';
import 'package:flutter_scaffold_bloc/widgets/texts/title.dart';

class ResetPasswordStep3View extends CustomBlocWidget<ResetPasswordBloc, ResetPasswordEvent, ResetPasswordState> {
  final _formKey = GlobalKey<FormBuilderState>();
  final _codeKey = GlobalKey<CustomTextInputState>();

  @override
  void onReady() {
    if (!(_codeKey.currentState?.focusNode.hasFocus ?? true)) _codeKey.currentState!.focusNode.requestFocus();
  }

  @override
  void onBuild() {
    if (state is ResetPasswordSubmitError && state.model.isCodeError) _codeKey.currentState?.shake();
  }

  void Function()? get _onSubmit =>
      !(state is ResetPasswordLoading) ? () => bloc.add(ResetPasswordEvent.submitCode(state.model)) : null;

  @override
  Widget builder(BuildContext context) {
    return FormBuilder(
      key: _formKey,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TitleText(translation.forgotPassword, hero: true),
          Padding(
            padding: ThemeSize.paddingOnly(top: S),
            child: SimpleText(translation.confirmYourIdentity('email')),
          ),
          CustomTextInput(
            label: translation.code,
            key: _codeKey,
            value: state.model.code,
            error: state.model.codeError,
            keyboard: TextInputType.number,
            onChanged: (_value) => bloc.add(ResetPasswordEvent.typingCode(state.model.copyWith(code: _value))),
            onSubmitted: (_) => (_onSubmit ?? () {})(),
          ),
          Padding(
            padding: ThemeSize.padding(M),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SubmitButton(
                  action: _onSubmit,
                  hero: true,
                ),
                SecondaryButton(
                  text: translation.cancel,
                  hero: true,
                  action: () {
                    AuthModule.navigatorKey.currentState!.pop();
                  },
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
