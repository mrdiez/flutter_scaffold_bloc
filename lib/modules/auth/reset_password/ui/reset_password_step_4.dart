import 'package:flutter/material.dart';
import 'package:flutter_scaffold_bloc/constants/theme/sizes.dart';
import 'package:flutter_scaffold_bloc/modules/auth/auth.dart';
import 'package:flutter_scaffold_bloc/modules/auth/reset_password/bloc/reset_password_bloc.dart';
import 'package:flutter_scaffold_bloc/modules/auth/reset_password/bloc/reset_password_event.dart';
import 'package:flutter_scaffold_bloc/modules/auth/reset_password/bloc/reset_password_state.dart';
import 'package:flutter_scaffold_bloc/widgets/buttons/submit.dart';
import 'package:flutter_scaffold_bloc/widgets/misc/custom_bloc.dart';
import 'package:flutter_scaffold_bloc/widgets/texts/simple.dart';
import 'package:flutter_scaffold_bloc/widgets/texts/title.dart';

class ResetPasswordStep4View extends CustomBlocWidget<ResetPasswordBloc, ResetPasswordEvent, ResetPasswordState> {
  @override
  Widget builder(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        TitleText(translation.forgotPassword, hero: true),
        Padding(
          padding: ThemeSize.paddingOnly(top: S),
          child: SimpleText(translation.yourNewPassword('1234')),
        ),
        Padding(
          padding: ThemeSize.padding(M),
          child: SubmitButton(
            text: translation.ok,
            tag: SubmitButton.DefaultText,
            action: () {
              AuthModule.navigatorKey.currentState!.pop();
            },
          ),
        ),
      ],
    );
  }
}
