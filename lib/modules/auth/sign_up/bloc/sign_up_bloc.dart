import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_scaffold_bloc/modules/auth/data/auth_repository.dart';
import 'package:flutter_scaffold_bloc/modules/auth/login/bloc/login_bloc.dart';
import 'package:flutter_scaffold_bloc/modules/auth/sign_up/bloc/sign_up_event.dart';
import 'package:flutter_scaffold_bloc/modules/auth/sign_up/bloc/sign_up_model.dart';
import 'package:flutter_scaffold_bloc/modules/auth/sign_up/bloc/sign_up_state.dart';
import 'package:flutter_scaffold_bloc/services/task_manager.dart';
import 'package:flutter_scaffold_bloc/utils/string.dart';
import 'package:flutter_scaffold_bloc/widgets/misc/custom_bloc.dart';

/// A [Bloc] class that emit states when receiving events
/// Check this to understand what is a [Bloc] and why we'll use it:
/// https://bloclibrary.dev/#/coreconcepts?id=bloc
class SignUpBloc extends CustomBloc<SignUpEvent, SignUpState> {
  SignUpBloc() : super(SignUpState.init());

  final AuthRepository _repository = const AuthRepository();

  SignUpModel _validate(SignUpModel model, [bool isSubmit = false]) => model.copyWith(
        usernameError: LoginBloc.usernameValidator(model.username, isSubmit),
        emailError: LoginBloc.emailValidator(model.email, isSubmit),
        passwordError: LoginBloc.passwordValidator(model.password, isSubmit),
        confirmPasswordError: confirmPasswordValidator(model.password, model.confirmPassword, isSubmit),
      );

  String? confirmPasswordValidator(String? password, String? confirmPassword, [bool isSubmit = false]) {
    var result = LoginBloc.passwordValidator(confirmPassword, isSubmit);
    if (StringTool.notNull(result)) return result;
    if (StringTool.notNull(confirmPassword) && password != confirmPassword) {
      return translation.passwordsMustBeIdentical;
    }
  }

  Stream<SignUpState> _onTyping(SignUpModel model) async* {
    var result = _validate(model);
    yield result.isError ? SignUpState.typingError(result) : SignUpState.init(result);
  }

  Stream<SignUpState> _onSubmit(SignUpModel model) async* {
    var result = _validate(model, true);
    try {
      if (result.isError) {
        yield SignUpState.submitError(result);
      } else {
        yield SignUpState.loading(result);
        taskManager.add(TaskManagerEvent.stopLoading(SignUpSubmit));
        await _repository.signUp(result);
        yield SignUpState.success();
        taskManager.add(TaskManagerEvent.stopLoading(SignUpSubmit));
      }
    } catch (error) {
      if (model.email != null && error.toString().contains(model.email!)) {
        yield SignUpState.submitError(result.copyWith(emailError: error.toString().removeException));
      } else {
        yield SignUpState.submitError(result.copyWith(usernameError: error.toString().removeException));
      }
      taskManager.add(TaskManagerEvent.stopLoading(SignUpSubmit));
    }
  }

  @override
  Stream<SignUpState> mapEventToState(SignUpEvent event) => event.maybeWhen(
        submit: _onSubmit,
        typing: _onTyping,
        cancelClick: () async* {
          yield SignUpState.init();
        },
        orElse: () async* {},
      );
}
