import 'package:flutter_scaffold_bloc/modules/auth/sign_up/bloc/sign_up_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'sign_up_event.freezed.dart';

/// An immutable generated class that define BLOC events
/// To generate code execute: flutter pub run build_runner build
/// For more info check the README # How to generate code (with freezed)
@freezed
class SignUpEvent with _$SignUpEvent {
  const factory SignUpEvent.submit(SignUpModel model) = SignUpSubmit;
  const factory SignUpEvent.typing(SignUpModel model) = SignUpTyping;
  const factory SignUpEvent.cancelClick() = SignUpCancel;
  const factory SignUpEvent.end() = SignUpEnd;
}
