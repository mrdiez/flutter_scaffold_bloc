// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'sign_up_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$SignUpEventTearOff {
  const _$SignUpEventTearOff();

  SignUpSubmit submit(SignUpModel model) {
    return SignUpSubmit(
      model,
    );
  }

  SignUpTyping typing(SignUpModel model) {
    return SignUpTyping(
      model,
    );
  }

  SignUpCancel cancelClick() {
    return const SignUpCancel();
  }

  SignUpEnd end() {
    return const SignUpEnd();
  }
}

/// @nodoc
const $SignUpEvent = _$SignUpEventTearOff();

/// @nodoc
mixin _$SignUpEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(SignUpModel model) submit,
    required TResult Function(SignUpModel model) typing,
    required TResult Function() cancelClick,
    required TResult Function() end,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(SignUpModel model)? submit,
    TResult Function(SignUpModel model)? typing,
    TResult Function()? cancelClick,
    TResult Function()? end,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SignUpSubmit value) submit,
    required TResult Function(SignUpTyping value) typing,
    required TResult Function(SignUpCancel value) cancelClick,
    required TResult Function(SignUpEnd value) end,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SignUpSubmit value)? submit,
    TResult Function(SignUpTyping value)? typing,
    TResult Function(SignUpCancel value)? cancelClick,
    TResult Function(SignUpEnd value)? end,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SignUpEventCopyWith<$Res> {
  factory $SignUpEventCopyWith(
          SignUpEvent value, $Res Function(SignUpEvent) then) =
      _$SignUpEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$SignUpEventCopyWithImpl<$Res> implements $SignUpEventCopyWith<$Res> {
  _$SignUpEventCopyWithImpl(this._value, this._then);

  final SignUpEvent _value;
  // ignore: unused_field
  final $Res Function(SignUpEvent) _then;
}

/// @nodoc
abstract class $SignUpSubmitCopyWith<$Res> {
  factory $SignUpSubmitCopyWith(
          SignUpSubmit value, $Res Function(SignUpSubmit) then) =
      _$SignUpSubmitCopyWithImpl<$Res>;
  $Res call({SignUpModel model});

  $SignUpModelCopyWith<$Res> get model;
}

/// @nodoc
class _$SignUpSubmitCopyWithImpl<$Res> extends _$SignUpEventCopyWithImpl<$Res>
    implements $SignUpSubmitCopyWith<$Res> {
  _$SignUpSubmitCopyWithImpl(
      SignUpSubmit _value, $Res Function(SignUpSubmit) _then)
      : super(_value, (v) => _then(v as SignUpSubmit));

  @override
  SignUpSubmit get _value => super._value as SignUpSubmit;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(SignUpSubmit(
      model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as SignUpModel,
    ));
  }

  @override
  $SignUpModelCopyWith<$Res> get model {
    return $SignUpModelCopyWith<$Res>(_value.model, (value) {
      return _then(_value.copyWith(model: value));
    });
  }
}

/// @nodoc

class _$SignUpSubmit implements SignUpSubmit {
  const _$SignUpSubmit(this.model);

  @override
  final SignUpModel model;

  @override
  String toString() {
    return 'SignUpEvent.submit(model: $model)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is SignUpSubmit &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(model);

  @JsonKey(ignore: true)
  @override
  $SignUpSubmitCopyWith<SignUpSubmit> get copyWith =>
      _$SignUpSubmitCopyWithImpl<SignUpSubmit>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(SignUpModel model) submit,
    required TResult Function(SignUpModel model) typing,
    required TResult Function() cancelClick,
    required TResult Function() end,
  }) {
    return submit(model);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(SignUpModel model)? submit,
    TResult Function(SignUpModel model)? typing,
    TResult Function()? cancelClick,
    TResult Function()? end,
    required TResult orElse(),
  }) {
    if (submit != null) {
      return submit(model);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SignUpSubmit value) submit,
    required TResult Function(SignUpTyping value) typing,
    required TResult Function(SignUpCancel value) cancelClick,
    required TResult Function(SignUpEnd value) end,
  }) {
    return submit(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SignUpSubmit value)? submit,
    TResult Function(SignUpTyping value)? typing,
    TResult Function(SignUpCancel value)? cancelClick,
    TResult Function(SignUpEnd value)? end,
    required TResult orElse(),
  }) {
    if (submit != null) {
      return submit(this);
    }
    return orElse();
  }
}

abstract class SignUpSubmit implements SignUpEvent {
  const factory SignUpSubmit(SignUpModel model) = _$SignUpSubmit;

  SignUpModel get model => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $SignUpSubmitCopyWith<SignUpSubmit> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SignUpTypingCopyWith<$Res> {
  factory $SignUpTypingCopyWith(
          SignUpTyping value, $Res Function(SignUpTyping) then) =
      _$SignUpTypingCopyWithImpl<$Res>;
  $Res call({SignUpModel model});

  $SignUpModelCopyWith<$Res> get model;
}

/// @nodoc
class _$SignUpTypingCopyWithImpl<$Res> extends _$SignUpEventCopyWithImpl<$Res>
    implements $SignUpTypingCopyWith<$Res> {
  _$SignUpTypingCopyWithImpl(
      SignUpTyping _value, $Res Function(SignUpTyping) _then)
      : super(_value, (v) => _then(v as SignUpTyping));

  @override
  SignUpTyping get _value => super._value as SignUpTyping;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(SignUpTyping(
      model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as SignUpModel,
    ));
  }

  @override
  $SignUpModelCopyWith<$Res> get model {
    return $SignUpModelCopyWith<$Res>(_value.model, (value) {
      return _then(_value.copyWith(model: value));
    });
  }
}

/// @nodoc

class _$SignUpTyping implements SignUpTyping {
  const _$SignUpTyping(this.model);

  @override
  final SignUpModel model;

  @override
  String toString() {
    return 'SignUpEvent.typing(model: $model)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is SignUpTyping &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(model);

  @JsonKey(ignore: true)
  @override
  $SignUpTypingCopyWith<SignUpTyping> get copyWith =>
      _$SignUpTypingCopyWithImpl<SignUpTyping>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(SignUpModel model) submit,
    required TResult Function(SignUpModel model) typing,
    required TResult Function() cancelClick,
    required TResult Function() end,
  }) {
    return typing(model);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(SignUpModel model)? submit,
    TResult Function(SignUpModel model)? typing,
    TResult Function()? cancelClick,
    TResult Function()? end,
    required TResult orElse(),
  }) {
    if (typing != null) {
      return typing(model);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SignUpSubmit value) submit,
    required TResult Function(SignUpTyping value) typing,
    required TResult Function(SignUpCancel value) cancelClick,
    required TResult Function(SignUpEnd value) end,
  }) {
    return typing(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SignUpSubmit value)? submit,
    TResult Function(SignUpTyping value)? typing,
    TResult Function(SignUpCancel value)? cancelClick,
    TResult Function(SignUpEnd value)? end,
    required TResult orElse(),
  }) {
    if (typing != null) {
      return typing(this);
    }
    return orElse();
  }
}

abstract class SignUpTyping implements SignUpEvent {
  const factory SignUpTyping(SignUpModel model) = _$SignUpTyping;

  SignUpModel get model => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $SignUpTypingCopyWith<SignUpTyping> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SignUpCancelCopyWith<$Res> {
  factory $SignUpCancelCopyWith(
          SignUpCancel value, $Res Function(SignUpCancel) then) =
      _$SignUpCancelCopyWithImpl<$Res>;
}

/// @nodoc
class _$SignUpCancelCopyWithImpl<$Res> extends _$SignUpEventCopyWithImpl<$Res>
    implements $SignUpCancelCopyWith<$Res> {
  _$SignUpCancelCopyWithImpl(
      SignUpCancel _value, $Res Function(SignUpCancel) _then)
      : super(_value, (v) => _then(v as SignUpCancel));

  @override
  SignUpCancel get _value => super._value as SignUpCancel;
}

/// @nodoc

class _$SignUpCancel implements SignUpCancel {
  const _$SignUpCancel();

  @override
  String toString() {
    return 'SignUpEvent.cancelClick()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is SignUpCancel);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(SignUpModel model) submit,
    required TResult Function(SignUpModel model) typing,
    required TResult Function() cancelClick,
    required TResult Function() end,
  }) {
    return cancelClick();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(SignUpModel model)? submit,
    TResult Function(SignUpModel model)? typing,
    TResult Function()? cancelClick,
    TResult Function()? end,
    required TResult orElse(),
  }) {
    if (cancelClick != null) {
      return cancelClick();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SignUpSubmit value) submit,
    required TResult Function(SignUpTyping value) typing,
    required TResult Function(SignUpCancel value) cancelClick,
    required TResult Function(SignUpEnd value) end,
  }) {
    return cancelClick(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SignUpSubmit value)? submit,
    TResult Function(SignUpTyping value)? typing,
    TResult Function(SignUpCancel value)? cancelClick,
    TResult Function(SignUpEnd value)? end,
    required TResult orElse(),
  }) {
    if (cancelClick != null) {
      return cancelClick(this);
    }
    return orElse();
  }
}

abstract class SignUpCancel implements SignUpEvent {
  const factory SignUpCancel() = _$SignUpCancel;
}

/// @nodoc
abstract class $SignUpEndCopyWith<$Res> {
  factory $SignUpEndCopyWith(SignUpEnd value, $Res Function(SignUpEnd) then) =
      _$SignUpEndCopyWithImpl<$Res>;
}

/// @nodoc
class _$SignUpEndCopyWithImpl<$Res> extends _$SignUpEventCopyWithImpl<$Res>
    implements $SignUpEndCopyWith<$Res> {
  _$SignUpEndCopyWithImpl(SignUpEnd _value, $Res Function(SignUpEnd) _then)
      : super(_value, (v) => _then(v as SignUpEnd));

  @override
  SignUpEnd get _value => super._value as SignUpEnd;
}

/// @nodoc

class _$SignUpEnd implements SignUpEnd {
  const _$SignUpEnd();

  @override
  String toString() {
    return 'SignUpEvent.end()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is SignUpEnd);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(SignUpModel model) submit,
    required TResult Function(SignUpModel model) typing,
    required TResult Function() cancelClick,
    required TResult Function() end,
  }) {
    return end();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(SignUpModel model)? submit,
    TResult Function(SignUpModel model)? typing,
    TResult Function()? cancelClick,
    TResult Function()? end,
    required TResult orElse(),
  }) {
    if (end != null) {
      return end();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SignUpSubmit value) submit,
    required TResult Function(SignUpTyping value) typing,
    required TResult Function(SignUpCancel value) cancelClick,
    required TResult Function(SignUpEnd value) end,
  }) {
    return end(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SignUpSubmit value)? submit,
    TResult Function(SignUpTyping value)? typing,
    TResult Function(SignUpCancel value)? cancelClick,
    TResult Function(SignUpEnd value)? end,
    required TResult orElse(),
  }) {
    if (end != null) {
      return end(this);
    }
    return orElse();
  }
}

abstract class SignUpEnd implements SignUpEvent {
  const factory SignUpEnd() = _$SignUpEnd;
}
