
import 'package:flutter_scaffold_bloc/utils/string.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'sign_up_model.freezed.dart';

/// An immutable generated class model
/// To generate code execute: flutter pub run build_runner build
/// For more info check the README # How to generate code (with freezed)
@freezed
class SignUpModel with _$SignUpModel {
  const SignUpModel._();
  const factory SignUpModel({
    String? username,
    String? email,
    String? password,
    String? confirmPassword,
    String? usernameError,
    String? emailError,
    String? passwordError,
    String? confirmPasswordError,
  }) = _SignUpModel;

  bool get isUsernameError => StringTool.notNull(usernameError);
  bool get isEmailError => StringTool.notNull(emailError);
  bool get isPasswordError => StringTool.notNull(passwordError);
  bool get isConfirmPasswordError => StringTool.notNull(confirmPasswordError);
  bool get isError => isUsernameError || isEmailError || isPasswordError || isConfirmPasswordError;
}
