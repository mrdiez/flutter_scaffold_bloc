// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'sign_up_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$SignUpModelTearOff {
  const _$SignUpModelTearOff();

  _SignUpModel call(
      {String? username,
      String? email,
      String? password,
      String? confirmPassword,
      String? usernameError,
      String? emailError,
      String? passwordError,
      String? confirmPasswordError}) {
    return _SignUpModel(
      username: username,
      email: email,
      password: password,
      confirmPassword: confirmPassword,
      usernameError: usernameError,
      emailError: emailError,
      passwordError: passwordError,
      confirmPasswordError: confirmPasswordError,
    );
  }
}

/// @nodoc
const $SignUpModel = _$SignUpModelTearOff();

/// @nodoc
mixin _$SignUpModel {
  String? get username => throw _privateConstructorUsedError;
  String? get email => throw _privateConstructorUsedError;
  String? get password => throw _privateConstructorUsedError;
  String? get confirmPassword => throw _privateConstructorUsedError;
  String? get usernameError => throw _privateConstructorUsedError;
  String? get emailError => throw _privateConstructorUsedError;
  String? get passwordError => throw _privateConstructorUsedError;
  String? get confirmPasswordError => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SignUpModelCopyWith<SignUpModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SignUpModelCopyWith<$Res> {
  factory $SignUpModelCopyWith(
          SignUpModel value, $Res Function(SignUpModel) then) =
      _$SignUpModelCopyWithImpl<$Res>;
  $Res call(
      {String? username,
      String? email,
      String? password,
      String? confirmPassword,
      String? usernameError,
      String? emailError,
      String? passwordError,
      String? confirmPasswordError});
}

/// @nodoc
class _$SignUpModelCopyWithImpl<$Res> implements $SignUpModelCopyWith<$Res> {
  _$SignUpModelCopyWithImpl(this._value, this._then);

  final SignUpModel _value;
  // ignore: unused_field
  final $Res Function(SignUpModel) _then;

  @override
  $Res call({
    Object? username = freezed,
    Object? email = freezed,
    Object? password = freezed,
    Object? confirmPassword = freezed,
    Object? usernameError = freezed,
    Object? emailError = freezed,
    Object? passwordError = freezed,
    Object? confirmPasswordError = freezed,
  }) {
    return _then(_value.copyWith(
      username: username == freezed
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String?,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      password: password == freezed
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String?,
      confirmPassword: confirmPassword == freezed
          ? _value.confirmPassword
          : confirmPassword // ignore: cast_nullable_to_non_nullable
              as String?,
      usernameError: usernameError == freezed
          ? _value.usernameError
          : usernameError // ignore: cast_nullable_to_non_nullable
              as String?,
      emailError: emailError == freezed
          ? _value.emailError
          : emailError // ignore: cast_nullable_to_non_nullable
              as String?,
      passwordError: passwordError == freezed
          ? _value.passwordError
          : passwordError // ignore: cast_nullable_to_non_nullable
              as String?,
      confirmPasswordError: confirmPasswordError == freezed
          ? _value.confirmPasswordError
          : confirmPasswordError // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$SignUpModelCopyWith<$Res>
    implements $SignUpModelCopyWith<$Res> {
  factory _$SignUpModelCopyWith(
          _SignUpModel value, $Res Function(_SignUpModel) then) =
      __$SignUpModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String? username,
      String? email,
      String? password,
      String? confirmPassword,
      String? usernameError,
      String? emailError,
      String? passwordError,
      String? confirmPasswordError});
}

/// @nodoc
class __$SignUpModelCopyWithImpl<$Res> extends _$SignUpModelCopyWithImpl<$Res>
    implements _$SignUpModelCopyWith<$Res> {
  __$SignUpModelCopyWithImpl(
      _SignUpModel _value, $Res Function(_SignUpModel) _then)
      : super(_value, (v) => _then(v as _SignUpModel));

  @override
  _SignUpModel get _value => super._value as _SignUpModel;

  @override
  $Res call({
    Object? username = freezed,
    Object? email = freezed,
    Object? password = freezed,
    Object? confirmPassword = freezed,
    Object? usernameError = freezed,
    Object? emailError = freezed,
    Object? passwordError = freezed,
    Object? confirmPasswordError = freezed,
  }) {
    return _then(_SignUpModel(
      username: username == freezed
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String?,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      password: password == freezed
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String?,
      confirmPassword: confirmPassword == freezed
          ? _value.confirmPassword
          : confirmPassword // ignore: cast_nullable_to_non_nullable
              as String?,
      usernameError: usernameError == freezed
          ? _value.usernameError
          : usernameError // ignore: cast_nullable_to_non_nullable
              as String?,
      emailError: emailError == freezed
          ? _value.emailError
          : emailError // ignore: cast_nullable_to_non_nullable
              as String?,
      passwordError: passwordError == freezed
          ? _value.passwordError
          : passwordError // ignore: cast_nullable_to_non_nullable
              as String?,
      confirmPasswordError: confirmPasswordError == freezed
          ? _value.confirmPasswordError
          : confirmPasswordError // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$_SignUpModel extends _SignUpModel {
  const _$_SignUpModel(
      {this.username,
      this.email,
      this.password,
      this.confirmPassword,
      this.usernameError,
      this.emailError,
      this.passwordError,
      this.confirmPasswordError})
      : super._();

  @override
  final String? username;
  @override
  final String? email;
  @override
  final String? password;
  @override
  final String? confirmPassword;
  @override
  final String? usernameError;
  @override
  final String? emailError;
  @override
  final String? passwordError;
  @override
  final String? confirmPasswordError;

  @override
  String toString() {
    return 'SignUpModel(username: $username, email: $email, password: $password, confirmPassword: $confirmPassword, usernameError: $usernameError, emailError: $emailError, passwordError: $passwordError, confirmPasswordError: $confirmPasswordError)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SignUpModel &&
            (identical(other.username, username) ||
                const DeepCollectionEquality()
                    .equals(other.username, username)) &&
            (identical(other.email, email) ||
                const DeepCollectionEquality().equals(other.email, email)) &&
            (identical(other.password, password) ||
                const DeepCollectionEquality()
                    .equals(other.password, password)) &&
            (identical(other.confirmPassword, confirmPassword) ||
                const DeepCollectionEquality()
                    .equals(other.confirmPassword, confirmPassword)) &&
            (identical(other.usernameError, usernameError) ||
                const DeepCollectionEquality()
                    .equals(other.usernameError, usernameError)) &&
            (identical(other.emailError, emailError) ||
                const DeepCollectionEquality()
                    .equals(other.emailError, emailError)) &&
            (identical(other.passwordError, passwordError) ||
                const DeepCollectionEquality()
                    .equals(other.passwordError, passwordError)) &&
            (identical(other.confirmPasswordError, confirmPasswordError) ||
                const DeepCollectionEquality()
                    .equals(other.confirmPasswordError, confirmPasswordError)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(username) ^
      const DeepCollectionEquality().hash(email) ^
      const DeepCollectionEquality().hash(password) ^
      const DeepCollectionEquality().hash(confirmPassword) ^
      const DeepCollectionEquality().hash(usernameError) ^
      const DeepCollectionEquality().hash(emailError) ^
      const DeepCollectionEquality().hash(passwordError) ^
      const DeepCollectionEquality().hash(confirmPasswordError);

  @JsonKey(ignore: true)
  @override
  _$SignUpModelCopyWith<_SignUpModel> get copyWith =>
      __$SignUpModelCopyWithImpl<_SignUpModel>(this, _$identity);
}

abstract class _SignUpModel extends SignUpModel {
  const factory _SignUpModel(
      {String? username,
      String? email,
      String? password,
      String? confirmPassword,
      String? usernameError,
      String? emailError,
      String? passwordError,
      String? confirmPasswordError}) = _$_SignUpModel;
  const _SignUpModel._() : super._();

  @override
  String? get username => throw _privateConstructorUsedError;
  @override
  String? get email => throw _privateConstructorUsedError;
  @override
  String? get password => throw _privateConstructorUsedError;
  @override
  String? get confirmPassword => throw _privateConstructorUsedError;
  @override
  String? get usernameError => throw _privateConstructorUsedError;
  @override
  String? get emailError => throw _privateConstructorUsedError;
  @override
  String? get passwordError => throw _privateConstructorUsedError;
  @override
  String? get confirmPasswordError => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$SignUpModelCopyWith<_SignUpModel> get copyWith =>
      throw _privateConstructorUsedError;
}
