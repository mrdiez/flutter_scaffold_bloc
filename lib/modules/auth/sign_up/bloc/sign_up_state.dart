import 'package:flutter_scaffold_bloc/modules/auth/sign_up/bloc/sign_up_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'sign_up_state.freezed.dart';

/// An immutable class that define UI states
/// To generate code execute: flutter pub run build_runner build
/// For more info check the README # How to generate code (with freezed)
@freezed
class SignUpState with _$SignUpState {
  const SignUpState._();
  const factory SignUpState.init([@Default(SignUpModel()) SignUpModel model]) = SignUpInit;
  const factory SignUpState.loading(SignUpModel model) = SignUpLoading;
  const factory SignUpState.typingError(SignUpModel model) = SignUpTypingError;
  const factory SignUpState.submitError(SignUpModel model) = SignUpSubmitError;
  const factory SignUpState.success() = SignUpSuccess;

  SignUpModel get model => maybeMap(
      loading: (state) => state.model,
      typingError: (state) => state.model,
      submitError: (state) => state.model,
      orElse: () => SignUpModel());
}
