// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'sign_up_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$SignUpStateTearOff {
  const _$SignUpStateTearOff();

  SignUpInit init([SignUpModel model = const SignUpModel()]) {
    return SignUpInit(
      model,
    );
  }

  SignUpLoading loading(SignUpModel model) {
    return SignUpLoading(
      model,
    );
  }

  SignUpTypingError typingError(SignUpModel model) {
    return SignUpTypingError(
      model,
    );
  }

  SignUpSubmitError submitError(SignUpModel model) {
    return SignUpSubmitError(
      model,
    );
  }

  SignUpSuccess success() {
    return const SignUpSuccess();
  }
}

/// @nodoc
const $SignUpState = _$SignUpStateTearOff();

/// @nodoc
mixin _$SignUpState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(SignUpModel model) init,
    required TResult Function(SignUpModel model) loading,
    required TResult Function(SignUpModel model) typingError,
    required TResult Function(SignUpModel model) submitError,
    required TResult Function() success,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(SignUpModel model)? init,
    TResult Function(SignUpModel model)? loading,
    TResult Function(SignUpModel model)? typingError,
    TResult Function(SignUpModel model)? submitError,
    TResult Function()? success,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SignUpInit value) init,
    required TResult Function(SignUpLoading value) loading,
    required TResult Function(SignUpTypingError value) typingError,
    required TResult Function(SignUpSubmitError value) submitError,
    required TResult Function(SignUpSuccess value) success,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SignUpInit value)? init,
    TResult Function(SignUpLoading value)? loading,
    TResult Function(SignUpTypingError value)? typingError,
    TResult Function(SignUpSubmitError value)? submitError,
    TResult Function(SignUpSuccess value)? success,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SignUpStateCopyWith<$Res> {
  factory $SignUpStateCopyWith(
          SignUpState value, $Res Function(SignUpState) then) =
      _$SignUpStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$SignUpStateCopyWithImpl<$Res> implements $SignUpStateCopyWith<$Res> {
  _$SignUpStateCopyWithImpl(this._value, this._then);

  final SignUpState _value;
  // ignore: unused_field
  final $Res Function(SignUpState) _then;
}

/// @nodoc
abstract class $SignUpInitCopyWith<$Res> {
  factory $SignUpInitCopyWith(
          SignUpInit value, $Res Function(SignUpInit) then) =
      _$SignUpInitCopyWithImpl<$Res>;
  $Res call({SignUpModel model});

  $SignUpModelCopyWith<$Res> get model;
}

/// @nodoc
class _$SignUpInitCopyWithImpl<$Res> extends _$SignUpStateCopyWithImpl<$Res>
    implements $SignUpInitCopyWith<$Res> {
  _$SignUpInitCopyWithImpl(SignUpInit _value, $Res Function(SignUpInit) _then)
      : super(_value, (v) => _then(v as SignUpInit));

  @override
  SignUpInit get _value => super._value as SignUpInit;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(SignUpInit(
      model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as SignUpModel,
    ));
  }

  @override
  $SignUpModelCopyWith<$Res> get model {
    return $SignUpModelCopyWith<$Res>(_value.model, (value) {
      return _then(_value.copyWith(model: value));
    });
  }
}

/// @nodoc

class _$SignUpInit extends SignUpInit {
  const _$SignUpInit([this.model = const SignUpModel()]) : super._();

  @JsonKey(defaultValue: const SignUpModel())
  @override
  final SignUpModel model;

  @override
  String toString() {
    return 'SignUpState.init(model: $model)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is SignUpInit &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(model);

  @JsonKey(ignore: true)
  @override
  $SignUpInitCopyWith<SignUpInit> get copyWith =>
      _$SignUpInitCopyWithImpl<SignUpInit>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(SignUpModel model) init,
    required TResult Function(SignUpModel model) loading,
    required TResult Function(SignUpModel model) typingError,
    required TResult Function(SignUpModel model) submitError,
    required TResult Function() success,
  }) {
    return init(model);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(SignUpModel model)? init,
    TResult Function(SignUpModel model)? loading,
    TResult Function(SignUpModel model)? typingError,
    TResult Function(SignUpModel model)? submitError,
    TResult Function()? success,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(model);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SignUpInit value) init,
    required TResult Function(SignUpLoading value) loading,
    required TResult Function(SignUpTypingError value) typingError,
    required TResult Function(SignUpSubmitError value) submitError,
    required TResult Function(SignUpSuccess value) success,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SignUpInit value)? init,
    TResult Function(SignUpLoading value)? loading,
    TResult Function(SignUpTypingError value)? typingError,
    TResult Function(SignUpSubmitError value)? submitError,
    TResult Function(SignUpSuccess value)? success,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class SignUpInit extends SignUpState {
  const factory SignUpInit([SignUpModel model]) = _$SignUpInit;
  const SignUpInit._() : super._();

  SignUpModel get model => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $SignUpInitCopyWith<SignUpInit> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SignUpLoadingCopyWith<$Res> {
  factory $SignUpLoadingCopyWith(
          SignUpLoading value, $Res Function(SignUpLoading) then) =
      _$SignUpLoadingCopyWithImpl<$Res>;
  $Res call({SignUpModel model});

  $SignUpModelCopyWith<$Res> get model;
}

/// @nodoc
class _$SignUpLoadingCopyWithImpl<$Res> extends _$SignUpStateCopyWithImpl<$Res>
    implements $SignUpLoadingCopyWith<$Res> {
  _$SignUpLoadingCopyWithImpl(
      SignUpLoading _value, $Res Function(SignUpLoading) _then)
      : super(_value, (v) => _then(v as SignUpLoading));

  @override
  SignUpLoading get _value => super._value as SignUpLoading;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(SignUpLoading(
      model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as SignUpModel,
    ));
  }

  @override
  $SignUpModelCopyWith<$Res> get model {
    return $SignUpModelCopyWith<$Res>(_value.model, (value) {
      return _then(_value.copyWith(model: value));
    });
  }
}

/// @nodoc

class _$SignUpLoading extends SignUpLoading {
  const _$SignUpLoading(this.model) : super._();

  @override
  final SignUpModel model;

  @override
  String toString() {
    return 'SignUpState.loading(model: $model)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is SignUpLoading &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(model);

  @JsonKey(ignore: true)
  @override
  $SignUpLoadingCopyWith<SignUpLoading> get copyWith =>
      _$SignUpLoadingCopyWithImpl<SignUpLoading>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(SignUpModel model) init,
    required TResult Function(SignUpModel model) loading,
    required TResult Function(SignUpModel model) typingError,
    required TResult Function(SignUpModel model) submitError,
    required TResult Function() success,
  }) {
    return loading(model);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(SignUpModel model)? init,
    TResult Function(SignUpModel model)? loading,
    TResult Function(SignUpModel model)? typingError,
    TResult Function(SignUpModel model)? submitError,
    TResult Function()? success,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(model);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SignUpInit value) init,
    required TResult Function(SignUpLoading value) loading,
    required TResult Function(SignUpTypingError value) typingError,
    required TResult Function(SignUpSubmitError value) submitError,
    required TResult Function(SignUpSuccess value) success,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SignUpInit value)? init,
    TResult Function(SignUpLoading value)? loading,
    TResult Function(SignUpTypingError value)? typingError,
    TResult Function(SignUpSubmitError value)? submitError,
    TResult Function(SignUpSuccess value)? success,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class SignUpLoading extends SignUpState {
  const factory SignUpLoading(SignUpModel model) = _$SignUpLoading;
  const SignUpLoading._() : super._();

  SignUpModel get model => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $SignUpLoadingCopyWith<SignUpLoading> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SignUpTypingErrorCopyWith<$Res> {
  factory $SignUpTypingErrorCopyWith(
          SignUpTypingError value, $Res Function(SignUpTypingError) then) =
      _$SignUpTypingErrorCopyWithImpl<$Res>;
  $Res call({SignUpModel model});

  $SignUpModelCopyWith<$Res> get model;
}

/// @nodoc
class _$SignUpTypingErrorCopyWithImpl<$Res>
    extends _$SignUpStateCopyWithImpl<$Res>
    implements $SignUpTypingErrorCopyWith<$Res> {
  _$SignUpTypingErrorCopyWithImpl(
      SignUpTypingError _value, $Res Function(SignUpTypingError) _then)
      : super(_value, (v) => _then(v as SignUpTypingError));

  @override
  SignUpTypingError get _value => super._value as SignUpTypingError;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(SignUpTypingError(
      model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as SignUpModel,
    ));
  }

  @override
  $SignUpModelCopyWith<$Res> get model {
    return $SignUpModelCopyWith<$Res>(_value.model, (value) {
      return _then(_value.copyWith(model: value));
    });
  }
}

/// @nodoc

class _$SignUpTypingError extends SignUpTypingError {
  const _$SignUpTypingError(this.model) : super._();

  @override
  final SignUpModel model;

  @override
  String toString() {
    return 'SignUpState.typingError(model: $model)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is SignUpTypingError &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(model);

  @JsonKey(ignore: true)
  @override
  $SignUpTypingErrorCopyWith<SignUpTypingError> get copyWith =>
      _$SignUpTypingErrorCopyWithImpl<SignUpTypingError>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(SignUpModel model) init,
    required TResult Function(SignUpModel model) loading,
    required TResult Function(SignUpModel model) typingError,
    required TResult Function(SignUpModel model) submitError,
    required TResult Function() success,
  }) {
    return typingError(model);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(SignUpModel model)? init,
    TResult Function(SignUpModel model)? loading,
    TResult Function(SignUpModel model)? typingError,
    TResult Function(SignUpModel model)? submitError,
    TResult Function()? success,
    required TResult orElse(),
  }) {
    if (typingError != null) {
      return typingError(model);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SignUpInit value) init,
    required TResult Function(SignUpLoading value) loading,
    required TResult Function(SignUpTypingError value) typingError,
    required TResult Function(SignUpSubmitError value) submitError,
    required TResult Function(SignUpSuccess value) success,
  }) {
    return typingError(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SignUpInit value)? init,
    TResult Function(SignUpLoading value)? loading,
    TResult Function(SignUpTypingError value)? typingError,
    TResult Function(SignUpSubmitError value)? submitError,
    TResult Function(SignUpSuccess value)? success,
    required TResult orElse(),
  }) {
    if (typingError != null) {
      return typingError(this);
    }
    return orElse();
  }
}

abstract class SignUpTypingError extends SignUpState {
  const factory SignUpTypingError(SignUpModel model) = _$SignUpTypingError;
  const SignUpTypingError._() : super._();

  SignUpModel get model => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $SignUpTypingErrorCopyWith<SignUpTypingError> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SignUpSubmitErrorCopyWith<$Res> {
  factory $SignUpSubmitErrorCopyWith(
          SignUpSubmitError value, $Res Function(SignUpSubmitError) then) =
      _$SignUpSubmitErrorCopyWithImpl<$Res>;
  $Res call({SignUpModel model});

  $SignUpModelCopyWith<$Res> get model;
}

/// @nodoc
class _$SignUpSubmitErrorCopyWithImpl<$Res>
    extends _$SignUpStateCopyWithImpl<$Res>
    implements $SignUpSubmitErrorCopyWith<$Res> {
  _$SignUpSubmitErrorCopyWithImpl(
      SignUpSubmitError _value, $Res Function(SignUpSubmitError) _then)
      : super(_value, (v) => _then(v as SignUpSubmitError));

  @override
  SignUpSubmitError get _value => super._value as SignUpSubmitError;

  @override
  $Res call({
    Object? model = freezed,
  }) {
    return _then(SignUpSubmitError(
      model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as SignUpModel,
    ));
  }

  @override
  $SignUpModelCopyWith<$Res> get model {
    return $SignUpModelCopyWith<$Res>(_value.model, (value) {
      return _then(_value.copyWith(model: value));
    });
  }
}

/// @nodoc

class _$SignUpSubmitError extends SignUpSubmitError {
  const _$SignUpSubmitError(this.model) : super._();

  @override
  final SignUpModel model;

  @override
  String toString() {
    return 'SignUpState.submitError(model: $model)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is SignUpSubmitError &&
            (identical(other.model, model) ||
                const DeepCollectionEquality().equals(other.model, model)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(model);

  @JsonKey(ignore: true)
  @override
  $SignUpSubmitErrorCopyWith<SignUpSubmitError> get copyWith =>
      _$SignUpSubmitErrorCopyWithImpl<SignUpSubmitError>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(SignUpModel model) init,
    required TResult Function(SignUpModel model) loading,
    required TResult Function(SignUpModel model) typingError,
    required TResult Function(SignUpModel model) submitError,
    required TResult Function() success,
  }) {
    return submitError(model);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(SignUpModel model)? init,
    TResult Function(SignUpModel model)? loading,
    TResult Function(SignUpModel model)? typingError,
    TResult Function(SignUpModel model)? submitError,
    TResult Function()? success,
    required TResult orElse(),
  }) {
    if (submitError != null) {
      return submitError(model);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SignUpInit value) init,
    required TResult Function(SignUpLoading value) loading,
    required TResult Function(SignUpTypingError value) typingError,
    required TResult Function(SignUpSubmitError value) submitError,
    required TResult Function(SignUpSuccess value) success,
  }) {
    return submitError(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SignUpInit value)? init,
    TResult Function(SignUpLoading value)? loading,
    TResult Function(SignUpTypingError value)? typingError,
    TResult Function(SignUpSubmitError value)? submitError,
    TResult Function(SignUpSuccess value)? success,
    required TResult orElse(),
  }) {
    if (submitError != null) {
      return submitError(this);
    }
    return orElse();
  }
}

abstract class SignUpSubmitError extends SignUpState {
  const factory SignUpSubmitError(SignUpModel model) = _$SignUpSubmitError;
  const SignUpSubmitError._() : super._();

  SignUpModel get model => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $SignUpSubmitErrorCopyWith<SignUpSubmitError> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SignUpSuccessCopyWith<$Res> {
  factory $SignUpSuccessCopyWith(
          SignUpSuccess value, $Res Function(SignUpSuccess) then) =
      _$SignUpSuccessCopyWithImpl<$Res>;
}

/// @nodoc
class _$SignUpSuccessCopyWithImpl<$Res> extends _$SignUpStateCopyWithImpl<$Res>
    implements $SignUpSuccessCopyWith<$Res> {
  _$SignUpSuccessCopyWithImpl(
      SignUpSuccess _value, $Res Function(SignUpSuccess) _then)
      : super(_value, (v) => _then(v as SignUpSuccess));

  @override
  SignUpSuccess get _value => super._value as SignUpSuccess;
}

/// @nodoc

class _$SignUpSuccess extends SignUpSuccess {
  const _$SignUpSuccess() : super._();

  @override
  String toString() {
    return 'SignUpState.success()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is SignUpSuccess);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(SignUpModel model) init,
    required TResult Function(SignUpModel model) loading,
    required TResult Function(SignUpModel model) typingError,
    required TResult Function(SignUpModel model) submitError,
    required TResult Function() success,
  }) {
    return success();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(SignUpModel model)? init,
    TResult Function(SignUpModel model)? loading,
    TResult Function(SignUpModel model)? typingError,
    TResult Function(SignUpModel model)? submitError,
    TResult Function()? success,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SignUpInit value) init,
    required TResult Function(SignUpLoading value) loading,
    required TResult Function(SignUpTypingError value) typingError,
    required TResult Function(SignUpSubmitError value) submitError,
    required TResult Function(SignUpSuccess value) success,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SignUpInit value)? init,
    TResult Function(SignUpLoading value)? loading,
    TResult Function(SignUpTypingError value)? typingError,
    TResult Function(SignUpSubmitError value)? submitError,
    TResult Function(SignUpSuccess value)? success,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class SignUpSuccess extends SignUpState {
  const factory SignUpSuccess() = _$SignUpSuccess;
  const SignUpSuccess._() : super._();
}
