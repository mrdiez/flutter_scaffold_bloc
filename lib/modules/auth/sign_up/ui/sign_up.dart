import 'package:flutter/material.dart';
import 'package:flutter_scaffold_bloc/modules/auth/sign_up/bloc/sign_up_bloc.dart';
import 'package:flutter_scaffold_bloc/modules/auth/sign_up/bloc/sign_up_event.dart';
import 'package:flutter_scaffold_bloc/modules/auth/sign_up/bloc/sign_up_state.dart';
import 'package:flutter_scaffold_bloc/modules/auth/sign_up/ui/sign_up_step1.dart';
import 'package:flutter_scaffold_bloc/modules/auth/sign_up/ui/sign_up_step2.dart';
import 'package:flutter_scaffold_bloc/widgets/misc/bloc_router.dart';
import 'package:flutter_scaffold_bloc/widgets/misc/custom_bloc.dart';

class SignUpView extends CustomBlocWidget<SignUpBloc, SignUpEvent, SignUpState> {
  @override
  final provider = (_) => SignUpBloc();

  @override
  final List<BlocRoute> routes = [
    BlocRoute(
        name: BlocRoute.Initial,
        view: SignUpStep1View(),
        pushOn: [SignUpInit],
        pushMethod: BlocRoutingMethod.PushAndRemoveUntil),
    BlocRoute(
        name: '/step_2',
        view: SignUpStep2View(),
        pushOn: [SignUpSuccess],
        pushMethod: BlocRoutingMethod.PushReplacement),
  ];

  @override
  Widget builder(BuildContext context) => Stack(
        alignment: Alignment.center,
        children: [route.view, if (state is SignUpLoading) CircularProgressIndicator()],
      );
}
