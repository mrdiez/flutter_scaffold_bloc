import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_scaffold_bloc/constants/theme/sizes.dart';
import 'package:flutter_scaffold_bloc/modules/auth/auth.dart';
import 'package:flutter_scaffold_bloc/modules/auth/sign_up/bloc/sign_up_bloc.dart';
import 'package:flutter_scaffold_bloc/modules/auth/sign_up/bloc/sign_up_event.dart';
import 'package:flutter_scaffold_bloc/modules/auth/sign_up/bloc/sign_up_state.dart';
import 'package:flutter_scaffold_bloc/widgets/buttons/secondary.dart';
import 'package:flutter_scaffold_bloc/widgets/buttons/submit.dart';
import 'package:flutter_scaffold_bloc/widgets/inputs/custom_text.dart';
import 'package:flutter_scaffold_bloc/widgets/misc/custom_bloc.dart';
import 'package:flutter_scaffold_bloc/widgets/texts/title.dart';

class SignUpStep1View extends CustomBlocWidget<SignUpBloc, SignUpEvent, SignUpState> {
  final _formKey = GlobalKey<FormBuilderState>();
  final GlobalKey<CustomTextInputState> _emailKey = GlobalKey<CustomTextInputState>();
  final GlobalKey<CustomTextInputState> _usernameKey = GlobalKey<CustomTextInputState>();
  final GlobalKey<CustomTextInputState> _passwordKey = GlobalKey<CustomTextInputState>();
  final GlobalKey<CustomTextInputState> _confirmPasswordKey = GlobalKey<CustomTextInputState>();

  @override
  void onReady() {
    if (!(_usernameKey.currentState?.focusNode.hasFocus ?? true)) _usernameKey.currentState!.focusNode.requestFocus();
  }

  @override
  void onBuild() {
    if (state is SignUpSubmitError) {
      if (state.model.isUsernameError) _usernameKey.currentState?.shake();
      if (state.model.isEmailError) _emailKey.currentState?.shake();
      if (state.model.isPasswordError) _passwordKey.currentState?.shake();
      if (state.model.isConfirmPasswordError) _confirmPasswordKey.currentState?.shake();
    }
  }

  void Function()? get _onSubmit => !(state is SignUpLoading) ? () => bloc.add(SignUpEvent.submit(state.model)) : null;

  @override
  Widget builder(BuildContext context) {
    return FormBuilder(
      key: _formKey,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TitleText(translation.signUp, hero: true),
          CustomTextInput(
            label: translation.username,
            key: _usernameKey,
            keyboard: TextInputType.emailAddress,
            value: state.model.username,
            error: state.model.usernameError,
            onChanged: (_value) => bloc.add(SignUpEvent.typing(state.model.copyWith(username: _value))),
            onSubmitted: (_) => _emailKey.currentState!.focusNode.requestFocus(),
          ),
          CustomTextInput(
            label: translation.email,
            key: _emailKey,
            keyboard: TextInputType.emailAddress,
            value: state.model.email,
            error: state.model.emailError,
            onChanged: (_value) => bloc.add(SignUpEvent.typing(state.model.copyWith(email: _value))),
            onSubmitted: (_) => _passwordKey.currentState!.focusNode.requestFocus(),
          ),
          CustomTextInput(
            label: translation.password,
            key: _passwordKey,
            secret: true,
            value: state.model.password,
            error: state.model.passwordError,
            onChanged: (_value) => bloc.add(SignUpEvent.typing(state.model.copyWith(password: _value))),
            onSubmitted: (_) => _confirmPasswordKey.currentState!.focusNode.requestFocus(),
          ),
          CustomTextInput(
            label: translation.confirmPassword,
            key: _confirmPasswordKey,
            secret: true,
            value: state.model.confirmPassword,
            error: state.model.confirmPasswordError,
            onChanged: (_value) => bloc.add(SignUpEvent.typing(state.model.copyWith(confirmPassword: _value))),
            onSubmitted: (_) => (_onSubmit ?? () {})(),
          ),
          Padding(
            padding: ThemeSize.padding(M),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SubmitButton(
                  action: _onSubmit,
                  hero: true,
                ),
                SecondaryButton(
                  text: translation.cancel,
                  hero: true,
                  action: () {
                    AuthModule.navigatorKey.currentState!.pop();
                  },
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
