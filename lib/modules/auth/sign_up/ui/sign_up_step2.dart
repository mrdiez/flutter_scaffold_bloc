import 'package:flutter/material.dart';
import 'package:flutter_scaffold_bloc/constants/theme/sizes.dart';
import 'package:flutter_scaffold_bloc/constants/translations/generated/l10n.dart';
import 'package:flutter_scaffold_bloc/modules/auth/auth.dart';
import 'package:flutter_scaffold_bloc/modules/auth/sign_up/bloc/sign_up_bloc.dart';
import 'package:flutter_scaffold_bloc/modules/auth/sign_up/bloc/sign_up_event.dart';
import 'package:flutter_scaffold_bloc/modules/auth/sign_up/bloc/sign_up_state.dart';
import 'package:flutter_scaffold_bloc/widgets/buttons/submit.dart';
import 'package:flutter_scaffold_bloc/widgets/misc/custom_bloc.dart';
import 'package:flutter_scaffold_bloc/widgets/texts/simple.dart';
import 'package:flutter_scaffold_bloc/widgets/texts/title.dart';

class SignUpStep2View extends CustomBlocWidget<SignUpBloc, SignUpEvent, SignUpState> {
  @override
  Widget builder(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        TitleText(translation.signUp, hero: true),
        Padding(
          padding: ThemeSize.paddingOnly(top: S),
          child: SimpleText(translation.pleaseConfirmYourRegistration, align: TextAlign.center),
        ),
        Padding(
          padding: ThemeSize.padding(M),
          child: SubmitButton(
            text: translation.ok,
            tag: SubmitButton.DefaultText,
            action: () {
              AuthModule.navigatorKey.currentState!.pop();
            },
          ),
        ),
      ],
    );
  }
}
