import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_scaffold_bloc/constants/theme/colors.dart';
import 'package:flutter_scaffold_bloc/constants/theme/sizes.dart';
import 'package:flutter_scaffold_bloc/constants/translations/generated/l10n.dart';
import 'package:flutter_scaffold_bloc/services/authenticator.dart';
import 'package:flutter_scaffold_bloc/services/theme_manager.dart';
import 'package:flutter_scaffold_bloc/widgets/buttons/custom_icon.dart';
import 'package:flutter_scaffold_bloc/widgets/buttons/submit.dart';
import 'package:flutter_scaffold_bloc/widgets/texts/title.dart';

class HomeModule extends StatelessWidget {
  const HomeModule();
  @override
  Widget build(BuildContext context) {
    var authenticator = context.read<Authenticator>();
    var themeManager = context.read<ThemeManager>();
    var translations = Translation.of(context);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: ThemeSize.paddingOnly(bottom: M),
          child: TitleText(translations.welcome(authenticator.user!.firstname!), hero: true),
        ),
        Padding(
            padding: ThemeSize.paddingOnly(bottom: M),
            child: DropdownButton<String>(
                onChanged: (theme) => themeManager.selectTheme(theme.toString()),
                value: themeManager.model.currentTheme,
                items: ThemeColor.Switcher.keys.map((key) {
                  return DropdownMenuItem<String>(
                    value: key,
                    child: Text(key),
                  );
                }).toList())),
        Padding(
          padding: ThemeSize.paddingOnly(bottom: M),
          child: CustomIconButton(
            icon: themeManager.getIcon(context),
            text: ThemeManager.isDark(context) ? translations.lightTheme : translations.darkTheme,
            action: () {
              themeManager.toggleMode(context);
            },
          ),
        ),
        SubmitButton(
          text: translations.logOut,
          action: () {
            authenticator.add(AuthenticatorEvent.loggedOut());
          },
        ),
      ],
    );
  }
}
