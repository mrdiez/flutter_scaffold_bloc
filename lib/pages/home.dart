import 'package:flutter/material.dart';
import 'package:flutter_scaffold_bloc/constants/theme/sizes.dart';
import 'package:flutter_scaffold_bloc/modules/home/home.dart';
import 'package:flutter_scaffold_bloc/widgets/images/main_logo.dart';
import 'package:flutter_scaffold_bloc/widgets/layout/custom_sliver_bar.dart';
import 'package:flutter_scaffold_bloc/widgets/layout/sliver.view.dart';

class HomePage extends StatelessWidget {
  const HomePage();
  @override
  Widget build(BuildContext context) => SliverView(
        fillRemaining: true,
        appBar: CustomSliverBar(title: SafeArea(child: MainLogo(size: XS))),
        child: const HomeModule(),
      );
}
