import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_scaffold_bloc/constants/theme/sizes.dart';
import 'package:flutter_scaffold_bloc/modules/auth/auth.dart';
import 'package:flutter_scaffold_bloc/widgets/images/main_logo.dart';
import 'package:flutter_scaffold_bloc/widgets/layout/sliver.view.dart';

class LoginPage extends StatelessWidget {
  const LoginPage();
  static ScrollController scrollController = ScrollController();
  @override
  Widget build(BuildContext context) {
    return SliverView(
        controller: scrollController,
        fillRemaining: true,
        slivers: [
          SliverToBoxAdapter(
            child: KeyboardVisibilityBuilder(builder: (_, isKeyboardVisible) {
              if (scrollController.position.hasContentDimensions) {
                if (isKeyboardVisible) {
                  scrollController.jumpTo(scrollController.position.maxScrollExtent);
                } else {
                  scrollController.jumpTo(scrollController.position.minScrollExtent);
                }
              }
              return SafeArea(child: MainLogo(size: S));
            }),
          )
        ],
        child: const AuthModule());
  }
}
