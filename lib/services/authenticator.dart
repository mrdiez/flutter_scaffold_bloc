import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_scaffold_bloc/entities/user.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'authenticator.freezed.dart';

/// A [Bloc] class that emit states when receiving events
/// Check this to understand what is a [Bloc] and why we'll use it:
/// https://bloclibrary.dev/#/coreconcepts?id=bloc
class Authenticator extends Bloc<AuthenticatorEvent, AuthenticatorState> {
  Authenticator() : super(AuthenticatorState.init());

  User? user;
  bool get isLogged => user != null;

  @override
  Stream<AuthenticatorState> mapEventToState(AuthenticatorEvent event) => event.when(
        loggedIn: (_user) async* {
          user = _user;
          yield AuthenticatorState.authenticated(_user);
        },
        loggedOut: () async* {
          user = null;
          yield AuthenticatorState.unauthenticated();
        },
      );
}

/// An immutable generated class that define BLOC events
/// To generate code execute: flutter pub run build_runner build
/// For more info check the README # How to generate code (with freezed)
@freezed
class AuthenticatorEvent with _$AuthenticatorEvent {
  const factory AuthenticatorEvent.loggedIn(User user) = LoggedIn;
  const factory AuthenticatorEvent.loggedOut() = LoggedOut;
}

/// An immutable class that define UI states
/// To generate code execute: flutter pub run build_runner build
/// For more info check the README # How to generate code (with freezed)
@freezed
class AuthenticatorState with _$AuthenticatorState {
  const factory AuthenticatorState.init() = AuthenticatorInit;
  const factory AuthenticatorState.authenticated(User user) = Authenticated;
  const factory AuthenticatorState.unauthenticated() = Unauthenticated;
}
