import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_scaffold_bloc/constants/config/logger.dart';

/// Custom [BlocObserver] which observes all bloc and cubit instances.
class CustomBlocObserver extends BlocObserver {
  @override
  void onEvent(Bloc bloc, Object? event) {
    super.onEvent(bloc, event);
    print(event);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    Log.i(transition);
  }

  @override
  void onError(BlocBase bloc, Object error, StackTrace stackTrace) {
    Log.e(error);
    super.onError(bloc, error, stackTrace);
  }
}
