import 'package:flutter_scaffold_bloc/constants/config/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// An abstract class to manage local storage
abstract class LocalStorage {
  // This constructor prevents instantiation and extension.
  // ignore: unused_element
  LocalStorage._();

  /// The [SharedPreferences] singleton
  static SharedPreferences? _prefs;

  /// A non null getter for the [SharedPreferences] singleton
  static SharedPreferences get prefs => _prefs!;

  /// A method that init [_prefs] if it's null
  static Future<void> init() async {
    _prefs ??= await SharedPreferences.getInstance();
  }

  /// STORE
  /// A method to store a given [value] with a given [key]
  /// then return a [bool] to know if value have been stored
  static Future<bool> store(dynamic key, dynamic value) async {
    await init();
    key = key.toString();
    var toPrint = value.toString().length > 100 ? value.toString().substring(0, 100) + '...' : value.toString();
    Log.i("Local storing '$key', a <${value.runtimeType.toString()}> : '$toPrint'");

    if (value is int) {
      return prefs.setInt(key, value);
    } else if (value is double) {
      return prefs.setDouble(key, value);
    } else if (value is String) {
      return prefs.setString(key, value);
    } else if (value is List<String>) {
      return prefs.setStringList(key, value);
    } else if (value is bool) return prefs.setBool(key, value);

    return false;
  }

  /// STORE ALL
  /// A method to store multiple given [values] with their keys
  /// then return a [List<bool>] to know if values have been stored
  static Future<List<bool>> storeAll(Map<dynamic, dynamic> values) async {
    var futures = <bool>[];
    await Future.forEach(values.keys, (key) async {
      futures.add(await store(key, values[key]));
    });
    return futures;
  }

  /// GET
  /// A method to get a [T] value stored with a given [key]
  static Future<T?> get<T>(dynamic key) async {
    await init();
    key = key.toString();
    T result;

    if (T == int) {
      result = prefs.getInt(key) as T;
    } else if (T == double) {
      result = prefs.getDouble(key) as T;
    } else if (T == String) {
      result = prefs.getString(key) as T;
    } else if (T.toString() == 'List<String>') {
      result = prefs.getStringList(key) as T;
    } else if (T == bool) {
      result = prefs.getBool(key) as T;
    } else {
      result = prefs.get(key) as T;
    }

    var toPrint = result.toString().length > 100 ? result.toString().substring(0, 100) + '...' : result.toString();
    Log.i("Retrieving '$key', a locally stored <${T.toString()}> : '$toPrint'");

    return result != 'null' ? result : null;
  }

  /// GET ALL
  /// A method to get a list of [T] values stored with given [keys]
  static Future<List<T?>> getAll<T>(List<dynamic> keys) {
    var futures = <Future<T?>>[];
    keys.forEach((key) => futures.add(get<T>(key.toString())));
    return Future.wait<T?>(futures);
  }

  /// REMOVE
  /// A method to remove a value stored with a given [key]
  static Future<bool> remove(dynamic key) async {
    await init();
    key = key.toString();
    Log.i("Removing '$key' from local storage");
    return prefs.remove(key);
  }

  /// GET ALL
  /// A method to remove a list of values stored with given [keys]
  static Future<List<bool>> removeAll(List<dynamic> keys) {
    var futures = <Future<bool>>[];
    keys.forEach((key) => futures.add(remove(key.toString())));
    return Future.wait<bool>(futures);
  }

  /// CLEAR
  /// A method to remove all stored values
  static Future<bool> clear() async {
    await init();
    return prefs.clear();
  }
}
