import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'task_manager.freezed.dart';

typedef TaskList = Map<dynamic, Task>;

/// A [Bloc] class that emit states when receiving events
/// Check this to understand what is a [Bloc] and why we'll use it:
/// https://bloclibrary.dev/#/coreconcepts?id=bloc
class TaskManager extends Bloc<TaskManagerEvent, TaskManagerState> {
  TaskManager() : super(TaskManagerState.free());
  TaskList history = {};
  TaskList current = {};

  static void Function() StartLoading = () {};

  void startLoading(dynamic id) => current[id] = Task(id, startDate: DateTime.now());
  void stopLoading(dynamic id) {
    if (current.containsKey(id)) {
      history[id] = current[id]!.copyWith(endDate: DateTime.now());
      current.remove(id);
    }
  }

  void toggleLoading(dynamic id) {
    if (current.containsKey(id)) {
      stopLoading(id);
    } else {
      startLoading(id);
    }
  }

  @override
  Stream<TaskManagerState> mapEventToState(TaskManagerEvent event) => event.when(startLoading: (id) async* {
        startLoading(id);
        yield TaskManagerState.busy(current);
      }, stopLoading: (id) async* {
        stopLoading(id);
        yield current.isNotEmpty ? TaskManagerState.busy(current) : TaskManagerState.free();
      }, toggleLoading: (id) async* {
        toggleLoading(id);
        yield current.isNotEmpty ? TaskManagerState.busy(current) : TaskManagerState.free();
      });
}

/// An immutable generated class that define BLOC events
/// To generate code execute: flutter pub run build_runner build
/// For more info check the README # How to generate code (with freezed)
@freezed
class TaskManagerEvent with _$TaskManagerEvent {
  const factory TaskManagerEvent.startLoading(dynamic id) = TaskStartLoading;
  const factory TaskManagerEvent.stopLoading(dynamic id) = TaskStopLoading;
  const factory TaskManagerEvent.toggleLoading(dynamic id) = TaskToggleLoading;
}

/// An immutable class that define UI states
/// To generate code execute: flutter pub run build_runner build
/// For more info check the README # How to generate code (with freezed)
@freezed
class TaskManagerState with _$TaskManagerState {
  const factory TaskManagerState.busy(TaskList tasks) = TaskIsLoading;
  const factory TaskManagerState.free() = NoTaskLoading;
}

/// An immutable generated class model
/// To generate code execute: flutter pub run build_runner build
/// For more info check the README # How to generate code (with freezed)
@freezed
class Task with _$Task {
  const factory Task(
    dynamic id, {
    DateTime? startDate,
    DateTime? endDate,
  }) = TaskInit;
}
