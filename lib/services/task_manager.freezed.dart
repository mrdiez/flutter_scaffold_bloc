// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'task_manager.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$TaskManagerEventTearOff {
  const _$TaskManagerEventTearOff();

  TaskStartLoading startLoading(dynamic id) {
    return TaskStartLoading(
      id,
    );
  }

  TaskStopLoading stopLoading(dynamic id) {
    return TaskStopLoading(
      id,
    );
  }

  TaskToggleLoading toggleLoading(dynamic id) {
    return TaskToggleLoading(
      id,
    );
  }
}

/// @nodoc
const $TaskManagerEvent = _$TaskManagerEventTearOff();

/// @nodoc
mixin _$TaskManagerEvent {
  dynamic get id => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(dynamic id) startLoading,
    required TResult Function(dynamic id) stopLoading,
    required TResult Function(dynamic id) toggleLoading,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(dynamic id)? startLoading,
    TResult Function(dynamic id)? stopLoading,
    TResult Function(dynamic id)? toggleLoading,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(TaskStartLoading value) startLoading,
    required TResult Function(TaskStopLoading value) stopLoading,
    required TResult Function(TaskToggleLoading value) toggleLoading,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(TaskStartLoading value)? startLoading,
    TResult Function(TaskStopLoading value)? stopLoading,
    TResult Function(TaskToggleLoading value)? toggleLoading,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $TaskManagerEventCopyWith<TaskManagerEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TaskManagerEventCopyWith<$Res> {
  factory $TaskManagerEventCopyWith(
          TaskManagerEvent value, $Res Function(TaskManagerEvent) then) =
      _$TaskManagerEventCopyWithImpl<$Res>;
  $Res call({dynamic id});
}

/// @nodoc
class _$TaskManagerEventCopyWithImpl<$Res>
    implements $TaskManagerEventCopyWith<$Res> {
  _$TaskManagerEventCopyWithImpl(this._value, this._then);

  final TaskManagerEvent _value;
  // ignore: unused_field
  final $Res Function(TaskManagerEvent) _then;

  @override
  $Res call({
    Object? id = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ));
  }
}

/// @nodoc
abstract class $TaskStartLoadingCopyWith<$Res>
    implements $TaskManagerEventCopyWith<$Res> {
  factory $TaskStartLoadingCopyWith(
          TaskStartLoading value, $Res Function(TaskStartLoading) then) =
      _$TaskStartLoadingCopyWithImpl<$Res>;
  @override
  $Res call({dynamic id});
}

/// @nodoc
class _$TaskStartLoadingCopyWithImpl<$Res>
    extends _$TaskManagerEventCopyWithImpl<$Res>
    implements $TaskStartLoadingCopyWith<$Res> {
  _$TaskStartLoadingCopyWithImpl(
      TaskStartLoading _value, $Res Function(TaskStartLoading) _then)
      : super(_value, (v) => _then(v as TaskStartLoading));

  @override
  TaskStartLoading get _value => super._value as TaskStartLoading;

  @override
  $Res call({
    Object? id = freezed,
  }) {
    return _then(TaskStartLoading(
      id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ));
  }
}

/// @nodoc

class _$TaskStartLoading implements TaskStartLoading {
  const _$TaskStartLoading(this.id);

  @override
  final dynamic id;

  @override
  String toString() {
    return 'TaskManagerEvent.startLoading(id: $id)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is TaskStartLoading &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(id);

  @JsonKey(ignore: true)
  @override
  $TaskStartLoadingCopyWith<TaskStartLoading> get copyWith =>
      _$TaskStartLoadingCopyWithImpl<TaskStartLoading>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(dynamic id) startLoading,
    required TResult Function(dynamic id) stopLoading,
    required TResult Function(dynamic id) toggleLoading,
  }) {
    return startLoading(id);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(dynamic id)? startLoading,
    TResult Function(dynamic id)? stopLoading,
    TResult Function(dynamic id)? toggleLoading,
    required TResult orElse(),
  }) {
    if (startLoading != null) {
      return startLoading(id);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(TaskStartLoading value) startLoading,
    required TResult Function(TaskStopLoading value) stopLoading,
    required TResult Function(TaskToggleLoading value) toggleLoading,
  }) {
    return startLoading(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(TaskStartLoading value)? startLoading,
    TResult Function(TaskStopLoading value)? stopLoading,
    TResult Function(TaskToggleLoading value)? toggleLoading,
    required TResult orElse(),
  }) {
    if (startLoading != null) {
      return startLoading(this);
    }
    return orElse();
  }
}

abstract class TaskStartLoading implements TaskManagerEvent {
  const factory TaskStartLoading(dynamic id) = _$TaskStartLoading;

  @override
  dynamic get id => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  $TaskStartLoadingCopyWith<TaskStartLoading> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TaskStopLoadingCopyWith<$Res>
    implements $TaskManagerEventCopyWith<$Res> {
  factory $TaskStopLoadingCopyWith(
          TaskStopLoading value, $Res Function(TaskStopLoading) then) =
      _$TaskStopLoadingCopyWithImpl<$Res>;
  @override
  $Res call({dynamic id});
}

/// @nodoc
class _$TaskStopLoadingCopyWithImpl<$Res>
    extends _$TaskManagerEventCopyWithImpl<$Res>
    implements $TaskStopLoadingCopyWith<$Res> {
  _$TaskStopLoadingCopyWithImpl(
      TaskStopLoading _value, $Res Function(TaskStopLoading) _then)
      : super(_value, (v) => _then(v as TaskStopLoading));

  @override
  TaskStopLoading get _value => super._value as TaskStopLoading;

  @override
  $Res call({
    Object? id = freezed,
  }) {
    return _then(TaskStopLoading(
      id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ));
  }
}

/// @nodoc

class _$TaskStopLoading implements TaskStopLoading {
  const _$TaskStopLoading(this.id);

  @override
  final dynamic id;

  @override
  String toString() {
    return 'TaskManagerEvent.stopLoading(id: $id)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is TaskStopLoading &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(id);

  @JsonKey(ignore: true)
  @override
  $TaskStopLoadingCopyWith<TaskStopLoading> get copyWith =>
      _$TaskStopLoadingCopyWithImpl<TaskStopLoading>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(dynamic id) startLoading,
    required TResult Function(dynamic id) stopLoading,
    required TResult Function(dynamic id) toggleLoading,
  }) {
    return stopLoading(id);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(dynamic id)? startLoading,
    TResult Function(dynamic id)? stopLoading,
    TResult Function(dynamic id)? toggleLoading,
    required TResult orElse(),
  }) {
    if (stopLoading != null) {
      return stopLoading(id);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(TaskStartLoading value) startLoading,
    required TResult Function(TaskStopLoading value) stopLoading,
    required TResult Function(TaskToggleLoading value) toggleLoading,
  }) {
    return stopLoading(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(TaskStartLoading value)? startLoading,
    TResult Function(TaskStopLoading value)? stopLoading,
    TResult Function(TaskToggleLoading value)? toggleLoading,
    required TResult orElse(),
  }) {
    if (stopLoading != null) {
      return stopLoading(this);
    }
    return orElse();
  }
}

abstract class TaskStopLoading implements TaskManagerEvent {
  const factory TaskStopLoading(dynamic id) = _$TaskStopLoading;

  @override
  dynamic get id => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  $TaskStopLoadingCopyWith<TaskStopLoading> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TaskToggleLoadingCopyWith<$Res>
    implements $TaskManagerEventCopyWith<$Res> {
  factory $TaskToggleLoadingCopyWith(
          TaskToggleLoading value, $Res Function(TaskToggleLoading) then) =
      _$TaskToggleLoadingCopyWithImpl<$Res>;
  @override
  $Res call({dynamic id});
}

/// @nodoc
class _$TaskToggleLoadingCopyWithImpl<$Res>
    extends _$TaskManagerEventCopyWithImpl<$Res>
    implements $TaskToggleLoadingCopyWith<$Res> {
  _$TaskToggleLoadingCopyWithImpl(
      TaskToggleLoading _value, $Res Function(TaskToggleLoading) _then)
      : super(_value, (v) => _then(v as TaskToggleLoading));

  @override
  TaskToggleLoading get _value => super._value as TaskToggleLoading;

  @override
  $Res call({
    Object? id = freezed,
  }) {
    return _then(TaskToggleLoading(
      id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ));
  }
}

/// @nodoc

class _$TaskToggleLoading implements TaskToggleLoading {
  const _$TaskToggleLoading(this.id);

  @override
  final dynamic id;

  @override
  String toString() {
    return 'TaskManagerEvent.toggleLoading(id: $id)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is TaskToggleLoading &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(id);

  @JsonKey(ignore: true)
  @override
  $TaskToggleLoadingCopyWith<TaskToggleLoading> get copyWith =>
      _$TaskToggleLoadingCopyWithImpl<TaskToggleLoading>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(dynamic id) startLoading,
    required TResult Function(dynamic id) stopLoading,
    required TResult Function(dynamic id) toggleLoading,
  }) {
    return toggleLoading(id);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(dynamic id)? startLoading,
    TResult Function(dynamic id)? stopLoading,
    TResult Function(dynamic id)? toggleLoading,
    required TResult orElse(),
  }) {
    if (toggleLoading != null) {
      return toggleLoading(id);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(TaskStartLoading value) startLoading,
    required TResult Function(TaskStopLoading value) stopLoading,
    required TResult Function(TaskToggleLoading value) toggleLoading,
  }) {
    return toggleLoading(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(TaskStartLoading value)? startLoading,
    TResult Function(TaskStopLoading value)? stopLoading,
    TResult Function(TaskToggleLoading value)? toggleLoading,
    required TResult orElse(),
  }) {
    if (toggleLoading != null) {
      return toggleLoading(this);
    }
    return orElse();
  }
}

abstract class TaskToggleLoading implements TaskManagerEvent {
  const factory TaskToggleLoading(dynamic id) = _$TaskToggleLoading;

  @override
  dynamic get id => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  $TaskToggleLoadingCopyWith<TaskToggleLoading> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
class _$TaskManagerStateTearOff {
  const _$TaskManagerStateTearOff();

  TaskIsLoading busy(Map<dynamic, Task> tasks) {
    return TaskIsLoading(
      tasks,
    );
  }

  NoTaskLoading free() {
    return const NoTaskLoading();
  }
}

/// @nodoc
const $TaskManagerState = _$TaskManagerStateTearOff();

/// @nodoc
mixin _$TaskManagerState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Map<dynamic, Task> tasks) busy,
    required TResult Function() free,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Map<dynamic, Task> tasks)? busy,
    TResult Function()? free,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(TaskIsLoading value) busy,
    required TResult Function(NoTaskLoading value) free,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(TaskIsLoading value)? busy,
    TResult Function(NoTaskLoading value)? free,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TaskManagerStateCopyWith<$Res> {
  factory $TaskManagerStateCopyWith(
          TaskManagerState value, $Res Function(TaskManagerState) then) =
      _$TaskManagerStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$TaskManagerStateCopyWithImpl<$Res>
    implements $TaskManagerStateCopyWith<$Res> {
  _$TaskManagerStateCopyWithImpl(this._value, this._then);

  final TaskManagerState _value;
  // ignore: unused_field
  final $Res Function(TaskManagerState) _then;
}

/// @nodoc
abstract class $TaskIsLoadingCopyWith<$Res> {
  factory $TaskIsLoadingCopyWith(
          TaskIsLoading value, $Res Function(TaskIsLoading) then) =
      _$TaskIsLoadingCopyWithImpl<$Res>;
  $Res call({Map<dynamic, Task> tasks});
}

/// @nodoc
class _$TaskIsLoadingCopyWithImpl<$Res>
    extends _$TaskManagerStateCopyWithImpl<$Res>
    implements $TaskIsLoadingCopyWith<$Res> {
  _$TaskIsLoadingCopyWithImpl(
      TaskIsLoading _value, $Res Function(TaskIsLoading) _then)
      : super(_value, (v) => _then(v as TaskIsLoading));

  @override
  TaskIsLoading get _value => super._value as TaskIsLoading;

  @override
  $Res call({
    Object? tasks = freezed,
  }) {
    return _then(TaskIsLoading(
      tasks == freezed
          ? _value.tasks
          : tasks // ignore: cast_nullable_to_non_nullable
              as Map<dynamic, Task>,
    ));
  }
}

/// @nodoc

class _$TaskIsLoading implements TaskIsLoading {
  const _$TaskIsLoading(this.tasks);

  @override
  final Map<dynamic, Task> tasks;

  @override
  String toString() {
    return 'TaskManagerState.busy(tasks: $tasks)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is TaskIsLoading &&
            (identical(other.tasks, tasks) ||
                const DeepCollectionEquality().equals(other.tasks, tasks)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(tasks);

  @JsonKey(ignore: true)
  @override
  $TaskIsLoadingCopyWith<TaskIsLoading> get copyWith =>
      _$TaskIsLoadingCopyWithImpl<TaskIsLoading>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Map<dynamic, Task> tasks) busy,
    required TResult Function() free,
  }) {
    return busy(tasks);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Map<dynamic, Task> tasks)? busy,
    TResult Function()? free,
    required TResult orElse(),
  }) {
    if (busy != null) {
      return busy(tasks);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(TaskIsLoading value) busy,
    required TResult Function(NoTaskLoading value) free,
  }) {
    return busy(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(TaskIsLoading value)? busy,
    TResult Function(NoTaskLoading value)? free,
    required TResult orElse(),
  }) {
    if (busy != null) {
      return busy(this);
    }
    return orElse();
  }
}

abstract class TaskIsLoading implements TaskManagerState {
  const factory TaskIsLoading(Map<dynamic, Task> tasks) = _$TaskIsLoading;

  Map<dynamic, Task> get tasks => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TaskIsLoadingCopyWith<TaskIsLoading> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NoTaskLoadingCopyWith<$Res> {
  factory $NoTaskLoadingCopyWith(
          NoTaskLoading value, $Res Function(NoTaskLoading) then) =
      _$NoTaskLoadingCopyWithImpl<$Res>;
}

/// @nodoc
class _$NoTaskLoadingCopyWithImpl<$Res>
    extends _$TaskManagerStateCopyWithImpl<$Res>
    implements $NoTaskLoadingCopyWith<$Res> {
  _$NoTaskLoadingCopyWithImpl(
      NoTaskLoading _value, $Res Function(NoTaskLoading) _then)
      : super(_value, (v) => _then(v as NoTaskLoading));

  @override
  NoTaskLoading get _value => super._value as NoTaskLoading;
}

/// @nodoc

class _$NoTaskLoading implements NoTaskLoading {
  const _$NoTaskLoading();

  @override
  String toString() {
    return 'TaskManagerState.free()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is NoTaskLoading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Map<dynamic, Task> tasks) busy,
    required TResult Function() free,
  }) {
    return free();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Map<dynamic, Task> tasks)? busy,
    TResult Function()? free,
    required TResult orElse(),
  }) {
    if (free != null) {
      return free();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(TaskIsLoading value) busy,
    required TResult Function(NoTaskLoading value) free,
  }) {
    return free(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(TaskIsLoading value)? busy,
    TResult Function(NoTaskLoading value)? free,
    required TResult orElse(),
  }) {
    if (free != null) {
      return free(this);
    }
    return orElse();
  }
}

abstract class NoTaskLoading implements TaskManagerState {
  const factory NoTaskLoading() = _$NoTaskLoading;
}

/// @nodoc
class _$TaskTearOff {
  const _$TaskTearOff();

  TaskInit call(dynamic id, {DateTime? startDate, DateTime? endDate}) {
    return TaskInit(
      id,
      startDate: startDate,
      endDate: endDate,
    );
  }
}

/// @nodoc
const $Task = _$TaskTearOff();

/// @nodoc
mixin _$Task {
  dynamic get id => throw _privateConstructorUsedError;
  DateTime? get startDate => throw _privateConstructorUsedError;
  DateTime? get endDate => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $TaskCopyWith<Task> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TaskCopyWith<$Res> {
  factory $TaskCopyWith(Task value, $Res Function(Task) then) =
      _$TaskCopyWithImpl<$Res>;
  $Res call({dynamic id, DateTime? startDate, DateTime? endDate});
}

/// @nodoc
class _$TaskCopyWithImpl<$Res> implements $TaskCopyWith<$Res> {
  _$TaskCopyWithImpl(this._value, this._then);

  final Task _value;
  // ignore: unused_field
  final $Res Function(Task) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? startDate = freezed,
    Object? endDate = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as dynamic,
      startDate: startDate == freezed
          ? _value.startDate
          : startDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      endDate: endDate == freezed
          ? _value.endDate
          : endDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
    ));
  }
}

/// @nodoc
abstract class $TaskInitCopyWith<$Res> implements $TaskCopyWith<$Res> {
  factory $TaskInitCopyWith(TaskInit value, $Res Function(TaskInit) then) =
      _$TaskInitCopyWithImpl<$Res>;
  @override
  $Res call({dynamic id, DateTime? startDate, DateTime? endDate});
}

/// @nodoc
class _$TaskInitCopyWithImpl<$Res> extends _$TaskCopyWithImpl<$Res>
    implements $TaskInitCopyWith<$Res> {
  _$TaskInitCopyWithImpl(TaskInit _value, $Res Function(TaskInit) _then)
      : super(_value, (v) => _then(v as TaskInit));

  @override
  TaskInit get _value => super._value as TaskInit;

  @override
  $Res call({
    Object? id = freezed,
    Object? startDate = freezed,
    Object? endDate = freezed,
  }) {
    return _then(TaskInit(
      id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as dynamic,
      startDate: startDate == freezed
          ? _value.startDate
          : startDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      endDate: endDate == freezed
          ? _value.endDate
          : endDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
    ));
  }
}

/// @nodoc

class _$TaskInit implements TaskInit {
  const _$TaskInit(this.id, {this.startDate, this.endDate});

  @override
  final dynamic id;
  @override
  final DateTime? startDate;
  @override
  final DateTime? endDate;

  @override
  String toString() {
    return 'Task(id: $id, startDate: $startDate, endDate: $endDate)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is TaskInit &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.startDate, startDate) ||
                const DeepCollectionEquality()
                    .equals(other.startDate, startDate)) &&
            (identical(other.endDate, endDate) ||
                const DeepCollectionEquality().equals(other.endDate, endDate)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(startDate) ^
      const DeepCollectionEquality().hash(endDate);

  @JsonKey(ignore: true)
  @override
  $TaskInitCopyWith<TaskInit> get copyWith =>
      _$TaskInitCopyWithImpl<TaskInit>(this, _$identity);
}

abstract class TaskInit implements Task {
  const factory TaskInit(dynamic id, {DateTime? startDate, DateTime? endDate}) =
      _$TaskInit;

  @override
  dynamic get id => throw _privateConstructorUsedError;
  @override
  DateTime? get startDate => throw _privateConstructorUsedError;
  @override
  DateTime? get endDate => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  $TaskInitCopyWith<TaskInit> get copyWith =>
      throw _privateConstructorUsedError;
}
