import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_scaffold_bloc/constants/config/logger.dart';
import 'package:flutter_scaffold_bloc/constants/keys.dart';
import 'package:flutter_scaffold_bloc/constants/theme/colors.dart';
import 'package:flutter_scaffold_bloc/services/local_storage.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'theme_manager.freezed.dart';

/// A [Cubit] class that emit [ThemeMode]
/// Check this to understand what is a [Cubit] and why we'll use it:
/// https://bloclibrary.dev/#/coreconcepts?id=cubit
class ThemeManager extends Cubit<ThemeManagerModel> {
  /// Constructor that set initial value [ThemeMode.System] to our [Cubit]
  /// and that also call [_initMode]
  ThemeManager() : super(ThemeManagerModel()) {
    _initMode();
  }

  ThemeManagerModel model = ThemeManagerModel();

  /// INIT MODE
  /// A method that init [mode] and [currentTheme]
  /// with stored values [StorageKey.ThemeMode] and [StorageKey.SelectedTheme]
  Future<void> _initMode() async {
    var _mode = await LocalStorage.get(StorageKey.ThemeMode);
    if (_mode != null) model = model.copyWith(mode: _mode == 'ThemeMode.dark' ? ThemeMode.dark : ThemeMode.light);
    var _theme = await LocalStorage.get(StorageKey.SelectedTheme);
    if (_theme != null) {
      model = model.copyWith(currentTheme: _theme);
      ThemeColor.Switcher[_theme]!();
    }
    emit(model);
  }

  /// IS DARK
  /// A simple method to know if applied theme is dark or not
  static bool isDark(BuildContext context) => Theme.of(context).brightness == Brightness.dark;

  /// SELECT THEME
  /// A method that use [ThemeColor.Switcher]
  /// to select a new set of [ThemeColor]
  /// and store choice with [LocalStorageService] and [StorageKey.SelectedTheme]
  Future<void> selectTheme(String theme) async {
    if (model.currentTheme == theme) return;
    model = model.copyWith(currentTheme: theme);
    ThemeColor.Switcher[theme]!();
    Log.i('New theme selected: $theme');
    await LocalStorage.store(StorageKey.SelectedTheme, theme);
    emit(model);
  }

  /// TOGGLE MODE
  /// A method to switch between [ThemeMode.light] or [ThemeMode.dark]
  Future<void> toggleMode(BuildContext context) async {
    model = model.copyWith(mode: isDark(context) ? ThemeMode.light : ThemeMode.dark);
    Log.i('Theme mode changed: ${model.mode.toString()}');
    await LocalStorage.store(StorageKey.ThemeMode, model.mode.toString());
    emit(model);
  }

  /// THEME SWITCHER ICON
  /// A getter that return theme's [IconData]:
  /// sunny icon when theme is [ThemeMode.dark]
  /// moon icon when theme is [ThemeMode.light]
  IconData getIcon(BuildContext context) {
    return isDark(context) ? Icons.wb_sunny : Icons.nights_stay;
  }
}

/// An immutable generated class model
/// To generate code execute: flutter pub run build_runner build
/// For more info check the README # How to generate code (with freezed)
@freezed
class ThemeManagerModel with _$ThemeManagerModel {
  const ThemeManagerModel._();
  const factory ThemeManagerModel(
      {

      /// The actually selected [ThemeData] name
      @Default(ThemeColor.DefaultTheme) String currentTheme,

      /// The actually selected [ThemeMode]
      @Default(ThemeMode.system) ThemeMode mode}) = _ThemeManagerModel;
}
