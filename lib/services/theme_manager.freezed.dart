// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'theme_manager.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$ThemeManagerModelTearOff {
  const _$ThemeManagerModelTearOff();

  _ThemeManagerModel call(
      {String currentTheme = ThemeColor.DefaultTheme,
      ThemeMode mode = ThemeMode.system}) {
    return _ThemeManagerModel(
      currentTheme: currentTheme,
      mode: mode,
    );
  }
}

/// @nodoc
const $ThemeManagerModel = _$ThemeManagerModelTearOff();

/// @nodoc
mixin _$ThemeManagerModel {
  /// The actually selected [ThemeData] name
  String get currentTheme => throw _privateConstructorUsedError;

  /// The actually selected [ThemeMode]
  ThemeMode get mode => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ThemeManagerModelCopyWith<ThemeManagerModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ThemeManagerModelCopyWith<$Res> {
  factory $ThemeManagerModelCopyWith(
          ThemeManagerModel value, $Res Function(ThemeManagerModel) then) =
      _$ThemeManagerModelCopyWithImpl<$Res>;
  $Res call({String currentTheme, ThemeMode mode});
}

/// @nodoc
class _$ThemeManagerModelCopyWithImpl<$Res>
    implements $ThemeManagerModelCopyWith<$Res> {
  _$ThemeManagerModelCopyWithImpl(this._value, this._then);

  final ThemeManagerModel _value;
  // ignore: unused_field
  final $Res Function(ThemeManagerModel) _then;

  @override
  $Res call({
    Object? currentTheme = freezed,
    Object? mode = freezed,
  }) {
    return _then(_value.copyWith(
      currentTheme: currentTheme == freezed
          ? _value.currentTheme
          : currentTheme // ignore: cast_nullable_to_non_nullable
              as String,
      mode: mode == freezed
          ? _value.mode
          : mode // ignore: cast_nullable_to_non_nullable
              as ThemeMode,
    ));
  }
}

/// @nodoc
abstract class _$ThemeManagerModelCopyWith<$Res>
    implements $ThemeManagerModelCopyWith<$Res> {
  factory _$ThemeManagerModelCopyWith(
          _ThemeManagerModel value, $Res Function(_ThemeManagerModel) then) =
      __$ThemeManagerModelCopyWithImpl<$Res>;
  @override
  $Res call({String currentTheme, ThemeMode mode});
}

/// @nodoc
class __$ThemeManagerModelCopyWithImpl<$Res>
    extends _$ThemeManagerModelCopyWithImpl<$Res>
    implements _$ThemeManagerModelCopyWith<$Res> {
  __$ThemeManagerModelCopyWithImpl(
      _ThemeManagerModel _value, $Res Function(_ThemeManagerModel) _then)
      : super(_value, (v) => _then(v as _ThemeManagerModel));

  @override
  _ThemeManagerModel get _value => super._value as _ThemeManagerModel;

  @override
  $Res call({
    Object? currentTheme = freezed,
    Object? mode = freezed,
  }) {
    return _then(_ThemeManagerModel(
      currentTheme: currentTheme == freezed
          ? _value.currentTheme
          : currentTheme // ignore: cast_nullable_to_non_nullable
              as String,
      mode: mode == freezed
          ? _value.mode
          : mode // ignore: cast_nullable_to_non_nullable
              as ThemeMode,
    ));
  }
}

/// @nodoc

class _$_ThemeManagerModel extends _ThemeManagerModel {
  const _$_ThemeManagerModel(
      {this.currentTheme = ThemeColor.DefaultTheme,
      this.mode = ThemeMode.system})
      : super._();

  @JsonKey(defaultValue: ThemeColor.DefaultTheme)
  @override

  /// The actually selected [ThemeData] name
  final String currentTheme;
  @JsonKey(defaultValue: ThemeMode.system)
  @override

  /// The actually selected [ThemeMode]
  final ThemeMode mode;

  @override
  String toString() {
    return 'ThemeManagerModel(currentTheme: $currentTheme, mode: $mode)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ThemeManagerModel &&
            (identical(other.currentTheme, currentTheme) ||
                const DeepCollectionEquality()
                    .equals(other.currentTheme, currentTheme)) &&
            (identical(other.mode, mode) ||
                const DeepCollectionEquality().equals(other.mode, mode)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(currentTheme) ^
      const DeepCollectionEquality().hash(mode);

  @JsonKey(ignore: true)
  @override
  _$ThemeManagerModelCopyWith<_ThemeManagerModel> get copyWith =>
      __$ThemeManagerModelCopyWithImpl<_ThemeManagerModel>(this, _$identity);
}

abstract class _ThemeManagerModel extends ThemeManagerModel {
  const factory _ThemeManagerModel({String currentTheme, ThemeMode mode}) =
      _$_ThemeManagerModel;
  const _ThemeManagerModel._() : super._();

  @override

  /// The actually selected [ThemeData] name
  String get currentTheme => throw _privateConstructorUsedError;
  @override

  /// The actually selected [ThemeMode]
  ThemeMode get mode => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$ThemeManagerModelCopyWith<_ThemeManagerModel> get copyWith =>
      throw _privateConstructorUsedError;
}
