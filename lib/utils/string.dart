import 'package:flutter/material.dart';
import 'package:flutter_scaffold_bloc/utils/regex.dart';

extension StringTool on String {
  static bool notNull(String? str) => str != null && str.isNotEmpty && str != 'null';
  bool get isNotNull => StringTool.notNull(this);
  String get capitalize => '${this[0].toUpperCase()}${substring(1)}';
  String get capitalizeAll => split(' ').reduce((a, b) => a.capitalize + b.capitalize);
  String plural(int nb) => nb > 1 ? '${this}s' : this;
  String get removeLast => substring(0, length - 1);
  String get removeLastZeros {
    if ((endsWith('0') && contains('.')) || endsWith('.') || endsWith(',') || endsWith(' ')) {
      return removeLast.removeLastZeros;
    } else {
      return this;
    }
  }

  String get removeException => getAllAfter('Exception: ');

  String getAllAfter(String target) {
    if (!contains(target)) return this;
    return substring(indexOf(target) + target.length);
  }

  String removeAllAfter(String target, [bool include = false]) {
    if (!contains(target)) return this;
    return substring(0, indexOf(target) + (include ? 0 : 1));
  }

  String removeAllAfterLast(String target, [bool include = false]) {
    if (!contains(target)) return this;
    return substring(0, lastIndexOf(target) + (include ? 0 : 1));
  }

  String? allBetween(String str1, String str2) {
    if (length <= str1.length + str2.length) return null;
    final startIndex = indexOf(str1);
    final endIndex = indexOf(str2, startIndex + str1.length);
    return (startIndex < 0 || endIndex < 0) ? null : substring(startIndex + str1.length, endIndex);
  }

  String replaceBetweenTwo(String replace, String str1, String str2) {
    final startIndex = indexOf(str1);
    final endIndex = indexOf(str2, startIndex + str1.length);

    if (startIndex < 0 || endIndex < 0) return this;

    final firstPart = substring(0, startIndex + str1.length);
    final endPart = substring(endIndex, length);

    return firstPart + replace + endPart;
  }

  static String joinNonNullStrings(List<String> stringList) =>
      stringList.where((str) => StringTool.notNull(str)).join((', '));

  String limit(int limit) => substring(0, length >= limit ? limit : length) + (length > limit ? '...' : '');

  List<String> splitEvery(int n) {
    if (length <= n) return [this];
    var exp = RegExp(r'\d{' '$n' '}');
    Iterable<Match> matches = exp.allMatches(this);
    return matches.map((match) => match.group(0)!).toList();
  }

  Key get key => Key(this);
  bool get isEmail => RegexTool.email.hasMatch(this);
}
