import 'package:flutter/material.dart';
import 'package:flutter_scaffold_bloc/utils/string.dart';

extension WidgetTool on Widget {
  static Widget heroWrapper({String? tag, required Widget child}) =>
      StringTool.notNull(tag) ? Hero(tag: tag!, child: child) : child;

  Widget wrapWithHero(dynamic tag) => WidgetTool.heroWrapper(tag: tag.toString(), child: this);

  Widget expand([bool shouldExpand = true]) => shouldExpand
      ? Row(
          children: [Expanded(child: this)],
        )
      : this;
}
