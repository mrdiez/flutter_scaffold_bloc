import 'package:flutter/material.dart';
import 'package:flutter_scaffold_bloc/constants/theme/sizes.dart';
import 'package:flutter_scaffold_bloc/utils/widget.dart';

class CustomBackButton extends StatelessWidget {
  final Color? color;
  final Sizing? size;
  final String? tag;
  final Function? action;
  final EdgeInsets? padding;
  final IconData? icon;

  CustomBackButton(
      {this.icon = Icons.arrow_back, this.color, this.size, this.tag = 'BACK_BUTTON', this.action, this.padding});

  void onTap(BuildContext context) async {
    if (action != null) await action!();
  }

  double get width => _size + _padding.horizontal;
  double get height => _size + _padding.vertical;
  double get _size => ThemeSize.icon(size ?? M);
  EdgeInsets get _padding => padding ?? ThemeSize.padding(S);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: color ?? Theme.of(context).iconTheme.color,
      width: width,
      height: height,
      child: Center(
        child: IconButton(
            icon: Icon(
              icon,
              size: _size,
              color: Theme.of(context).colorScheme.onPrimary,
            ),
            iconSize: _size,
            onPressed: () => onTap(context)),
      ),
    ).wrapWithHero(tag);
  }
}
