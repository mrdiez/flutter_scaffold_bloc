import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_scaffold_bloc/constants/theme/sizes.dart';
import 'package:flutter_scaffold_bloc/utils/string.dart';
import 'package:flutter_scaffold_bloc/utils/widget.dart';

class CustomIconButton extends StatelessWidget {
  const CustomIconButton({required this.icon, this.action, this.hero = false, this.tag, this.text});

  final Function()? action;
  final bool hero;
  final String? tag;
  final String? text;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    var isText = StringTool.notNull(text);
    return ElevatedButton(
      onPressed: action,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: ThemeSize.paddingOnly(right: isText ? S : Zero),
            child: Icon(icon),
          ),
          if (isText) Text(text!)
        ],
      ),
    ).wrapWithHero(hero ? tag ?? text : tag);
  }
}
