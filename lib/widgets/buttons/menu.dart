import 'package:flutter/material.dart';
import 'package:flutter_scaffold_bloc/constants/theme/sizes.dart';
import 'package:flutter_scaffold_bloc/utils/widget.dart';
import 'package:flutter_scaffold_bloc/widgets/layout/root_container.dart';

class MenuButton extends StatelessWidget {
  final Color? color;
  final Sizing? size;
  final String? tag;
  final Function? action;
  final EdgeInsets? padding;
  final IconData? icon;

  MenuButton({this.icon = Icons.menu, this.color, this.size, this.tag = 'MENU_BUTTON', this.action, this.padding});

  void onTap(BuildContext context) async {
    if (action != null) await action!();
    RootContainer.scaffoldKey.currentState?.openDrawer();
  }

  double get width => _size + _padding.horizontal;
  double get height => _size + _padding.vertical;
  double get _size => ThemeSize.icon(size ?? M);
  EdgeInsets get _padding => padding ?? ThemeSize.padding(S);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: color ?? Theme.of(context).iconTheme.color,
      width: width,
      height: height,
      child: Center(
        child: IconButton(
            icon: Icon(
              icon,
              size: _size,
              color: Theme.of(context).colorScheme.onPrimary,
            ),
            iconSize: _size,
            onPressed: () => onTap(context)),
      ),
    ).wrapWithHero(tag);
  }
}
