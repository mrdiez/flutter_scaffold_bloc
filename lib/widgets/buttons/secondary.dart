import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_scaffold_bloc/constants/theme/sizes.dart';
import 'package:flutter_scaffold_bloc/utils/widget.dart';

class SecondaryButton extends StatelessWidget {
  const SecondaryButton({this.action, this.hero = false, this.tag, required this.text});
  final Function()? action;
  final bool hero;
  final String? tag;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: ThemeSize.paddingSymmetric(S, Zero),
      child: TextButton(
        onPressed: action,
        child: AutoSizeText(text, maxLines: 1),
      ).wrapWithHero(hero ? tag ?? text : tag),
    );
  }
}
