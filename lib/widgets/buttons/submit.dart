import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_scaffold_bloc/utils/widget.dart';

class SubmitButton extends StatelessWidget {
  const SubmitButton({this.action, this.hero = false, this.tag, this.text = DefaultText});
  static const String DefaultText = 'Submit';
  final Function()? action;
  final bool hero;
  final String? tag;
  final String text;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: action,
      child: Text(text),
    ).wrapWithHero(hero ? tag ?? text : tag);
  }
}
