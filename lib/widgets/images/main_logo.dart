import 'package:flutter/material.dart';
import 'package:flutter_scaffold_bloc/constants/assets.dart';
import 'package:flutter_scaffold_bloc/constants/theme/sizes.dart';
import 'package:flutter_scaffold_bloc/services/theme_manager.dart';
import 'package:flutter_scaffold_bloc/widgets/images/simple_image.dart';

class MainLogo extends StatefulWidget {
  final Axis? axis;
  final String hero;
  final Sizing? size;
  final double height;
  final double width;

  MainLogo({key, this.size, this.axis, this.hero = "MAIN_LOGO"})
      : height = ThemeSize.image(size ?? M),
        width = ThemeSize.image(size ?? M),
        super(key: key);

  @override
  MainLogoState createState() => MainLogoState();
}

class MainLogoState extends State<MainLogo> {
  String path = Asset.VerticalLogo;

  void _setPath(BuildContext context) {
    var isDark = ThemeManager.isDark(context);
    switch (widget.axis) {
      case Axis.vertical:
        path = isDark ? Asset.VerticalDarkLogo : Asset.VerticalLogo;
        break;
      case Axis.horizontal:
        path = isDark ? Asset.HorizontalDarkLogo : Asset.HorizontalLogo;
        break;
      default:
        path = Asset.VerticalLogo;
    }
  }

  @override
  Widget build(BuildContext context) {
    _setPath(context);

    return SimpleImage(
      path: path,
      hero: widget.hero,
      height: widget.height,
      width: widget.width,
    );
  }
}
