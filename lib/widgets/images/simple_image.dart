import 'package:flutter/material.dart';
import 'package:flutter_scaffold_bloc/constants/theme/sizes.dart';
import 'package:flutter_scaffold_bloc/utils/widget.dart';

class SimpleImage extends StatelessWidget {
  final Sizing? size;
  final double? height;
  final double? width;
  final String path;
  final String? hero;

  SimpleImage({key, this.size, this.height, this.width, required this.path, this.hero});

  double get _size => ThemeSize.image(size ?? M);
  double get _height => height ?? _size;
  double get _width => width ?? _size;

  @override
  Widget build(BuildContext context) {
    return Container(
        height: _height,
        width: _width,
        child: Center(
          child: Image.asset(
            path,
            height: _height,
            width: _width,
          ).wrapWithHero(hero),
        ));
  }
}
