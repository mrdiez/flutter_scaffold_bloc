import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_scaffold_bloc/utils/widget.dart';
import 'package:flutter_scaffold_bloc/widgets/misc/shake_on_trigger.dart';

class CustomRadioInput<T> extends StatefulWidget {
  CustomRadioInput(
      {Key? key,
      required this.valuesAndLabels,
      this.label,
      this.value,
      this.error,
      this.onChanged,
      this.onSubmitted,
      this.validator})
      : super(key: key);

  final T? value;
  final String? label;
  final Map<T, dynamic> valuesAndLabels;
  final String? error;
  final Function(dynamic)? onChanged;
  final Function(dynamic)? onSubmitted;
  final FormFieldValidator? validator;

  @override
  CustomRadioInputState createState() => CustomRadioInputState<T>();
}

class CustomRadioInputState<T> extends State<CustomRadioInput> {
  final GlobalKey<FormFieldState> _inputKey = GlobalKey<FormFieldState>();
  final GlobalKey<ShakeOnTriggerState> _shakerKey = GlobalKey<ShakeOnTriggerState>();
  final FocusNode focusNode = FocusNode();

  T? get value => _inputKey.currentState?.value;

  List<FormBuilderFieldOption<T?>> get radios => widget.valuesAndLabels.keys.map((value) {
        var label = widget.valuesAndLabels[value] is String
            ? Text(widget.valuesAndLabels[value]!).wrapWithHero('${value}_INPUT')
            : widget.valuesAndLabels[value] ?? Container();
        return FormBuilderFieldOption<T>(
          value: value,
          child: label,
        );
      }).toList();

  void shake() {
    if (_shakerKey.currentState != null && !_shakerKey.currentState!.isShaking) _shakerKey.currentState!.animate();
  }

  void shakeIfInvalid() {
    if (_inputKey.currentState != null && !_inputKey.currentState!.isValid) shake();
  }

  @override
  void dispose() {
    _shakerKey.currentState?.dispose();
    _inputKey.currentState?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ShakeOnTrigger(
        key: _shakerKey,
        child: FormBuilderRadioGroup<T?>(
          name: 'Method',
          key: _inputKey,
          initialValue: widget.value,
          wrapAlignment: WrapAlignment.spaceAround,
          focusNode: focusNode,
          decoration: InputDecoration(
            labelText: widget.label,
            errorText: widget.error,
          ),
          onChanged: widget.onChanged,
          onSaved: widget.onSubmitted,
          validator: widget.validator,
          options: radios,
        ));
  }
}
