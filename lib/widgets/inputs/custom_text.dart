import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_scaffold_bloc/widgets/misc/shake_on_trigger.dart';

class CustomTextInput extends StatefulWidget {
  CustomTextInput(
      {Key? key,
      required this.label,
      this.value,
      this.keyboard,
      this.error,
      this.onChanged,
      this.onSubmitted,
      this.secret = false,
      this.validator})
      : super(key: key);

  final String label;
  final String? value;
  final String? error;
  final Function(String?)? onChanged;
  final Function(String?)? onSubmitted;
  final FormFieldValidator<String>? validator;
  final TextInputType? keyboard;
  final bool secret;

  @override
  CustomTextInputState createState() => CustomTextInputState();
}

class CustomTextInputState extends State<CustomTextInput> {
  final TextEditingController _controller = TextEditingController();
  final GlobalKey<FormFieldState> _inputKey = GlobalKey<FormFieldState>();
  final GlobalKey<ShakeOnTriggerState> _shakerKey = GlobalKey<ShakeOnTriggerState>();
  final FocusNode focusNode = FocusNode();

  String get value => _controller.value.text;
  set value(String value) => _controller.value = TextEditingValue(text: value);

  void shake() {
    if (_shakerKey.currentState != null && !_shakerKey.currentState!.isShaking) _shakerKey.currentState!.animate();
  }

  void shakeIfInvalid() {
    if (_inputKey.currentState != null && !_inputKey.currentState!.isValid) shake();
  }

  @override
  void initState() {
    value = widget.value ?? '';
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    _shakerKey.currentState?.dispose();
    _inputKey.currentState?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ShakeOnTrigger(
      key: _shakerKey,
      child: Hero(
        tag: '${widget.label}_INPUT',
        child: FormBuilderTextField(
          key: _inputKey,
          name: widget.label,
          controller: _controller,
          focusNode: focusNode,
          obscureText: widget.secret,
          keyboardType: widget.keyboard,
          decoration: InputDecoration(
                  labelText: widget.label,
                  errorText: widget.error,
                  errorStyle: Theme.of(context).inputDecorationTheme.errorStyle)
              .applyDefaults(Theme.of(context).inputDecorationTheme),
          onChanged: widget.onChanged,
          onSubmitted: widget.onSubmitted,
          validator: widget.validator,
          autovalidateMode: AutovalidateMode.onUserInteraction,
        ),
      ),
    );
  }
}
