import 'package:flutter/material.dart';
import 'package:flutter_scaffold_bloc/constants/theme/sizes.dart';
import 'package:flutter_scaffold_bloc/widgets/buttons/custom_back.dart';
import 'package:flutter_scaffold_bloc/widgets/buttons/menu.dart';

class CustomSliverBar extends StatelessWidget {
  final Widget? title;
  final Widget? background;
  final Widget? collapsed;
  final double? expandedHeight;
  final EdgeInsets? padding;
  final BorderRadius? borderRadius;
  final Future Function()? onBack;
  final Future Function()? onMenu;
  final bool showBackButton;
  final bool showMenuButton;

  CustomSliverBar({
    this.title,
    this.background,
    this.collapsed,
    this.expandedHeight,
    this.padding,
    this.borderRadius,
    this.onBack,
    this.onMenu,
    this.showBackButton = true,
    this.showMenuButton = true,
  });

  @override
  Widget build(BuildContext context) {
    var canPop = Navigator.of(context).canPop();

    var mainContent = (collapsed != null
        ? title != null
            ? Stack(children: [collapsed!, title!])
            : collapsed
        : title)!;

    var backButton = canPop && showBackButton ? CustomBackButton(action: onBack) : null;

    return SliverAppBar(
      pinned: true,
      expandedHeight: expandedHeight,
      automaticallyImplyLeading: false,
      leading: backButton,
      leadingWidth: backButton?.width ?? 0,
      actions: [if (showMenuButton) MenuButton(action: onMenu)],
      flexibleSpace: ClipRRect(
        borderRadius: borderRadius ?? ThemeSize.borderRadius(Zero),
        child: FlexibleSpaceBar(
          centerTitle: true,
          title: mainContent,
          background: background,
          titlePadding: padding,
        ),
      ),
      shape: RoundedRectangleBorder(
        borderRadius: borderRadius ?? ThemeSize.borderRadius(Zero),
      ),
    );
  }
}
