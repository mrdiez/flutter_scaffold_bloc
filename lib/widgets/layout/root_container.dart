import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_scaffold_bloc/constants/theme/sizes.dart';
import 'package:flutter_scaffold_bloc/services/authenticator.dart';
import 'package:flutter_scaffold_bloc/services/task_manager.dart';
import 'package:flutter_scaffold_bloc/widgets/layout/side.menu.dart';

class RootContainer extends StatelessWidget {
  RootContainer({required this.child});

  final Widget child;

  static final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
        key: scaffoldKey,
        body: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            child,
            BlocBuilder<TaskManager, TaskManagerState>(
                buildWhen: (previous, next) => previous != next,
                builder: (context, state) => state.maybeMap(
                    busy: (_) => LinearProgressIndicator(
                          minHeight: ThemeSize.progressBarHeight(M),
                        ),
                    orElse: () => Container())),
          ],
        ),
        drawer: context.read<Authenticator>().isLogged ? SideMenu() : null,
      ),
    );
  }
}
