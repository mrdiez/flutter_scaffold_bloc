import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_scaffold_bloc/constants/assets.dart';
import 'package:flutter_scaffold_bloc/constants/theme/sizes.dart';
import 'package:flutter_scaffold_bloc/services/authenticator.dart';

class SideMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: SafeArea(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            Padding(
              padding: ThemeSize.padding(S),
              child: DrawerHeader(
                decoration:
                    BoxDecoration(image: DecorationImage(fit: BoxFit.contain, image: AssetImage(Asset.EasterEggLogo))),
                child: Container(),
              ),
            ),
            ListTile(
              title: Text('Log out'),
              onTap: () {
                context.read<Authenticator>().add(AuthenticatorEvent.loggedOut());
              },
            ),
          ],
        ),
      ),
    );
  }
}
