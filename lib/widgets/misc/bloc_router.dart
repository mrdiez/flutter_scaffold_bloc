import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_scaffold_bloc/widgets/misc/custom_bloc.dart';
import 'package:flutter_scaffold_bloc/widgets/misc/custom_navigator.dart';

class BlocRouter<E, B extends CustomBloc<E, S>, S> extends StatefulWidget {
  BlocRouter({required this.routes, Widget Function(BuildContext, BlocRoute)? routeBuilder, this.navigatorKey})
      : routeBuilder = routeBuilder ?? ((_, route) => route.view),
        assert(routes.isNotEmpty && routes.firstWhereOrNull((route) => route.name == BlocRoute.Initial) != null);

  final List<BlocRoute> routes;
  final Widget Function(BuildContext, BlocRoute) routeBuilder;
  final GlobalKey<NavigatorState>? navigatorKey;

  @override
  _BlocRouterState<E, B, S> createState() => _BlocRouterState<E, B, S>();
}

class _BlocRouterState<E, B extends CustomBloc<E, S>, S> extends State<BlocRouter<E, B, S>> {
  final List<String> _history = [BlocRoute.Initial];
  BuildContext? parentContext;

  void _routeListener(BuildContext context, dynamic eventOrState, String currentRoute) {
    var route = widget.routes
        .firstWhereOrNull((_route) => currentRoute != _route.name && _route.matchPush(eventOrState.runtimeType));

    if (route != null) {
      route.navigate(widget.navigatorKey?.currentState ?? context, _history);
    } else {
      route = widget.routes.firstWhereOrNull((_route) {
        var result = _history.lastOrNull == currentRoute &&
            currentRoute == _route.name &&
            _route.matchPop(eventOrState.runtimeType);
        return result;
      });

      if (route != null) {
        var _context = _history.length == 1 && _history.last == BlocRoute.Initial ? parentContext : context;
        route.pop(widget.navigatorKey?.currentState ?? _context, _history);
      }
    }
  }

  BlocListener Function(BuildContext) getBlocRouteListener(BlocRoute route) => (context) {
        context.read<B>().eventListener[route.name] = (E _event) => _routeListener(context, _event, route.name);
        return BlocListener<B, S>(
          listenWhen: (previous, next) => previous != next,
          listener: (context, state) => _routeListener(context, state, route.name),
          child: widget.routeBuilder(context, route),
        );
      };

  @override
  Widget build(BuildContext context) {
    parentContext = context;
    return CustomNavigator(
        navigatorKey: widget.navigatorKey,
        routes: Map.fromEntries(widget.routes.map((route) {
          return MapEntry(route.name, getBlocRouteListener(route));
        })));
  }
}

enum BlocRoutingMethod { Push, PushReplacement, PushAndRemoveUntil }

class BlocRoute {
  const BlocRoute(
      {required this.name,
      this.on = const [],
      this.pushOn = const [],
      this.popOn = const [],
      this.pushMethod = BlocRoutingMethod.Push,
      required this.view});

  final String name;
  final BlocRoutingMethod pushMethod;
  final Widget view;

  static const Initial = '/';

  final List<dynamic> pushOn;
  final List<dynamic> popOn;
  final List<dynamic> on;

  bool matchPop(Type _type) {
    return popOn.firstWhereOrNull((_event) => _type == _event || _type.toString() == '_\$$_event') != null;
  }

  bool matchPush(Type _type) {
    return pushOn.firstWhereOrNull((_event) => _type == _event || _type.toString() == '_\$$_event') != null;
  }

  bool match(Type _type) {
    var array = on.isNotEmpty ? on : pushOn;
    return array.firstWhereOrNull((_event) => _type == _event || _type.toString() == '_\$$_event') != null;
  }

  void navigate(dynamic _navigator, [List<String>? _history]) {
    _navigator = _navigator is BuildContext ? Navigator.of(_navigator) : _navigator;
    switch (pushMethod) {
      case BlocRoutingMethod.PushReplacement:
        if (_history != null) _history.last = name;
        _navigator.pushReplacementNamed(name);
        break;
      case BlocRoutingMethod.PushAndRemoveUntil:
        if (_history != null) _history = [name];
        _navigator.pushNamedAndRemoveUntil(name, (_) => false);
        break;
      default:
        if (_history != null) _history.add(name);
        _navigator.pushNamed(name);
    }
  }

  void pop(dynamic _navigator, [List<String>? _history]) {
    _navigator = _navigator is BuildContext ? Navigator.of(_navigator) : _navigator;
    if (_history != null) _history.removeLast();
    _navigator.pop();
  }
}
