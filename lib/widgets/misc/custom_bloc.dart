import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_scaffold_bloc/constants/translations/generated/l10n.dart';
import 'package:flutter_scaffold_bloc/services/authenticator.dart';
import 'package:flutter_scaffold_bloc/services/task_manager.dart';
import 'package:flutter_scaffold_bloc/widgets/misc/bloc_router.dart';

abstract class CustomBlocWidget<B extends CustomBloc<E, S>, E, S> extends StatelessWidget {
  CustomBlocWidget([this._onState, this._onEvent, this._onBuild]);

  final _GETTERS _getters = _GETTERS<B, E, S>();
  B get bloc => _getters.bloc;
  E get event => _getters.event;
  S get state => _getters.state;
  BlocRoute get route => _getters.route!;
  BuildContext get context => _getters.context!;
  Translation get translation => _getters.bloc.translation!;

  final Function(BuildContext, S)? _onState;
  final Function(E)? _onEvent;
  final Function(S)? _onBuild;

  final B Function(BuildContext)? provider = null;

  void onReady() {}
  void onBuild() {}
  void onState(BuildContext context, S state) {}

  void _injectCustomBlocDependencies(BuildContext _context) {
    _getters.bloc.taskManager = _context.read<TaskManager>();
    _getters.bloc.translation = Translation.of(_context);
    _getters.bloc.authenticator = _context.read<Authenticator>();
  }

  final List<BlocRoute> routes = [];

  Widget _build(BuildContext context, [Widget Function(BuildContext)? _builder]) => Builder(builder: (_context) {
        _getters.bloc = _context.read<B>();
        _getters.bloc.eventListener[CustomBlocWidget] = (E _event) {
          _getters.event = _event;
          if (_onEvent != null) _onEvent!(_event);
        };
        if (!_getters.isReady) {
          _getters.isReady = true;
          WidgetsBinding.instance?.addPostFrameCallback((_) {
            onReady();
          });
        }
        return BlocConsumer<B, S>(
          listenWhen: (previous, next) => previous != next,
          listener: (_context, _state) {
            if (_onState != null) _onState!(_context, _state);
            onState(_context, _state);
          },
          buildWhen: (previous, next) => previous != next,
          builder: (_context, _state) {
            _getters.bloc = _context.read<B>();
            _injectCustomBlocDependencies(_context);
            _getters.state = _state;
            _getters.context = _context;
            WidgetsBinding.instance?.addPostFrameCallback((_) {
              if (_onBuild != null) _onBuild!(_state);
              onBuild();
            });
            return (_builder ?? builder)(_context);
          },
        );
      });

  @override
  Widget build(BuildContext _context) {
    Widget? result;
    if (routes.isNotEmpty) {
      result = BlocRouter<E, B, S>(
        routes: routes,
        routeBuilder: (_context, _route) {
          return _build(_context, (__context) {
            _getters.route = _route;
            _getters.context = __context;
            return builder(__context);
          });
        },
      );
    }
    result ??= _build(_context);
    if (provider != null) {
      result = BlocProvider(create: provider!, child: result);
    }
    return result;
  }

  // To be implemented
  Widget builder(BuildContext context);
}

abstract class CustomBloc<E, S> extends Bloc<E, S> {
  CustomBloc(state) : super(state);
  Map<dynamic, Function(E)?> eventListener = {};
  late TaskManager taskManager;
  late Translation translation;
  late Authenticator authenticator;

  @override
  void onEvent(E event) {
    eventListener.forEach((key, listener) {
      if (listener != null) listener(event);
    });
    super.onEvent(event);
  }
}

class _GETTERS<B, E, S> {
  B? bloc;
  E? event;
  S? state;
  BlocRoute? route;
  BuildContext? context;
  Translation? translation;
  bool isReady = false;
}
