import 'package:flutter/material.dart';

class CustomNavigator extends StatelessWidget {
  CustomNavigator({this.navigatorKey, required this.routes}) : assert(routes.keys.contains('/'));

  final GlobalKey<NavigatorState>? navigatorKey;
  final HeroController _heroController = HeroController();
  // final Map<String, Widget> routes;
  final Map<String, Widget Function(BuildContext)> routes;

  @override
  Widget build(BuildContext context) {
    return ClipRect(
      child: Navigator(
        key: navigatorKey,
        observers: [_heroController],
        onGenerateRoute: (settings) => MaterialPageRoute<dynamic>(
          // builder: (context) => routes[settings.name]!,
          builder: routes[settings.name]!,
          settings: settings,
        ),
      ),
    );
  }
}
