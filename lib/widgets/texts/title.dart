import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_scaffold_bloc/constants/theme/sizes.dart';
import 'package:flutter_scaffold_bloc/utils/widget.dart';

class TitleText extends StatelessWidget {
  final String? text;
  final TextAlign? align;
  final Color? color;
  final Sizing size;
  final TextStyle? style;
  final TextOverflow overflow;
  final bool wrap;
  final bool uppercase;
  final bool hero;
  final String? tag;
  final bool expand;
  final int maxLines;

  TitleText(this.text,
      {this.hero = false,
      this.tag,
      this.maxLines = 1,
      this.expand = false,
      this.align,
      this.color,
      this.size = M,
      this.style,
      this.overflow = TextOverflow.visible,
      this.wrap = true,
      this.uppercase = false})
      : assert([XXXL, XXL, XL, L, M, S].contains(size));

  TextStyle getTextStyle(BuildContext context) {
    switch (size) {
      case XXXL:
        return Theme.of(context).textTheme.headline1!;
      case XXL:
        return Theme.of(context).textTheme.headline2!;
      case XL:
        return Theme.of(context).textTheme.headline3!;
      case L:
        return Theme.of(context).textTheme.headline4!;
      case M:
        return Theme.of(context).textTheme.headline5!;
      case S:
        return Theme.of(context).textTheme.headline6!;
      default:
        return Theme.of(context).textTheme.headline4!;
    }
  }

  @override
  Widget build(BuildContext context) {
    var _text = text ?? '';
    var _style = getTextStyle(context);
    return AutoSizeText(
      uppercase ? _text.toUpperCase() : _text,
      softWrap: wrap,
      overflow: overflow,
      style: _style.copyWith(color: color ?? _style.color),
      textAlign: align ?? TextAlign.left,
      maxLines: maxLines,
    ).wrapWithHero(hero ? tag ?? text : tag).expand(expand);
  }
}
